/******************************************************************************
 * program:     wp2latex                                                      *
 * function:    Convert WordPerfect files into LaTeX.			      *
 * modul:       pass1ole.cc                                                   *
 * description: Opens OLE archive and allow conversion of embedded documents. *
 * libraries:	cole   (modified for wp2latex)				      *
 * licency:     GPL		                                              *
 ******************************************************************************/
#include <string.h>
#include <stdio.h>
#include <stdlib.h>

#include <ctype.h>

#include <sets.h>
#include <lists.h>
#include <stringa.h>
#if defined(__UNIX__)||defined(__DJGPP__)
 #include <unistd.h>
#endif

#include "wp2latex.h"
#include "cole/cole.h"

class TconvertedPass1_OLE:public TconvertedPass1
     {
public:
     virtual int Convert_first_pass(void);
     };

/*Register translators here*/
TconvertedPass1 *Factory_OLE(void) {return new TconvertedPass1_OLE;}
FFormatTranslator FormatOLEWrapper("OLE Stream",Factory_OLE);

extern string wpd_filename;


/*******************************************************************/
/* This procedure provides all needed processing for the first pass*/
int TconvertedPass1_OLE::Convert_first_pass(void)
{
#ifdef DEBUG
  fprintf(log,"\n#Convert_pass1_OLE() ");fflush(log);
#endif
COLEFS *OleFS;
COLEFILE *Fin;
COLEDIR *Dir;
COLEDIRENT *cde;
string temp_filename;
TconvertedPass1 *cq1_new;
int RetVal;

  temp_filename = copy(wpd_filename,0,
		   GetExtension(wpd_filename()) - wpd_filename()) + ".$$$";

  if(Verbosing >= 1) printf(_("Opening OLE archive:\n"));

  OleFS = (COLEFS *)malloc(sizeof(COLEFS));	//Mount OLE filesystem
  if(OleFS==NULL) {RunError(0x204);return -1;}

  memset(OleFS,0,sizeof(COLEFS));
  OleFS->file = wpd;
  if((RetVal=cpp_OLEdecode(NULL, OleFS, 0))!=0)
	{
        if(Verbosing>=1 && err!=NULL)
          fprintf(err,_("Error: Problem No:%d during OLE archive open.\n"), RetVal);
        free(OleFS);return(0);
        }

  Dir=cole_opendir_rootdir(OleFS,NULL);
		/* Iterate through childrens */
  for(cde=cole_visiteddirentry(Dir); cde!=NULL; cde=cole_nextdirentry(Dir))
       {
       const char *entry_name = chk(cole_direntry_getname(cde));

       cole_fprint_direntry(cde,log);

       if(cole_direntry_isfile(cde))
	  {
	  Fin=cole_fopen_direntry(cde,NULL,strdup(temp_filename()));
	  if(Fin)
	    {
	    //cq1_new=NULL;
	    CheckFileFormat(Fin->file,FilForD);
	    cq1_new=GetConverter(FilForD.Converter);

	    if(cq1_new==NULL && !strcmp(entry_name,"WordDocument"))
		{
		FilForD.DocVersion=0x800;	/*The autodetection failed - try to guess*/
		cq1_new=GetConverter("WORD");
		}
	    if(cq1_new!=NULL)
		{
		//cq.perc.Hide();
		if(Verbosing >= 1) printf(_("[nested \"%s\" %s] "), entry_name,FilForD.Converter);
		cq1_new->InitMe(Fin->file,table,strip,log,err);
		cq1_new->Dispatch(DISP_WRAPPER,OleFS);
		if(cq1_new->Convert_first_pass()>=1) RetVal=1;
		if(Verbosing >= 1) printf(_("\n[continuing] "));
		if(log!=NULL) fputs(_("\n--End or nested file.--\n"),log);
		//cq.perc.Show();
		delete cq1_new;
		}

	    cole_fclose(Fin,NULL);
	    }
	  }
       }

  cole_closedir(Dir);

  OleFS->file=NULL;		//Tricky way for not closing file
  cole_umount(OleFS,NULL);
  return(RetVal);
}


/*--------------------End of PASS1_OLE--------------------*/