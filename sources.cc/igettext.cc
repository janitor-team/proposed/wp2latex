/******************************************************************************
 * program:     wp2latex                                                      *
 * function:    convert WordPerfect 5.x files into LaTeX                      *
 * modul:       igettext.cc                                                   *
 * description: This modul initializes language translation features. This    *
 *              allow you to communicate with this program using your native  *
 *              language. It is possible to omit this feature when            *
 *	        __gettext__ is not defined.		                      *
 * licency:     GPL		                                              *
 ******************************************************************************/
#ifdef __gettext__

#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>

#include <stringa.h>
#include <locale.h>
#include "wp2latex.h"

#include "images/img_futi.h"


const char *Empty2Info = NULL;


/// This function expands system environment variables referenced from
/// inputstring.
string expand_spec(string name)
{
//#ifdef DEBUG
//  fprintf(cq->log,"\n!expand_spec(%s) ",name.ch);fflush(cq->log);
//#endif
int pos,end;
string temp;

while(StrStr(name,"$(")!=NULL)
	{
	pos=StrStr(name,"$(")-name();
	end=pos+2;
	while (name[end]!=0)
	   {
	   if (name[end]==')') break;
	   end++;
	   }
	if (name[end]==0) break;
	temp=copy(name,pos+2,end-pos-2);

        if (StrStr(temp,"$(")!=NULL)
                 temp=expand_spec(temp);
//puts(temp.ch);
        if(!temp.isEmpty())
		{
                temp = getenv(temp);
		}
//puts(temp.ch);
        name = copy(name,0,pos)+temp+copy(name,end+1,length(name)-end-1);

        temp=name;//!!!!
//puts(temp.ch);
	}

return(temp);
}


void Proces_File(const string filename)
{
//#ifdef DEBUG
//  fprintf(cq->log,"\n!Proces_File(%s) ",filename.ch);fflush(cq->log);
//#endif
FILE *f;
string line,temp;
int i;

if((f=fopen(filename,"r"))==NULL) return;
while(!feof(f))
  {
  fGets2(f, line);
  line.trim();
  if(line=="")     continue;
  if(line[0]=='#') continue;

  temp.erase();
  for(i=0; i<length(line); i++)
           {
           if(isalpha(line[i])) temp+=line[i];
                           else break;
           }
  if(line[i++] != '=') continue;
  temp+='=';
  for(; i<length(line); i++)
           {
           if(!isalnum(line[i]) || (line[i]!='$') ||
                 (line[i]!='(') || (line[i]!=')') || (line[i]!='/') ||
                 (line[i]!='\\')|| (line[i]!=':')) temp+=line[i];
                                              else break;
           }
  if(!temp.isEmpty())
    putenv(temp.ExtractString()); //The string must not be released!!!
//puts(temp.ch);
  }
fclose(f);
}


/** This procedure provides inicialization  of gettext library.
  Its main task is to find a proper LOCALEDIR */
void InitGettext(const char *GtxtDomain)
{
//#ifdef DEBUG
//  fprintf(cq->log,"\n!InitGettext() ");fflush(cq->log);
//#endif
  string Locale_Dir;

#if defined(__DJGPP__) || defined(_WIN32)
  Proces_File(expand_spec("$(DJGPP)"));
#endif

  Locale_Dir = expand_spec("$(LOCALEDIR)");

#if defined(__DJGPP__) || defined(_WIN32)
      /* The default localedir for DJGPP is: $(DJDIR)/share/locale/  */
  if (Locale_Dir=="")
      {
      Locale_Dir = expand_spec("$(DJDIR)");
      if(Locale_Dir == "%:/>DJGPP%")
  	 {
  	 Locale_Dir = getenv("DJGPP");
  	 Locale_Dir = copy(Locale_Dir,0,GetPathEnd(Locale_Dir));
  	 }
      if(!Locale_Dir.isEmpty())
         {
	 Locale_Dir += "/share/locale";
         }
      else Locale_Dir = "c:/usr/local/share/locale";
      }
#else           /* The default localedir for UNIX is: /usr/share/locale/  */
if (Locale_Dir=="")
      {
//    Locale_Dir = "/usr/local/share/locale";
      Locale_Dir = "/usr/share/locale";
      }
#endif

//putenv("LANGUAGE=cs");
//puts(Locale_Dir());
  setlocale(LC_ALL & ~LC_NUMERIC,"");
  setlocale(LC_NUMERIC,"POSIX");
  bindtextdomain(GtxtDomain, Locale_Dir());	//"g:/gcc/share/locale"
  textdomain(GtxtDomain);

  return;
}


void CheckGettext(const char *PROG_NAME, const char *GtxtDomain)
{
int i;
string ProgPath;
static const char GET_LOCALE_INFO[] = "";

  Empty2Info = _(GET_LOCALE_INFO);	//when gettext is active "" is expanded to the localisation details
  i = StrLen(Empty2Info);	// NULL aware check.
  if(i==0)
     {
     i = GetPathEnd(PROG_NAME);
     if(i<=0) ProgPath="./locale";
     else {
          if(*PROG_NAME=='"')
            ProgPath = copy(PROG_NAME,1,i-1)+"/locale";
          else
	    ProgPath = copy(PROG_NAME,0,i)+"/locale";
	  }
     bindtextdomain(GtxtDomain, ProgPath());

     Empty2Info = _(GET_LOCALE_INFO);	//when gettext is active "" is expanded to the localisation details 
     }

#ifdef _MSC_VER
  if(Empty2Info!=NULL && *Empty2Info!=0)
  {
    //printf("%s",Empty2Info);
    if(strstr(Empty2Info,"charset=UTF-8")!=NULL)
    {
      FixConsole2UTF8();
    }
  }
#endif
}


#endif //__gettext__

