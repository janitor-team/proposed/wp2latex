#include "imgsupp.h"

#include <stdio.h>

#include "raster.h"


#define ImplementReader(SUFFIX,EXTENSION) \
Image LoadPicture##SUFFIX(const char *Name);\
int SavePicture##SUFFIX(const char *Name,const Image &Img);\
TImageFileHandler HANDLER_##SUFFIX(EXTENSION,


#ifdef SupportART
 ImplementReader(ART,".ART")
 #if SupportART>=4 || SupportART==2
   LoadPictureART,
 #else
   NULL,
 #endif
 #if SupportART>=3
   SavePictureART);
 #else
   NULL);
 #endif
#endif


#ifdef SupportBMP
 ImplementReader(BMP,".BMP")
 #if SupportBMP>=4 || SupportBMP==2
	LoadPictureBMP,
 #else
	NULL,
 #endif
 #if SupportBMP>=3
	SavePictureBMP);
 #else
	NULL);
 #endif

 #if SupportBMP>=4 || SupportBMP==2
  TImageFileHandler HANDLER_RLE(".RLE", LoadPictureBMP, NULL);
 #endif

#endif


#ifdef SupportCSV
 ImplementReader(CSV,".CSV")
 #if SupportCSV>=4 || SupportCSV==2
	LoadPictureCSV,
 #else
	NULL,
 #endif
 #if SupportCSV>=3
	SavePictureCSV);
 #else
	NULL);
 #endif
#endif


#ifdef SupportCUT
 ImplementReader(CUT,".CUT")
 #if SupportCUT>=4 || SupportCUT==2
	LoadPictureCUT,
 #else
	NULL,
 #endif
// #if SupportCUT>=3
//	SavePictureCUT);
// #else
	NULL);
// #endif
#endif


#ifdef SupportDIB
 ImplementReader(DIB,".DIB")
 #if SupportDIB>=4 || SupportDIB==2
	LoadPictureDIB,
 #else
	NULL,
 #endif
 #if SupportDIB>=3
	SavePictureDIB);
 #else
	NULL);
 #endif
#endif


#ifdef SupportEPS
 ImplementReader(EPS,".EPS")
 #if SupportEPS>=4 || SupportEPS==2
	LoadPictureEPS,
 #else
	NULL,
 #endif
 #if SupportEPS>=3
	SavePictureEPS);
 #else
	NULL);
 #endif
 TImageFileHandler PS_H(".PS",
 #if SupportEPS>=4 || SupportEPS==2
	LoadPictureEPS,
 #else
	NULL,
 #endif
 #if SupportEPS>=3
	SavePictureEPS);
 #else
	NULL);
 #endif
#endif


#ifdef SupportFITS
 ImplementReader(FITS,".FITS") 
 #if SupportFITS>=4 || SupportFITS==2
	LoadPictureFITS,
 #else
	NULL,
 #endif
 #if SupportFITS>=3
	SavePictureFITS);
 #else
	NULL);
 #endif

 TImageFileHandler FTS_H(".FTS",
 #if SupportFITS>=4 || SupportFITS==2
	LoadPictureFITS,
 #else
	NULL,
 #endif
 #if SupportFITS>=3
	SavePictureFITS);
 #else
	NULL);
 #endif
#endif


#ifdef SupportGIF
 ImplementReader(GIF,".GIF")
 #if SupportGIF>=4 || SupportGIF==2
	LoadPictureGIF,
 #else
	NULL,
 #endif
 #if SupportGIF>=3
	SavePictureGIF);
#else
	NULL);
#endif
#endif


#ifdef SupportHRZ
 ImplementReader(HRZ,".HRZ")
 #if SupportHRZ>=4 || SupportHRZ==2
	LoadPictureHRZ,
 #else
	NULL,
 #endif
 #if SupportHRZ>=3
	SavePictureHRZ);
 #else
	NULL);
 #endif
#endif


#ifdef SupportICO
 ImplementReader(ICO,".ICO")
 #if SupportICO>=4 || SupportICO==2
	LoadPictureICO,
 #else
	NULL,
 #endif
 #if SupportICO>=3
	SavePictureICO);
 #else
	NULL);
 #endif
#endif


#ifdef SupportMAC
 ImplementReader(MAC, ".MAC")
 #if SupportMAC>=4 || SupportMAC==2
	LoadPictureMAC,
 #else
	NULL,
 #endif
 #if SupportMAC>=3
	SavePictureMAC);
 #else
	NULL);
 #endif
#endif


#ifdef SupportMAT
 ImplementReader(MAT,".MAT")
 #if SupportMAT>=4 || SupportMAT==2
	LoadPictureMAT,
 #else
	NULL,
 #endif
 #if SupportMAT>=3
	SavePictureMAT);
 #else
	NULL);
 #endif

 ImplementReader(MAT4,".MAT")
 #if SupportMAT>=4 || SupportMAT==2
	LoadPictureMAT4,
 #else
	NULL,
 #endif
	NULL,
        "r4");

#endif


#ifdef SupportOKO
 ImplementReader(OKO,".OKO")
#if SupportOKO>=4 || SupportOKO==2
	LoadPictureOKO,
#else
	NULL,
#endif
#if SupportOKO>=3
	SavePictureOKO);
#else
	NULL);
#endif
#endif


#ifdef SupportPBM
 ImplementReader(PBM,".PBM")
 #if SupportPBM>=4 || SupportPBM==2
	LoadPicturePBM,
 #else
	NULL,
 #endif
 #if SupportPBM>=3
	SavePicturePBM);
 #else
	NULL);
 #endif

 TImageFileHandler PPM_H(".PPM",
 #if SupportPBM>=4 || SupportPBM==2
	LoadPicturePBM,
 #else
	NULL,
 #endif
 #if SupportPBM>=3
	SavePicturePBM);
 #else
	NULL);
 #endif
#endif


#ifdef SupportPCX
 ImplementReader(PCX,".PCX")
#if SupportPCX>=4 || SupportPCX==2
	LoadPicturePCX,
#else
	NULL,
#endif
#if SupportPCX>=3
	SavePicturePCX);
#else
	NULL);
#endif
#endif


#ifdef SupportRAS_SUN
 ImplementReader(RAS_SUN, ".RAS")
 #if SupportRAS_SUN>=4 || SupportRAS_SUN==2
	LoadPictureRAS_SUN,
 #else
	NULL,
 #endif
 #if SupportRAS_SUN>=3
 	SavePictureRAS_SUN,
 #else
	NULL,
 #endif
        "SUN");
#endif


#ifdef SupportRAS
 ImplementReader(RAS,".RAS")
 #if SupportRAS>=4 || SupportRAS==2
	LoadPictureRAS,
 #else
	NULL,
 #endif
 #if SupportRAS>=3
	SavePictureRAS,
 #else
	NULL,
 #endif
        "topoL");
#endif


#ifdef SupportRAW
  ImplementReader(RAW,".RAW")
//#if SupportRAW>=4 || SupportRAW==2
//	LoadPictureRAW,
//#else
	NULL,
//#endif
#if SupportRAW>=3
	SavePictureRAW);
#else
	NULL);
#endif
#endif


#ifdef SupportTGA
 ImplementReader(TGA,".TGA")
#if SupportTGA>=4 || SupportTGA==2
	LoadPictureTGA,
#else
	NULL,
#endif
#if SupportTGA>=3
	SavePictureTGA);
#else
	NULL);
#endif
#if SupportTGA>=4 || SupportTGA==2
 ImplementReader(ICB,".ICB")
	LoadPictureTGA,
	NULL);
 ImplementReader(VDA,".VDA")
	LoadPictureTGA,
	NULL);
 ImplementReader(VST,".VST")
	LoadPictureTGA,
	NULL);
#endif
#endif


#ifdef SupportTIFF
 ImplementReader(TIFF,".TIF")
#if SupportTIFF>=4 || SupportTIFF==2
	LoadPictureTIFF,
#else
	NULL,
#endif
//#if SupportTIFF>=3
//	SavePictureTIFF);
//#else
	NULL);
//#endif

TImageFileHandler TIFF_H(".TIFF",
#if SupportTIFF>=4 || SupportTIFF==2
	LoadPictureTIFF,
#else
	NULL,
#endif
//#if SupportTIFF>=3
//	SavePictureTIFF);
//#else
	NULL);
//#endif

#endif

#ifdef SupportTXT
 ImplementReader(TXT,".TXT")
 #if SupportTXT>=4 || SupportTXT==2
	LoadPictureTXT,
 #else
	NULL,
 #endif
 #if SupportTXT>=3
	SavePictureTXT,
 #else
	NULL,
 #endif
	"plain");
#endif


#ifdef SupportTXTgm
 ImplementReader(TXTgm, ".TXT")
 #if SupportTXTgm>=4 || SupportTXTgm==2
	LoadPictureTXTgm,
 #else
	NULL,
 #endif
 #if SupportTXTgm>=3
	SavePictureTXTgm,
 #else
	NULL,
 #endif
	"gm");
#endif


#ifdef SupportWMF
 ImplementReader(WMF,".WMF")
 #if SupportWMF>=4 || SupportWMF==2
	LoadPictureWMF,
 #else
	NULL,
 #endif
 #if SupportWMF>=3
	SavePictureWMF);
 #else
	NULL);
 #endif
#endif


#if defined(SupportWPG) && SupportWPG>=2
 ImplementReader(WPG,".WPG")
 #if SupportWPG>=4 || SupportWPG==2
	LoadPictureWPG,
 #else
	NULL,
 #endif
 #if SupportWPG>=3
 	SavePictureWPG);
 #else
	NULL);
 #endif
#endif


#if defined(SupportXPM) && SupportXPM>=2
 ImplementReader(XPM3,".XPM")
 #if SupportXPM>=4 || SupportXPM==2
	LoadPictureXPM3,
 #else
	NULL,
 #endif
 #if SupportXPM>=3
 	SavePictureXPM3,
 #else
	NULL,
 #endif
        "3");
#endif


#if defined(SupportXPM) && SupportXPM>=2
 ImplementReader(XPM2,".XPM")
 #if SupportXPM>=4 || SupportXPM==2
	LoadPictureXPM2,
 #else
	NULL,
 #endif
 //#if SupportXPM>=3
 //	SavePictureXPM);
 //#else
	NULL,
 //#endif
        "2");
#endif


/////////////////////////////////////////////////


#if defined(SupportJPG) && SupportJPG>=2
 ImplementReader(JPG,".JPG")
 #if SupportJPG>=4 || SupportJPG==2
   LoadPictureJPG,
 #else
   NULL,
 #endif
 #if SupportJPG>=3
   SavePictureJPG);
 #else
   NULL);
 #endif

 TImageFileHandler JPEG_HANDLER(".JPEG",
 #if SupportJPG>=4 || SupportJPG==2
	LoadPictureJPG,
 #else
	NULL,
 #endif
 #if SupportJPG>=3
	SavePictureJPG);
 #else
	NULL);
 #endif
#endif


#if defined(SupportPNG) && SupportPNG>=2
 ImplementReader(PNG,".PNG")
 #if SupportPNG>=4 || SupportPNG==2
   LoadPicturePNG,
 #else
   NULL,
 #endif
 #if SupportPNG>=3
   SavePicturePNG);
 #else
   NULL);
 #endif
#endif


#if defined(SupportFTG) && SupportFTG>=2
 ImplementReader(FTG,".FTG")
 #if SupportFTG>=4 || SupportFTG==2
   LoadPictureFTG,
 #else
   NULL,
 #endif
 #if SupportFTG>=3
   SavePictureFTG);
 #else
   NULL);
 #endif
#endif
