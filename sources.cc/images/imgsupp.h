//Support flags 0 undefined - no support
//		1 typedefs only
//		2 load format only
//		3 save format only
//	      >=4 both load and save
// These support flags could be predefined externally.

#ifdef NO_IMG
 #define SupportART 0
 #define SupportBMP 2	//Read procedure only
 #define SupportCUT 0
 #define SupportCSV 0
 #define SupportDIB 0
 #define SupportFITS 0
 #define SupportFTG 0
 #define SupportGIF 0
 #define SupportHRZ 0
 #define SupportICO 0
 #define SupportJPG 0
 #define SupportMAC 0
 #define SupportMAT 0
 #define SupportOKO 0
 #define SupportPBM 0
 #define SupportPCX 2	//Read procedure only
 #define SupportPNG 0
 #define SupportRAS 0
 #define SupportRAS_SUN 0
 #define SupportRAW 0
 #define SupportTGA 0
 #define SupportTIFF 0
 #define SupportTXT 0
 #define SupportTXTgm 0
 #define SupportWMF 1
 #define SupportWPG 1	//Compile only supporting stub, build-in reader included
 #define SupportXPM 0
#else
 #define SupportART 2 	//Read procedure only
 #define SupportBMP 2	//Read procedure only
 #define SupportCSV 2	//Read procedure only
 #define SupportDIB 2
 #define SupportFITS 2	//Read procedure only
 #define SupportICO 2	//Read procedure only
 #define SupportMAT 2	//Read procedure only
 #define SupportOKO 2	//Read procedure only
 #define SupportPCX 2	//Read procedure only
 #define SupportRAS 2	//Read procedure only
 #define SupportRAW 2	//Read procedure only
 #define SupportTGA 2	//Read procedure only
 #define SupportTXT 2	//Read procedure only
 #define SupportWMF 2
 #define SupportWPG 1	//Compile only supporting stub, build-in reader included
 #define SupportXPM 2
#endif
#define SupportFTG  0

/*----------------------------------------*/

#ifndef SupportART
  #define SupportART  10
#endif
#ifndef SupportBMP
  #define SupportBMP  10
#endif
#ifndef SupportCSV
  #define SupportCSV  10
#endif
#ifndef SupportCUT
  #define SupportCUT  10
#endif
#ifndef SupportDIB
  #define SupportDIB  2
#endif
#ifndef SupportEPS
  #define SupportEPS  10
#endif
#ifndef SupportFITS
  #define SupportFITS 10
#endif
#ifndef SupportGIF
  #define SupportGIF  10
#endif
#ifndef SupportHRZ
  #define SupportHRZ  2
#endif
#ifndef SupportICO
  #define SupportICO  10
#endif
#ifndef SupportMAC
  #define SupportMAC  2
#endif
#ifndef SupportMAT
  #define SupportMAT  10
#endif
#ifndef SupportOKO
  #define SupportOKO  10
#endif
#ifndef SupportPBM
  #define SupportPBM  2
#endif
#ifndef SupportPCX
  #define SupportPCX  10
#endif
#ifndef SupportRAS
  #define SupportRAS  10
#endif
#ifndef SupportRAS_SUN
  #define SupportRAS_SUN  10
#endif
#ifndef SupportRAW
  #define SupportRAW  10
#endif
#ifndef SupportTGA
  #define SupportTGA 10
#endif
#ifndef SupportTIFF
  #define SupportTIFF 10
#endif
#ifndef SupportTXT
  #define SupportTXT 10
#endif
#ifndef SupportTXTgm
  #define SupportTXTgm 10
#endif
#ifndef SupportWMF
  #define SupportWMF  2
#endif
#ifndef SupportWPG
  #define SupportWPG  10
#endif
#ifndef SupportXPM
  #define SupportXPM  10
#endif


#ifndef SupportJPG
  #define SupportJPG  10
#endif
#ifndef SupportPNG
  #define SupportPNG  10
#endif

#ifndef SupportFTG
  #define SupportFTG  10
#endif
