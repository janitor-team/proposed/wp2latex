/******************************************************************************
 * Unit:    raster            release 0.22                                    *
 * Prototype declaration of separate classes. Normally this is not needed     *
 * to be included.                                                            *
 * licency:     GPL or LGPL                                                   *
 * Copyright: (c) 1998-2023 Jaroslav Fojtik                                   *
 ******************************************************************************/
#ifndef _RAS_PROT_H_
#define _RAS_PROT_H_

//#define OPTIMISE_SPEED

#ifdef _MSC_VER
 #pragma warning (push)
 #pragma warning (disable : 4250) 
#endif


/********************* Specialised Raster 1D modules *******************/

/** 1 bit plane */
class Raster1D_1Bit: virtual public Raster1DAbstract
	{
public: virtual int GetPlanes(void) const {return(1);};

	virtual DWORD GetValue1D(unsigned x) const;
	virtual void SetValue1D(unsigned x, DWORD NewValue);
#ifdef _REENTRANT
        virtual DWORD PTR_GetValue1D(const void *RAW_Data1D, unsigned x) const;
        virtual void PTR_SetValue1D(void *RAW_Data1D, unsigned x, DWORD NewValue) const;
#endif
#ifdef OPTIMISE_SPEED
	virtual void Get(Raster1DAbstract &R1) const;
	virtual void Set(const Raster1DAbstract &R1);
#endif
	};


/** 2 bit planes */
class Raster1D_2Bit: virtual public Raster1DAbstract
	{
public: virtual int GetPlanes(void) const {return(2);};

	virtual DWORD GetValue1D(unsigned x) const;
	virtual void SetValue1D(unsigned x, DWORD NewValue);
#ifdef _REENTRANT
        virtual DWORD PTR_GetValue1D(const void *RAW_Data1D, unsigned x) const;
        virtual void PTR_SetValue1D(void *RAW_Data1D, unsigned x, DWORD NewValue) const;
#endif
	};


/** 4 bit planes */
class Raster1D_4Bit: virtual public Raster1DAbstract
	{
public: virtual int GetPlanes(void) const {return(4);};

	virtual DWORD GetValue1D(unsigned x) const;
	virtual void SetValue1D(unsigned x, DWORD NewValue);
#ifdef _REENTRANT
        virtual DWORD PTR_GetValue1D(const void *RAW_Data1D, unsigned x) const;
        virtual void PTR_SetValue1D(void *RAW_Data1D, unsigned x, DWORD NewValue) const;
#endif

#ifdef OPTIMISE_SPEED
	virtual void Set(const Raster1DAbstract &R1);
	virtual void Get(Raster1DAbstract &R1) const;
#endif	//OPTIMISE_SPEED
	};


class Raster1D_4BitIDX: public Raster1D_4Bit
{
public: virtual int GetPlanes(void) const {return(-4);};
};


/** 8 bit planes */
class Raster1D_8Bit: virtual public Raster1DAbstract
	{
public:	Raster1D_8Bit(int InitSize1D) {if((Data1D=malloc(InitSize1D))!=NULL) Size1D=InitSize1D;}
	Raster1D_8Bit(void)	{};

	virtual int GetPlanes(void) const {return(8);};
#ifdef OPTIMISE_SPEED
	virtual void Set(const Raster1DAbstract &R1);
	virtual void Get(Raster1DAbstract &R1) const;
#endif	//OPTIMISE_SPEED

	virtual DWORD GetValue1D(unsigned x) const;
	virtual void SetValue1D(unsigned x, DWORD NewValue);

#ifdef _REENTRANT
        virtual DWORD PTR_GetValue1D(const void *RAW_Data1D, unsigned x) const;
        virtual void PTR_SetValue1D(void *RAW_Data1D, unsigned x, DWORD NewValue) const;
        virtual double PTR_GetValue1Dd(const void *RAW_Data1D, unsigned x) const;
	virtual void PTR_SetValue1Dd(void *RAW_Data1D, unsigned x, double NewValue) const;	
#endif
	};


class Raster1D_8BitIDX: public Raster1D_8Bit
{
public: virtual int GetPlanes(void) const {return(-8);};
};



/** 16 bit planes */
class Raster1D_16Bit: virtual public Raster1DAbstract
	{
public: Raster1D_16Bit(int InitSize1D) {if((Data1D=malloc(2*InitSize1D))!=NULL) Size1D=InitSize1D;}
	Raster1D_16Bit(void)	{};

	virtual int GetPlanes(void) const {return(16);};

	virtual DWORD GetValue1D(unsigned x) const {if(Size1D<=x) return(0); return(((WORD *)Data1D)[x]);};
	virtual void SetValue1D(unsigned x, DWORD NewValue) {if(Size1D<=x) return; ((WORD *)Data1D)[x]=(WORD)NewValue;};
#ifdef _REENTRANT
        virtual DWORD PTR_GetValue1D(const void *RAW_Data1D, unsigned x) const;
        virtual void PTR_SetValue1D(void *RAW_Data1D, unsigned x, DWORD NewValue) const;
	virtual double PTR_GetValue1Dd(const void *RAW_Data1D, unsigned x) const;
	virtual void PTR_SetValue1Dd(void *RAW_Data1D, unsigned x, double NewValue) const;
#endif
#ifdef OPTIMISE_SPEED
	virtual void Get(Raster1DAbstract &R1) const;
	void Set(const Raster1DAbstract &R1);
#endif
	};


/*
class Raster1D_16BitIDX: public Raster1D_16Bit
{
public: Raster1D_16BitIDX(int InitSize1D, Raster1DAbstractRGB *newPalette=NULL);
        Raster1D_16BitIDX(void)	{pPalette=NULL;};
        ~Raster1D_16BitIDX();

        virtual int GetPlanes(void) const {return(-16);};
        virtual void Set(const Raster1DAbstract &R1);

        Raster1DAbstractRGB *pPalette;
};
*/


/** 24 bit planes Gray */
class Raster1D_24Bit:virtual public Raster1DAbstract
	{
public: virtual int GetPlanes(void) const {return(24);};

	virtual DWORD GetValue1D(unsigned x) const;
	virtual void SetValue1D(unsigned x, DWORD NewValue);
#ifdef _REENTRANT
        virtual DWORD PTR_GetValue1D(const void *RAW_Data1D, unsigned x) const;
	virtual void PTR_SetValue1D(void *RAW_Data1D, unsigned x, DWORD NewValue) const;
#endif
#ifdef OPTIMISE_SPEED
	virtual void Get(Raster1DAbstract &R1) const;
	virtual void Set(const Raster1DAbstract &R1);
#endif
	virtual void Get24BitRGB(void *Buffer24Bit) const;

	virtual void Peel8Bit(BYTE *Buffer8Bit, char plane8) const;
	virtual void Join8Bit(BYTE *Buffer8Bit, char plane8);
	};


/** 32 bit planes */
class Raster1D_32Bit:virtual public Raster1DAbstract
	{
public: virtual int GetPlanes(void) const {return(32);};

	virtual DWORD GetValue1D(unsigned x) const {if(x<Size1D) return(((DWORD *)Data1D)[x]); return(0);};
	virtual void SetValue1D(unsigned x, DWORD NewValue) {if(x<Size1D) ((DWORD *)Data1D)[x]=NewValue; return;};
	virtual void SetValue1Dd(unsigned x, double NewValue);
#ifdef _REENTRANT
        virtual DWORD PTR_GetValue1D(const void *RAW_Data1D, unsigned x) const;
        virtual void PTR_SetValue1D(void *RAW_Data1D, unsigned x, DWORD NewValue) const;
        virtual void PTR_SetValue1Dd(void *RAW_Data1D, unsigned x, double NewValue) const;
#endif
#ifdef OPTIMISE_SPEED
	virtual void Get(Raster1DAbstract &R1) const;
	virtual void Set(const Raster1DAbstract &R1);
#endif
	};


#ifdef QWORD
/** 64 bit planes */
class Raster1D_64Bit:virtual public Raster1DAbstract
	{
public: virtual int GetPlanes(void) const {return(64);};

	virtual DWORD GetValue1D(unsigned x) const;
	virtual double GetValue1Dd(unsigned x) const {if(x<Size1D) return((double)((QWORD *)Data1D)[x]); return(0);};
	virtual void SetValue1D(unsigned x, DWORD NewValue);
	virtual void SetValue1Dd(unsigned x, double NewValue);
#ifdef _REENTRANT
        virtual DWORD PTR_GetValue1D(const void *RAW_Data1D, unsigned x) const;
        virtual double PTR_GetValue1Dd(const void *RAW_Data1D, unsigned x) const;
        virtual void PTR_SetValue1D(void *RAW_Data1D, unsigned x, DWORD NewValue) const;
        virtual void PTR_SetValue1Dd(void *RAW_Data1D, unsigned x, double NewValue) const;
#endif
#ifdef OPTIMISE_SPEED
	//virtual void Get(Raster1DAbstract &R1) const;
	virtual void Set(const Raster1DAbstract &R1);
#endif
	};
#endif


/** 32 bit planes - float */
class Raster1D_32FltBit: virtual public Raster1DAbstract
	{
public: float Min, Max;
	Raster1D_32FltBit(): Min(0), Max(1) {};

	virtual int GetPlanes(void) const {return(-32);};

	virtual DWORD GetValue1D(unsigned x) const;
	virtual double GetValue1Dd(unsigned x) const;
	virtual void SetValue1D(unsigned x, DWORD NewValue);
	virtual void SetValue1Dd(unsigned x, double NewValue);
#ifdef _REENTRANT
	virtual DWORD PTR_GetValue1D(const void *RAW_Data1D, unsigned x) const;
	virtual double PTR_GetValue1Dd(const void *RAW_Data1D, unsigned x) const;
	virtual void PTR_SetValue1D(void *RAW_Data1D, unsigned x, DWORD NewValue) const;
	virtual void PTR_SetValue1Dd(void *RAW_Data1D, unsigned x, double NewValue) const;
#endif
#ifdef OPTIMISE_SPEED
        virtual void Get(Raster1DAbstract &R1) const;
        virtual void Set(const Raster1DAbstract &R1);
#endif
	};


/** 64 bit planes - double */
class Raster1D_64FltBit: virtual public Raster1DAbstract
{
public: double Min, Max;
	Raster1D_64FltBit(): Min(0), Max(1) {};

	virtual int GetPlanes(void) const {return(-64);};

	virtual DWORD GetValue1D(unsigned x) const;
	virtual double GetValue1Dd(unsigned x) const;
	virtual void SetValue1D(unsigned x, DWORD NewValue);
	virtual void SetValue1Dd(unsigned x, double NewValue);
#ifdef _REENTRANT
	virtual DWORD PTR_GetValue1D(const void *RAW_Data1D, unsigned x) const;
	virtual double PTR_GetValue1Dd(const void *RAW_Data1D, unsigned x) const;
	virtual void PTR_SetValue1D(void *RAW_Data1D, unsigned x, DWORD NewValue) const;
	virtual void PTR_SetValue1Dd(void *RAW_Data1D, unsigned x, double NewValue) const;
#endif
#ifdef OPTIMISE_SPEED
        virtual void Get(Raster1DAbstract &R1) const;
        virtual void Set(const Raster1DAbstract &R1);
#endif
};


/*
class Raster1D_5BitRGB: virtual public Raster1D_16Bit, virtual public Raster1DAbstractRGB
{
public:
	unsigned R(unsigned x) const {return(((WORD*)Data1D)[x]&0x1F);}
	unsigned G(unsigned x) const {return((((WORD*)Data1D)[x]>>5)&0x1F);}
	unsigned B(unsigned x) const {return((((WORD*)Data1D)[x]>>10)&0x1F);}

};
*/


/** 8 bit planes for 3 channels R,G,B */
class Raster1D_8BitRGB: virtual public Raster1DAbstractRGB
	{
public:	Raster1D_8BitRGB(int InitSize1D)  {Allocate1D(InitSize1D);};
	Raster1D_8BitRGB(void)		{};

	virtual int GetPlanes(void) const {return(3*8);};
	virtual unsigned GetSize1D(void) const {return(Size1D);};

	virtual void Get(unsigned index, RGBQuad *RGB) const;
	//virtual void Set(unsigned index, const RGBQuad *RGB);

	virtual void Get24BitRGB(void *Buffer24Bit) const;

	virtual DWORD GetValue1DRAW(unsigned x) const {if(x<3*Size1D) return(((BYTE *)Data1D)[x]); return(0);};
	virtual void SetValue1DRAW(unsigned x, DWORD NewValue) {if(x<3*Size1D) ((BYTE *)Data1D)[x]=(BYTE)NewValue;};
#ifdef _REENTRANT
        virtual DWORD PTR_GetValue1DRAW(const void *RAW_Data1D, unsigned x) const 
            {if(x<3*Size1D) return(((BYTE *)RAW_Data1D)[x]); return(0);};
	virtual void PTR_SetValue1DRAW(void *RAW_Data1D, unsigned x, DWORD NewValue)
            {if(x<3*Size1D) ((BYTE *)RAW_Data1D)[x]=NewValue;};
        virtual DWORD PTR_GetValue1D(const void *RAW_Data1D, unsigned x) const; 
	virtual void PTR_SetValue1D(void *RAW_Data1D, unsigned x, DWORD NewValue) const;
#endif

	virtual DWORD GetValue1D(unsigned x) const;
	virtual void SetValue1D(unsigned x, DWORD NewValue);
	};


/** 16 bit planes for 3 channels R,G,B */
class Raster1D_16BitRGB: virtual public Raster1DAbstractRGB
	{
public:	Raster1D_16BitRGB(int InitSize1D)	{Allocate1D(InitSize1D);}
	Raster1D_16BitRGB(void)	{};

	virtual unsigned GetSize1D(void) const {return(Size1D);};
	virtual int GetPlanes(void) const {return(3*16);};

	virtual void Get(unsigned index, RGBQuad *RGB) const;
	//virtual void Set(unsigned index, const RGBQuad *RGB);

	virtual void Get24BitRGB(void *Buffer24Bit) const;

        virtual DWORD GetValue1DRAW(unsigned x) const {if(x<3*Size1D) return(((WORD *)Data1D)[x]); return(0);};
	virtual void SetValue1DRAW(unsigned x, DWORD NewValue) {if(x<3*Size1D) ((WORD *)Data1D)[x]=NewValue;};

	virtual DWORD GetValue1D(unsigned x) const;
	virtual void SetValue1D(unsigned x, DWORD NewValue);

#ifdef _REENTRANT
protected:
        virtual DWORD PTR_GetValue1DRAW(const void *RAW_Data1D, unsigned x) const 
            {if(x<3*Size1D) return(((WORD *)RAW_Data1D)[x]); return(0);};
	virtual void PTR_SetValue1DRAW(void *RAW_Data1D, unsigned x, DWORD NewValue)
            {if(x<3*Size1D) ((WORD *)RAW_Data1D)[x]=NewValue;};
        virtual DWORD PTR_GetValue1D(const void *RAW_Data1D, unsigned x) const; 
	virtual void PTR_SetValue1D(void *RAW_Data1D, unsigned x, DWORD NewValue) const;
#endif
	};


/** 8 bit planes for 4 channels R,G,B, A */
class Raster1D_8BitRGBA: virtual public Raster1DAbstractRGBA
	{
public:	Raster1D_8BitRGBA(int InitSize1D)  {Allocate1D(InitSize1D);};
	Raster1D_8BitRGBA(void)		{};

	virtual int GetPlanes(void) const {return(4*8);};
	virtual unsigned GetSize1D(void) const {return(Size1D);};

	virtual DWORD GetValue1DRAW(unsigned x) const {if(x<4*Size1D) return(((BYTE *)Data1D)[x]); return(0);};
	virtual void SetValue1DRAW(unsigned x, DWORD NewValue) {if(x<4*Size1D) ((BYTE *)Data1D)[x]=NewValue;};

	virtual DWORD GetValue1D(unsigned x) const;
	virtual void SetValue1D(unsigned x, DWORD NewValue);
#ifdef _REENTRANT
        virtual void PTR_SetValue1D(void *RAW_Data1D, unsigned x, DWORD NewValue) const;
protected:
        virtual DWORD PTR_GetValue1DRAW(const void *RAW_Data1D, unsigned x) const 
            {if(x<4*Size1D) return(((BYTE *)RAW_Data1D)[x]); return(0);};
	virtual void PTR_SetValue1DRAW(void *RAW_Data1D, unsigned x, DWORD NewValue) 
            {if(x<4*Size1D) ((BYTE *)RAW_Data1D)[x]=NewValue;};
#endif
	};


/** 16 bit planes for 4 channels R,G,B, A */
class Raster1D_16BitRGBA: virtual public Raster1DAbstractRGBA
	{
public:	Raster1D_16BitRGBA(int InitSize1D)	{Allocate1D(InitSize1D);}
	Raster1D_16BitRGBA(void)		{};

	virtual unsigned GetSize1D(void) const {return(Size1D);};
	virtual int GetPlanes(void) const {return(4*16);};

        virtual DWORD GetValue1DRAW(unsigned x) const {if(x<4*Size1D) return(((WORD *)Data1D)[x]);return(0);};
	virtual void SetValue1DRAW(unsigned x, DWORD NewValue) {if(x<4*Size1D) ((WORD *)Data1D)[x]=NewValue;};

	virtual DWORD GetValue1D(unsigned x) const;
	virtual void SetValue1D(unsigned x, DWORD NewValue);	

#ifdef _REENTRANT
        virtual void PTR_SetValue1D(void *RAW_Data1D, unsigned x, DWORD NewValue) const;
protected:
        virtual DWORD PTR_GetValue1DRAW(const void *RAW_Data1D, unsigned x) const 
            {if(x<4*Size1D) return(((WORD *)RAW_Data1D)[x]);return(0);};
	virtual void PTR_SetValue1DRAW(void *RAW_Data1D, unsigned x, DWORD NewValue) 
            {if(x<4*Size1D) ((WORD *)RAW_Data1D)[x]=NewValue;};
#endif
};


/********************* Specialised Raster 2D modules *********************/

class Raster2D_1Bit: virtual public Raster2DAbstract, public Raster1D_1Bit {};
class Raster2D_2Bit: virtual public Raster2DAbstract, public Raster1D_2Bit {};
class Raster2D_4Bit: virtual public Raster2DAbstract, public Raster1D_4Bit {};
class Raster2D_8Bit: virtual public Raster2DAbstract, public Raster1D_8Bit {};
class Raster2D_16Bit:virtual public Raster2DAbstract, public Raster1D_16Bit {};
class Raster2D_24Bit:virtual public Raster2DAbstract, public Raster1D_24Bit {};
class Raster2D_32Bit:virtual public Raster2DAbstract, public Raster1D_32Bit {};
#ifdef QWORD
class Raster2D_64Bit:virtual public Raster2DAbstract, public Raster1D_64Bit {};
#endif

class Raster2D_32FltBit:virtual public Raster2DAbstract,public Raster1D_32FltBit {};
class Raster2D_64FltBit:virtual public Raster2DAbstract,public Raster1D_64FltBit {};

class Raster2D_8BitRGB: virtual public Raster2DAbstractRGB, virtual public Raster1D_8BitRGB {};
class Raster2D_16BitRGB: virtual public Raster2DAbstractRGB, virtual public Raster1D_16BitRGB {};

class Raster2D_8BitRGBA: virtual public Raster2DAbstractRGBA, virtual public Raster1D_8BitRGBA {};
class Raster2D_16BitRGBA: virtual public Raster2DAbstractRGBA, virtual public Raster1D_16BitRGBA {};


/********************* Specialised Raster 3D modules *********************/
#ifdef RASTER_3D

class Raster3D_1Bit: virtual public Raster3DAbstract, public Raster2D_1Bit {};
class Raster3D_2Bit: virtual public Raster3DAbstract, public Raster2D_2Bit {};
class Raster3D_4Bit: virtual public Raster3DAbstract, public Raster2D_4Bit {};
class Raster3D_8Bit: virtual public Raster3DAbstract, public Raster2D_8Bit {};
class Raster3D_16Bit: virtual public Raster3DAbstract, public Raster2D_16Bit {};
class Raster3D_24Bit: virtual public Raster3DAbstract, public Raster2D_24Bit {};
class Raster3D_32Bit: virtual public Raster3DAbstract, public Raster2D_32Bit {};
#ifdef QWORD
class Raster3D_64Bit: virtual public Raster3DAbstract, public Raster2D_64Bit {};
#endif

class Raster3D_32FltBit:virtual public Raster3DAbstract, public Raster2D_32FltBit {};
class Raster3D_64FltBit:virtual public Raster3DAbstract, public Raster2D_64FltBit {};

#endif


#ifdef _MSC_VER 
 #pragma warning (pop)
#endif
#endif // _RAS_PROT_H_