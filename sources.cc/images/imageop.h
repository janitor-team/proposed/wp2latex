#ifndef __IMAGEOP_H__ 
#define __IMAGEOP_H__ 


void Convert2Gray(Image &I);

Image Mask(const Image &im_in, int size);


Image Peel8bit(const Image Img, int PlaneNum8);
Image PeelPlane(const Image Img, int PlaneNum);


#endif // __IMAGEOP_H__ 