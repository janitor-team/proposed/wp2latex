#ifndef _IMG_TOOL_H
#define _IMG_TOOL_H

#ifdef __cplusplus
extern "C" {
#endif

void swab16(unsigned char *block, int pixels);
void swab32(unsigned char *block, int pixels);
void swab64(unsigned char *block, int pixels);

void RGB_BGR(char *Data, int size);
void RGB_BGR2(unsigned char *OutData, const unsigned char *InData, unsigned PixelCount);
void RGB32_BGR24(unsigned char *OutData, const unsigned char *InData, unsigned PixelCount);
void RGB_Gray(unsigned char *OutData, const unsigned char *InData, unsigned PixelCount);
void RGB_Gray24(unsigned char *OutData, const unsigned char *InData, unsigned PixelCount);
void BGR_Gray24(unsigned char *OutData, const unsigned char *InData, unsigned PixelCount);
void BGR32_Gray24(unsigned char *OutData, const unsigned char *InData, unsigned PixelCount);
void RGB32_Gray(unsigned char *OutData, const unsigned char *InData, unsigned PixelCount);

void NotR(char *R, int size);
void YUV_RGB(unsigned char *OutData, const unsigned char *y, const unsigned char *u, const unsigned char *v, unsigned PixelCount);
void YUYV_RGB(unsigned char *OutData, const unsigned char *yuyv, unsigned PixelCount);

#ifdef __cplusplus
}
#endif


#endif	/* _IMG_TOOL_H */
