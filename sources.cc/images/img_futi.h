#ifndef _IMG_FUTI_H
#define _IMG_FUTI_H

#ifdef FileSize
 #if FileSize==1
  #undef FileSize
 #endif
#endif

#ifndef FileSize
long FileSize(FILE *f);
#endif


long ReadInt(FILE *F, char *pch=NULL, bool *isvalid=NULL);
double ReadDouble(FILE *F, char *pch=NULL);
void ReadWord(FILE *F, char *data, int SizeLimit, char *pch=NULL);
void readln(FILE *f, char *pch=NULL);

#ifdef __Stringa_h
string & fGets2(FILE *f, string & pstr, long MaxStrSize=0);
#endif


#endif	// _IMG_FUTI_H