/******************************************************************************
 * program:     rasimg library                                                *
 * function:    Module for JPG support                                        *
 * modul:       ras_jpg.cc                                                    *
 * licency:     GPL or LGPL                                                   *
 ******************************************************************************/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "imgsupp.h"


#define MaxTextExtent 256

#ifndef MIN
 #define MIN(a,b) ((a)<(b) ? (a) : (b))
#endif



///// JPG Reader /////

#if defined(SupportJPG) && SupportJPG>0

extern "C" {
#include <jpeglib.h>
#include <setjmp.h>
}

#include "typedfs.h"
#include "raster.h"


const char ExifPreffix[6] = {'E','x','i','f', '\0', '\0'};


struct my_error_mgr {
  struct jpeg_error_mgr pub;	/* "public" fields */

  jmp_buf setjmp_buffer;	/* for return to caller */
};

typedef struct my_error_mgr * my_error_ptr;
int JPG_quality = 75;


/* Here's the routine that will replace the standard error_exit method: */
METHODDEF(void) my_error_exit (j_common_ptr cinfo)
{
  /* cinfo->err really points to a my_error_mgr struct, so coerce pointer */
  my_error_ptr myerr = (my_error_ptr) cinfo->err;

  /* Always display the message. */
  /* We could postpone this until after returning, if we chose. */
  (*cinfo->err->output_message) (cinfo);

  /* Return control to the setjmp point */
  longjmp(myerr->setjmp_buffer, 1);
}


#if SupportJPG>=4 || SupportJPG==2

static const char xmp_std_header[]="http://ns.adobe.com/xap/1.0/";

static int GetCharacter(j_decompress_ptr jpeg_info)
{
  if (jpeg_info->src->bytes_in_buffer == 0)
    if ((!((*jpeg_info->src->fill_input_buffer)(jpeg_info))) ||
        (jpeg_info->src->bytes_in_buffer == 0))
      return EOF;
  jpeg_info->src->bytes_in_buffer--;
  return(GETJOCTET(*jpeg_info->src->next_input_byte++));
}


static inline int GetProfileLength(j_decompress_ptr jpeg_info)
{
  const int _c = GetCharacter(jpeg_info);
  if((_c != EOF) && (_c >= 0))
  {
    const int _c2 = GetCharacter(jpeg_info);
    if((_c2 != EOF) && (_c2 >= 0))
        return _c * 256 + _c2;
  }
  return 0;
}


static boolean ReadGenericProfile(j_decompress_ptr jpeg_info)
{
  size_t header_length=0, length;
  register size_t i;
  string profile_name;
  unsigned char *profile;
  int c, marker;

  if(jpeg_info->client_data==NULL) return TRUE;
  Image *Img = (Image *)jpeg_info->client_data;

	/* Determine length of generic profile. */
  length = GetProfileLength(jpeg_info);
  if(length <= 2) return TRUE;
  length -= 2;

  /*
    jpeg_info->unread_marker ('int') is either zero or the code of a
    JPEG marker that has been read from the data source, but has not
    yet been processed.  The underlying type for a marker appears to
    be UINT8 (JPEG_COM, or JPEG_APP0+n).

    Unexpected markers are prevented due to registering for specific
    markers we are interested in via jpeg_set_marker_processor().

    #define JPEG_APP0 0xE0
    #define JPEG_COM 0xFE

    #define ICC_MARKER  (JPEG_APP0+2)
    #define IPTC_MARKER  (JPEG_APP0+13)
    #define XML_MARKER  (JPEG_APP0+1)
   */
  marker = jpeg_info->unread_marker - JPEG_APP0;

	/* Compute generic profile name. */
  profile_name.printf("APP%d",marker);
//  FormatString(profile_name,"APP%d",marker);

	/* Obtain Image.*/
//  client_data=(MagickClientData *) jpeg_info->client_data;

	/* Copy profile from JPEG to allocated memory. */
//  profile = client_data->buffer;

  if(length < 6) return TRUE;
  for(i=0 ; i<6 ; i++)
  {
    c = GetCharacter(jpeg_info);
    if(c==EOF) return TRUE;
    if(c!=ExifPreffix[i]) return TRUE;
  }

  if(marker != 1)  return TRUE;

  length -= 6;

  PropertyItem *PropExif = new PropertyItem;
  PropExif->Data = (unsigned char *)malloc(length);
  if(PropExif->Data==NULL) return TRUE;
  PropExif->DataSize = length;

  for(i=0 ; i < length ; i++)
  {
    c = GetCharacter(jpeg_info);
    if(c==EOF)
    {
      delete PropExif;
      return TRUE;
    }
    ((char*)PropExif->Data)[i] = c;
  }
  PropExif->Name = ExifPreffix;
  Img->AttachProperty(PropExif);

/*
  else if(((marker == 1) && (length > strlen(xmp_std_header)+1)) &&
           (memcmp(profile, xmp_std_header, strlen(xmp_std_header)+1) == 0))
  {
	// XMP is required to fit in one 64KB chunk.  Strip off its JPEG namespace header.
     header_length = strlen(xmp_std_header)+1;
     //FormatString((char *) profile_name,"XMP");
  }
*/

  //if(profile) free(profile);
  return  TRUE;
}


Raster2DAbstract *LoadFragmentJPG(FILE *F)
{
struct jpeg_decompress_struct cinfo;
struct my_error_mgr jerr;
int row_stride;		/* physical row width in output buffer */
JSAMPARRAY buffer;	/* Output row buffer */
unsigned current_line = 0;
//int i;

  if(F==NULL) return NULL;

	/* Step 1: allocate and initialize JPEG decompression object */

  cinfo.err = jpeg_std_error(&jerr.pub);
  jerr.pub.error_exit = my_error_exit;
  /* Establish the setjmp return context for my_error_exit to use. */
  if(setjmp(jerr.setjmp_buffer))
     { /* If we get here, the JPEG code has signaled an error. We need to clean up the JPEG object, close the input file, and return. */
     jpeg_destroy_decompress(&cinfo);
     return NULL;
     }
  
  jpeg_create_decompress(&cinfo);	/* Now we can initialize the JPEG decompression object. */

	/* Step 2: specify data source (eg, a file) */
  jpeg_stdio_src(&cinfo, F);

/*
  for (i=1; i < 16; i++)
    if ((i != 2) && (i != 13) && (i != 14))
      jpeg_set_marker_processor(&cinfo,JPEG_APP0+i,ReadGenericProfile);
*/

	/* Step 3: read file parameters with jpeg_read_header() */
  (void) jpeg_read_header(&cinfo, TRUE);
  /* We can ignore the return value from jpeg_read_header since
   *   (a) suspension is not possible with the stdio data source, and
   *   (b) we passed TRUE to reject a tables-only JPEG file as an error.
   * See libjpeg.doc for more info. */

   Raster2DAbstract *Raster = CreateRaster2D(cinfo.image_width,cinfo.image_height,24);
   if(Raster==NULL) return NULL;

	/* Step 5: Start decompressor */
  (void)jpeg_start_decompress(&cinfo);

  row_stride = cinfo.output_width * cinfo.output_components;
  /* Make a one-row-high sample array that will go away when done with image */
  buffer = (*cinfo.mem->alloc_sarray)((j_common_ptr) &cinfo, JPOOL_IMAGE, row_stride, 1);

	/* Step 6: while (scan lines remain to be read) */
	/*           jpeg_read_scanlines(...); */

  /* Here we use the library's state variable cinfo.output_scanline as the
   * loop counter, so that we don't have to keep track ourselves. */
  while(cinfo.output_scanline < cinfo.output_height)
    { /* jpeg_read_scanlines expects an array of pointers to scanlines.
       * Here the array is only one element long, but you could ask for
       * more than one scanline at a time if that's more convenient. */
    (void)jpeg_read_scanlines(&cinfo, buffer, 1);
    
    int line_size = 3 * cinfo.output_width;	    /* Assume put_scanline_someplace wants a pointer and sample count. */

    if(line_size > row_stride) line_size = row_stride;

    if(current_line < Raster->Size2D)
		memcpy(Raster->GetRow(current_line), buffer[0], line_size);
    current_line++;

    //if AlineProc<>nil then AlineProc^.NextLine;

    //put_scanline_someplace(buffer[0], row_stride);
    }

	/* Step 7: Finish decompression */

  (void)jpeg_finish_decompress(&cinfo);
  /* We can ignore the return value since suspension is not possible with the stdio data source. */

	/* Step 8: Release JPEG decompression object */
	/* This is an important step since it will release a good deal of memory. */
  jpeg_destroy_decompress(&cinfo);

return Raster;
}


Image LoadPictureJPG(const char *Name)
{
Image Img;
unsigned current_line = 0;
    /* This struct contains the JPEG decompression parameters and pointers to
     * working space (which is allocated as needed by the JPEG library). */
struct jpeg_decompress_struct cinfo;
  /* We use our private extension JPEG error handler.
   * Note that this struct must live as long as the main JPEG parameter
   * struct, to avoid dangling-pointer problems. */
struct my_error_mgr jerr;
  /* More stuff */
FILE * infile;		/* source file */
JSAMPARRAY buffer;	/* Output row buffer */
int row_stride;		/* physical row width in output buffer */
int i;

  /* In this example we want to open the input file before doing anything else,
   * so that the setjmp() error recovery below can assume the file is open.
   * VERY IMPORTANT: use "b" option to fopen() if you are on a machine that
   * requires it in order to read binary files. */

  if ((infile = fopen(Name, "rb")) == NULL)
    {//fprintf(stderr, "can't open %s\n", filename);
    return Img;
    }

  /* Step 1: allocate and initialize JPEG decompression object */

  /* We set up the normal JPEG error routines, then override error_exit. */
  cinfo.err = jpeg_std_error(&jerr.pub);
  jerr.pub.error_exit = my_error_exit;
  /* Establish the setjmp return context for my_error_exit to use. */
  if (setjmp(jerr.setjmp_buffer))
     { /* If we get here, the JPEG code has signaled an error. We need to clean up the JPEG object, close the input file, and return. */
     jpeg_destroy_decompress(&cinfo);
     fclose(infile);
     return Img;
     }
     
  /* Now we can initialize the JPEG decompression object. */
  jpeg_create_decompress(&cinfo);

  /* Step 2: specify data source (eg, a file) */
  jpeg_stdio_src(&cinfo, infile);

  for (i=1; i < 16; i++)
    if ((i != 2) && (i != 13) && (i != 14))
      jpeg_set_marker_processor(&cinfo,JPEG_APP0+i,ReadGenericProfile);

  cinfo.client_data = &Img;

  /* Step 3: read file parameters with jpeg_read_header() */

  (void) jpeg_read_header(&cinfo, TRUE);
  /* We can ignore the return value from jpeg_read_header since
   *   (a) suspension is not possible with the stdio data source, and
   *   (b) we passed TRUE to reject a tables-only JPEG file as an error.
   * See libjpeg.doc for more info. */

   Raster2DAbstract *Raster = CreateRaster2D(cinfo.image_width,cinfo.image_height,24);
   if(Raster==NULL) goto ENDPROC;

  /* Step 4: set parameters for decompression */

  /* In this example, we don't need to change any of the defaults set by
   * jpeg_read_header(), so we do nothing here.
   */

  /* Step 5: Start decompressor */

  (void) jpeg_start_decompress(&cinfo);
  /* We can ignore the return value since suspension is not possible
   * with the stdio data source.  */

  /* We may need to do some setup of our own at this point before reading
   * the data.  After jpeg_start_decompress() we have the correct scaled
   * output image dimensions available, as well as the output colormap
   * if we asked for color quantization.
   * In this example, we need to make an output work buffer of the right size. */
  /* JSAMPLEs per row in output buffer */
  row_stride = cinfo.output_width * cinfo.output_components;
  /* Make a one-row-high sample array that will go away when done with image */
  buffer = (*cinfo.mem->alloc_sarray)((j_common_ptr) &cinfo, JPOOL_IMAGE, row_stride, 1);

  /* Step 6: while (scan lines remain to be read) */
  /*           jpeg_read_scanlines(...); */

  /* Here we use the library's state variable cinfo.output_scanline as the
   * loop counter, so that we don't have to keep track ourselves. */
  while(cinfo.output_scanline < cinfo.output_height)
    { /* jpeg_read_scanlines expects an array of pointers to scanlines.
       * Here the array is only one element long, but you could ask for
       * more than one scanline at a time if that's more convenient. */
    (void) jpeg_read_scanlines(&cinfo, buffer, 1);
    
    /* Assume put_scanline_someplace wants a pointer and sample count. */
    int line_size = 3 * cinfo.output_width;

    //{     BlockWrite(outfile, buffer^, row_stride);}

    if(line_size > row_stride) line_size = row_stride;

    if(current_line < Raster->Size2D)
		memcpy(Raster->GetRow(current_line), buffer[0], line_size);
    current_line++;

    //if AlineProc<>nil then AlineProc^.NextLine;

    //put_scanline_someplace(buffer[0], row_stride);
    }

  /* Step 7: Finish decompression */

  (void) jpeg_finish_decompress(&cinfo);
  /* We can ignore the return value since suspension is not possible
   * with the stdio data source. */

  /* Step 8: Release JPEG decompression object */
  /* This is an important step since it will release a good deal of memory. */
  jpeg_destroy_decompress(&cinfo);

  /* After finish_decompress, we can close the input file.
   * Here we postpone it until after no more JPEG errors are possible,
   * so as to simplify the setjmp error logic above.  (Actually, I don't
   * think that jpeg_destroy can do an error exit, but why assume anything...) */

ENDPROC:
  fclose(infile);
  /* At this point you may want to check to see whether any corrupt-data
   * warnings occurred (test whether jerr.pub.num_warnings is nonzero). */

  Img.AttachRaster(Raster);
  return(Img);  /* And we're done! */
}
#endif


#if SupportJPG>=3
int SavePictureJPG(const char *Name,const Image &Img)
{
  /* This struct contains the JPEG compression parameters and pointers to
   * working space (which is allocated as needed by the JPEG library).
   * It is possible to have several such structures, representing multiple
   * compression/decompression processes, in existence at once.  We refer
   * to any one struct (and its associated working data) as a "JPEG object".
   */
struct jpeg_compress_struct cinfo;
  /* This struct represents a JPEG error handler.  It is declared separately
   * because applications often want to supply a specialized error handler
   * (see the second half of this file for an example).  But here we just
   * take the easy way out and use the standard error handler, which will
   * print a message on stderr and call exit() if compression fails.
   * Note that this struct must live as long as the main JPEG parameter
   * struct, to avoid dangling-pointer problems. */
struct jpeg_error_mgr jerr;
  /* More stuff */
FILE * outfile;		 /* target file */
JSAMPROW row_pointer[1]; /* pointer to JSAMPLE row[s] */
int row_stride;		 /* physical row width in image buffer */

  if(Img.Raster==NULL) return(-10);

  /* Step 1: allocate and initialize JPEG compression object */

  /* We have to set up the error handler first, in case the initialization
   * step fails.  (Unlikely, but it could happen if you are out of memory.)
   * This routine fills in the contents of struct jerr, and returns jerr's
   * address which we place into the link field in cinfo. */
  cinfo.err = jpeg_std_error(&jerr);
  /* Now we can initialize the JPEG compression object. */
  jpeg_create_compress(&cinfo);

  /* Step 2: specify data destination (eg, a file) */
  /* Note: steps 2 and 3 can be done in either order. */

  /* Here we use the library-supplied code to send compressed data to a
   * stdio stream.  You can also write your own code to do something else.
   * VERY IMPORTANT: use "b" option to fopen() if you are on a machine that
   * requires it in order to write binary files. */
  if((outfile = fopen(Name, "wb")) == NULL) return(-1);
              //fprintf(stderr, "can't open %s\n", filename);

  jpeg_stdio_dest(&cinfo, outfile);

  /* Step 3: set parameters for compression */

  /* First we supply a description of the input image.
   * Four fields of the cinfo struct must be filled in: */
  cinfo.image_width = Img.Raster->GetSize1D(); /* image width and height, in pixels */
  cinfo.image_height = Img.Raster->Size2D;
  cinfo.input_components = 3;		/* # of color components per pixel */
  cinfo.in_color_space = JCS_RGB; 	/* colorspace of input image */
  /* Now use the library's routine to set default compression parameters.
   * (You must set at least cinfo.in_color_space before calling this,
   * since the defaults depend on the source color space.) */
  jpeg_set_defaults(&cinfo);
  /* Now you can set any non-default parameters you wish to.
   * Here we just illustrate the use of quality (quantization table) scaling:   */
  jpeg_set_quality(&cinfo, JPG_quality, TRUE /* limit to baseline-JPEG values */);

  /* Step 4: Start compressor */

  /* TRUE ensures that we will write a complete interchange-JPEG file.
   * Pass TRUE unless you are very sure of what you're doing.
   */
  jpeg_start_compress(&cinfo, TRUE);

  if(!Img.Properties.isEmpty())
  {
    for(int i=0; i<Img.Properties.PropCount; i++)
    {
      PropertyItem *Prop = Img.Properties.pProperties[i];
      if(Prop!=NULL && Prop->Data!=NULL && Prop->DataSize>0 && Prop->Name=="Exif")
      {
        int j;
		// First chunk is scattered into 2 pieces.
        jpeg_write_m_header(&cinfo, JPEG_APP0+1, (int)MIN(Prop->DataSize+6,65533L));
        for(j=0; j<6; j++) jpeg_write_m_byte (&cinfo, ExifPreffix[j]);
        for(j=0; j<(int)MIN(Prop->DataSize,65533L-6); j++)
          jpeg_write_m_byte (&cinfo, ((unsigned char*)Prop->Data)[j]);
        while(j<Prop->DataSize)
        {
          jpeg_write_marker(&cinfo, JPEG_APP0+1, (unsigned char*)Prop->Data+j, (int)MIN(Prop->DataSize-j,65533L));
          j += 65533L;
        }
      }
    }
  }

  /* Step 5: while (scan lines remain to be written) */
  /*           jpeg_write_scanlines(...); */

  /* Here we use the library's state variable cinfo.next_scanline as the
   * loop counter, so that we don't have to keep track ourselves.
   * To keep things simple, we pass one scanline per call; you can pass
   * more if you wish, though.
   */
  row_stride = Img.Raster->GetSize1D() * 3;	/* JSAMPLEs per row in image_buffer */

  char *ptrLineX = (char *)malloc(row_stride);
  Raster1DAbstract *RasLine;

  while (cinfo.next_scanline < cinfo.image_height)
    { /* jpeg_write_scanlines expects an array of pointers to scanlines.
       * Here the array is only one element long, but you could pass
       * more than one scanline at a time if that's more convenient. */
    RasLine=Img.Raster->GetRowRaster(cinfo.next_scanline);

    switch(Img.ImageType())
      {
      case ImagePalette:{                               //palette image
             RGBQuad RGB;
             BYTE *ptrLine=(BYTE *)ptrLineX;
             for(unsigned X=0;X<RasLine->GetSize1D();X++)
	       {
	       Img.Palette->Get(RasLine->GetValue1D(X),&RGB);
               *ptrLine++=RGB.R;
               *ptrLine++=RGB.G;
               *ptrLine++=RGB.B;
               }
             break;
             }
      case ImageGray:
      case ImageTrueColor:
      default:
             RasLine->Get24BitRGB(ptrLineX);    //Gray image
             break;
      }
       
    row_pointer[0] = (JSAMPLE *)ptrLineX;
    (void) jpeg_write_scanlines(&cinfo, row_pointer, 1);
    }
    
  if(ptrLineX) {free(ptrLineX);ptrLineX=NULL;}    

  /* Step 6: Finish compression */
  jpeg_finish_compress(&cinfo);
  /* After finish_compress, we can close the output file. */
  fclose(outfile);

  /* Step 7: release JPEG compression object */
  /* This is an important step since it will release a good deal of memory. */
  jpeg_destroy_compress(&cinfo);
          /* And we're done! */
  return 0;
}
#endif

#endif


