/*
   String functions that do not depend on the string object.
   This file fixes NULL problem inside standard functions like
   strcmp, strchr, strlen
*/
#include <stdio.h>

#include "std_str.h"


//This function fixes error in finding substring in NULL string
#ifndef __Safe_strchr

#ifdef __Strict_Const_Procs

const char *StrChr(const char *s, int c)
{
 if(s==NULL) return(NULL);
return(strchr(s,c));
}

char *StrChr(char *s, int c)
{
 if(s==NULL) return(NULL);
return(strchr(s,c));
}

#else
char *StrChr(const char *s, int c)
{
 if(s==NULL) return(NULL);
return(strchr(s,c));
}

#endif
#endif


#ifndef __Safe_strstr

#ifdef __Strict_Const_Procs

const char *StrStr(const char *str1,const char *str2)
{
 if((str1==NULL)||(str2==NULL)) return(NULL);
 return(strstr(str1,str2));
}

char *StrStr(char *str1,const char *str2)
{
 if((str1==NULL)||(str2==NULL)) return(NULL);
 return(strstr(str1,str2));
}

#else	// ~__Strict_Const_Procs

char *StrStr(const char *str1,const char *str2)
{
 if((str1==NULL)||(str2==NULL)) return(NULL);
 return(strstr(str1,str2));
}

#endif	// !__Strict_Const_Procs
#endif	// !__Safe_strstr

//This function fixes up the error in comparing two NULL string
#ifndef __Safe_strcmp
int StrCmp(const char *str1,const char *str2)
{
if(str1==NULL)
	{
	if(str2==NULL) return(0);
	if(*str2==0) return(0);
	return(-1);
	}
if(str2==NULL)
	{
	return(*str1!=0);	// if str1="" and str2==NULL return 0
	}
return(strcmp(str1,str2));
}
#endif


//This function fixes up the error in comparing two NULL string
#ifndef __Safe_strncmp
int StrNCmp(const char *str1,const char *str2,int str_size)
{
if(str1==NULL)
	   {
	   if(str2==NULL) return(0);
	   if(*str2==0) return(0);
	   return(-1);
	   }
if(str2==NULL) return(1);
return(strncmp(str1,str2,str_size));
}
#endif


//This function fixes up the error in measuring a length of the NULL string
#ifndef __Safe_strlen
unsigned StrLen(const char *str)
{
 if(str==NULL) return(0);
 return(strlen(str));
}
#endif


#ifndef __InlineStrdup
char *StrDup(const char *source)
{
 if(source==NULL) return NULL;
 return strdup(source);
}
#endif
