/****************************************************
* unit:    matrix           release 0.7             *
* purpose: implementation of concretized templates. *
* licency:   GPL or LGPL                            *
* Copyright: (c) 1998-2016 Jaroslav Fojtik          *
*****************************************************/
#include <stdio.h>
#include <math.h>

//#include "matrix.h"
#include "matrix.cc"

#define FLOAT float

 
#if (!defined(_MSC_VER)) && (!defined(__BORLANDC__)) || (__BORLANDC__ >= 0x0540) || (_MSC_VER >= 1310)
//New conventions for expanding templates
template class matrix<FLOAT>;

 #if !defined(__GNUC__)
  #define USE_MTX_T
 #else
  #if (__GNUC__ < 4)
   #define USE_MTX_T
  #endif
 #endif
   
 #ifdef USE_MTX_T
  template matrix<FLOAT>::matrix(void);
  template matrix<FLOAT>::matrix(TSIZE u1,TSIZE u2);
  template matrix<FLOAT>::matrix(TSIZE u1,TSIZE u2, FLOAT i);
  #undef USE_MTX_T
 #endif

#else

//Old conventions for expanding templates
void UseMTX_FLOAT(void)
{
 matrix<FLOAT> m;
 matrix<FLOAT> m2(0,0,(FLOAT)1.1);
 #ifdef _MSC_VER
 m=m2*m2;
 #endif
}
#endif
