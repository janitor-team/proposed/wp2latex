/*****************************************************************
* unit:    doublelists             release 0.5                   *
* purpose: general manipulation with array of couples of strings *
* Licency: GPL or LGPL                                           *
* Copyright: (c) 1998-2020 Jaroslav Fojtik                       *
******************************************************************/
#ifndef __DoubleList_h
#define __DoubleList_h


#ifndef __Lists_h
  #include "lists.h"
#endif

class doublelist:public list {
           int flipped;
           
   public:
	   doublelist(void): flipped(0) {};
           doublelist(const doublelist &dl);

	   char *Member(const int i, const int j);
           const char *Member(const int i, const int j) const;

	   //int operator+=(const char *str);
	   doublelist &operator=(const doublelist &dl);

	   void Add(const char *str1,const char *str2);

           void Flip(int FlipMode);
	   };


#endif
