;special characters
;    see for info: https://code.google.com/p/doctype-mirror/wiki/CharacterEntitiesC

  acute        Acute
  AElig        AE Digraph
  aelig        ae Digraph
  brvbar       Absolute Value (Divides)
  bull		Bullet
  cedil        Cedilla
  cent	       Cent
  copy         Copyright
  curren       General Currency Symbol
  deg          Degree
  ETH          Uppercase Eth
  eth          Lowercase Eth
  frac14       1/4
  frac12       1/2
  frac34       3/4
  frasl		Figure Slash (Fraction)
  hellip	Baseline Ellipses
  iexcl         Inverted Exclamation Point
  iquest        Inverted Question Mark
  laquo         Left Double Guillemet
  ldquo		Inverted Double Quote	;(Like 66)
  lsquo		Inverted Single Quote	;(Like 6)
  macr		Macron
  middot	Small Center Dot
  micro         Micro
  not	        Logical Not
  ordf          Feminine Spanish Ordinal
  ordm          Masculine Spanish Ordinal
  oline         Overline (Long Mark)
  para          Paragraph Sign
  plusmn        Plus or Minus
  pound         Pound/Sterling
  prime         Prime
  Prime         Double Prime
  raquo		Right Double Guillemet
  rdquo		Right Double Quote	;(Like 99)
  reg		Registered Trademark
  rsquo		Right Single Quote	;(Like 9)
  sect		Section Sign
  sup1		Power of 1
  sup2		Power of 2
  sup3		Power of 3
  szlig		German Double s
  THORN		Uppercase Thorn
  thorn		Lowercase Thorn
  times		Multiply (x)
  uml		Diaeresis (Umlaut)
  yen		Yen

  weierp	Weierstrass
  image         fraktur I
  real          fraktur R
  trade         Trademark
  alefsym	Hebrew Alef

  ldquor	Base Double Quote;	like 99
  rdquor	Right Double Quote;	Like 99

;accented characters

  Aacute       A Acute
  aacute       a Acute
  Eacute       E Acute
  eacute       e Acute
  Iacute       I Acute
  iacute       i Acute
  Oacute       O Acute
  oacute       o Acute
  Uacute       U Acute
  uacute       u Acute
  Yacute       Y Acute
  yacute       y Acute

  Ccedi1       C Cedilla
  ccedil       c Cedilla

  Acirc       A Circumflex
  acirc       a Circumflex
  Ecirc       E Circumflex
  ecirc       e Circumflex
  Icirc       I Circumflex
  icirc       i Circumflex
  Ocirc       O Circumflex
  ocirc       o Circumflex
  Ucirc       U Circumflex
  ucirc       u Circumflex
  Ycirc       Y Circumflex
  ycirc       y Circumflex
 
  Auml         A Diaeresis ;(Umlaut)
  auml         a Diaeresis ;(Umlaut)
  Euml         E Diaeresis ;(Umlaut)
  euml         e Diaeresis ;(Umlaut)
  Iuml         I Diaeresis ;(Umlaut)
  iuml         i Diaeresis ;(Umlaut)
  Ouml         O Diaeresis ;(Umlaut)
  ouml         o Diaeresis ;(Umlaut)
  Uuml         U Diaeresis ;(Umlaut)
  uuml         u Diaeresis ;(Umlaut)
  Yuml         Y Diaeresis ;(Umlaut)
  yuml         y Diaeresis ;(Umlaut)
 
  Agrave       A Grave
  agrave       a Grave
  Egrave       E Grave
  egrave       e Grave
  Igrave       I Grave
  igrave       i Grave
  Ograve       O Grave
  ograve       o Grave
  Ugrave       U Grave
  ugrave       u Grave
  Ygrave       Y Grave
  ygrave       y Grave
 
  Aring        A Ring
  aring        a Ring
  Uring        U Ring
  uring        u Ring

  Oslash       O Slash
  oslash       o Slash


  Atilde       A Tilde
  atilde       a Tilde
  Ntilde       N Tilde
  ntilde       n Tilde
  Otilde       O Tilde
  otilde       o Tilde



;greek characters
  alpha		alpha
  amp		Ampersand
  beta		beta
  chi		chi
  Chi		CHI
  copy		Copyright
  delta		delta
  epsilon	epsilon
  eta		eta
  gamma		gamma
  gt          Greater Than
  iota        iota
  kappa       kappa
  lambda      lambda
  lt          Less Than
  mu          mu
  nu          nu
  omega       omega
  omicron     omicron
  Omicron     OMICRON
  phi         phi
  pi          pi
  psi         psi
  quot        Double Quote
  rho         rho
  sigma       sigma
  tau         tau
  theta       theta
  upsilon     upsilon
  varepsilon  epsilon (Variant)
  varphi      phi (Variant)
  varpi       pi (Variant)
  varrho      rho (Variant)
  varsigma    sigma (Terminal)
  vtheta      theta (Variant)
  xi	      xi
  zeta        zeta

;Image symbols
  smilley	Happy Face
  sadsmilley	Sad Face
  clock		Clock
  telephone	Telephone
  endash	En Dash
  ndash		En Dash
  emdash	Em Dash
  mdash		Em Dash
  spades	Spade
  clubs		Club
  hearts	Heart
  diams		Diamond

;Math symbols
  and		Logical And
  asymp		Approximately Equal
  ang           Angle
  cap           Intersection
  cong          Congruent
  cup		Union
  cdots		Axis Ellipses
  ddots		Diagonal Ellipses
  dots		Baseline Ellipses
  empty         Empty Set
  exist		There Exists
  equiv		Equivalent
  forall	For All
  ge		Greater Than Or Equal
  inf		Infinity
  infin		Infinity
  int		Integral
  isin          Member ;(Element)
  le            Less Than Or Equal
  lowast	Asterisk Multiply
  minus		Minus
  nabla		Nabla (Gradient)
  ne		Not Equal
  ni            Such That (Contains as a Member)
  notin         Not a Member (Not an Element)
  nsub  	Not Subset
  or		Logical Or
  oplus		Circle Plus
  otimes        Circle Multiply
  part		Partial Derivative
  perp		Bottom
  prod		Product
  prop          Proportional
  radic		Bent Radical
  sdot          Small Solid Circle
  sim		Similar
  sub           Proper Subset
  sube          Reflex Subset (Contained In or Equals)
  sum		Summation
  sup           Proper Superset
  supe		Reflex Superset (Contains or Equals)
  there4	Therefore
  vdots		Vertical Ellipses

  larr		Horiz Arrow [Left]
  rarr		Horiz Arrow [Right]
  uarr   	Up Arrow
  darr		Down Arrow
  harr		Left and Right Arrow
  crarr		Down Arrow with corner left
  lArr		Horiz Dbl Arrow [Left]
  rArr		Horiz Dbl Arrow [Right]
  uArr   	Double Arrow Up
  dArr		Double Arrow Down
  harr		Double Arrow Left and Right

  lcub		Left Brace
  rcub		Right Brace
  lceil         Left Ceiling
  rceil		Right Ceiling
  lfloor        Left Floor
  rfloor        Right Floor
  lang          Left Angle Bracket (Bra)
  rang          Right Angle Bracket (Ket)








