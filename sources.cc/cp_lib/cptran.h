#ifndef __CP_Tran_h
#define __CP_Tran_h

#include "common.h"
#ifndef WORD
 #include "typedfs.h"
#endif


/// Parent codepage translator that maintains list of translators.
class CpTranslator
	{
private:static CpTranslator *First;
	CpTranslator *Previous;
	CpTranslator *Next;

public: explicit CpTranslator(const char *name);
	~CpTranslator();

	const char *Name;

	virtual WORD operator[](const WORD n) const {return(n);}
	virtual WORD number(void) const {return(0);}
        static CpTranslator *getFirst(void) {return(First);};
        CpTranslator *getNext(void) {return(Next);};

	friend CpTranslator *GetTranslator(const char *Name);
	};
CpTranslator *GetTranslator(const char *Name);	


/// Codepage translator that converts to wchar.
class WCpTranslator: public CpTranslator
	{
public: WCpTranslator(const char *name, WORD number, const WORD *trn);

	WORD Number;
	const WORD *Trn;

	virtual WORD operator[](const WORD n) const {if(n>=Number) return(0); return(Trn[n]);}
	WORD number(void) const {return(Number);}
	};


/// Codepage translator that converts to 8bit char.
class BCpTranslator: public CpTranslator
	{
public: BCpTranslator(const char *name, WORD number, const BYTE *trn);

	WORD Number;
	const BYTE *Trn;

	virtual WORD operator[](const WORD n) const {if(n>=Number) return(0); return(Trn[n]);}
	WORD number(void) const {return(Number);}
	};        

/// Codepage translator that converts to wchar where the space is not contiguous.
class WSparseCpTranslator: public CpTranslator
	{
public: WSparseCpTranslator(const char *name, BYTE banks, const WORD *const *trn_bank);

	BYTE Banks;
	const WORD * const* Trn_bank;

	virtual WORD operator[](const WORD n) const;
	WORD number(void) const {return(256*Banks);}
	};


/// Codepage translator that converts to wchar where the space is not contiguous.
class BSparseCpTranslator: public CpTranslator
	{
public: BSparseCpTranslator(const char *name, BYTE banks, const BYTE *const *trn_bank);

	BYTE Banks;
	const BYTE * const* Trn_bank;

	virtual WORD operator[](const WORD n) const;
	WORD number(void) const {return(256*Banks);}
	};


#endif