/******************************************************************************
 * program:     wp2latex                                                      *
 * function:    module for conversion Accent files into LaTeX 		      *
 * modul:       pass1acc.cc                                                   *
 * description: This module contains parser for Accent documents. It could be *
 *		optionally compiled with WP2LaTeX package.		      *
 * licency:     GPL		                                              *
 ******************************************************************************/
#include<stdio.h>
#include<string.h>
#include<stdlib.h>
#include<stringa.h>

#include<lists.h>
#include<dbllist.h>

#include"wp2latex.h"
#include"pass1xml.h"
#include "cp_lib/cptran.h"


/*Register translators here*/
class TconvertedPass1_Accent: public TconvertedPass1_XML
     {
public:
     virtual int Convert_first_pass(void);
     };
TconvertedPass1 *Factory_Accent(void) {return new TconvertedPass1_Accent;}
FFormatTranslator FormatAccent("Accent",Factory_Accent);

#define AccentVersion "0.4"


static void AccentImage(TconvertedPass1_XML *cq)
{
#ifdef DEBUG
  fprintf(cq->log,"\n#AccentImage() ");fflush(cq->log);
#endif
int i;
long len;
const char *TAG;
TBox Box;

  if(length(cq->TAG_Args)<1) return;
  TAG=cq->TAG_Args[0];
  if(TAG==NULL) return;
  if(strncmp(TAG,"GRAPHIC WMF",11)) return;
  TAG+=11;

  i=sscanf(TAG,"%ld",&len);
  if(len<0 || i<1)
     {
     if(cq->err != NULL)
	fprintf(cq->err,_("\nError: Length of embedded image is not specified!"));
     return;
     }

  fseek(cq->wpd,len,SEEK_CUR);  //Skip image - this might be improved

  Box.Width = -1; 		// Undefined, use default 100mm
  Box.Image_type=0;		//Image on disk
  Box.AnchorType = 0; 		/*0-Paragraph, 1-Page, 2-Character*/
  Box.HorizontalPos=2;		/*0-Left, 1-Right, 2-Center, 3-Full */
  Box.Image_size=0;

  ImageWP(cq, "dummy", Box);

}


static void ProcessKeyAccent(TconvertedPass1_XML *cq)
{
#ifdef DEBUG
  fprintf(cq->log,"\n#ProcessKeyAccent() ");fflush(cq->log);
#endif
string TAG;
const char *tag;
BYTE by,subby;

 *cq->ObjType=0;
 if(cq->TAG.isEmpty()) cq->ReadXMLTag();
 by=cq->by;
 subby=cq->subby;

 switch(by)
	{
	case XML_char:
               switch(cq->subby)                        //Normal character
		  {
		  case 10:
		  case 13:by=128;break; //CR
		  case  9:strcpy(cq->ObjType, "!Tab");
		  case 32:by=32;break;  //Space
		  }
	       break;
	case XML_extchar:
               if(cq->TAG.isEmpty()) break;			//Extended chatacter &xxx;
	       TAG=Ext_chr_str(cq->TAG[0],cq)+copy(cq->TAG,1,length(cq->TAG)-1);
	       tag=TAG();
	       by=201;
	       break;
	case XML_tag:
               TAG = copy(cq->TAG,1,length(cq->TAG)-2);	//Normal tag <xxx>
	       if((tag=TAG())==NULL) break;
	       if(TAG=="AC"){by=134;subby=0x82;break;}	//Justification Center
	       if(TAG=="AJ"){by=134;subby=0x81;break;}	//Justification Full
	       if(TAG=="AL"){by=134;subby=0x80;break;}	//Justification Left
	       if(TAG=="AR"){by=134;subby=0x83;break;}	//Justification Right
	       if(TAG=="B")
			{
			if(StrStr(cq->TAG_Args.Member(0,0),"/B"))
			     by=130;
			else by=129;
			subby=12;break;
			}   //ATTR on bold
	       if(TAG=="BIG")  {by=129;subby=2;break;}    //ATTR on large
	       if(TAG=="BR")   {by=131;break;} 	   	  //HRt
	       if(TAG=="DD")   {by=140;break;} 	   	  //Indented definition
	       if(TAG=="FLD")  {by=145;break;} 	   	  //Image
	       if(TAG=="H1")   {by=135;subby=1;break;}    //section level 1
	       if(TAG=="H2")   {by=135;subby=2;break;}    //section level 2
	       if(TAG=="H3")   {by=135;subby=3;break;}    //section level 3
	       if(TAG=="H4")   {by=135;subby=4;break;}    //section level 4
	       if(TAG=="H5")   {by=135;subby=1;break;}    //section level 5
	       if(TAG=="H6")   {by=135;subby=2;break;}    //section level 6
	       if(TAG=="HR")   {by=139;subby=0;break;}    //horizontal line
	       if(TAG=="I")				  //ATTR on italic
			{
			if(StrStr(cq->TAG_Args.Member(0,0),"/I"))
			     by=130;
			else by=129;
			subby=8;break;
			}
	       if(TAG=="IMG")  {by=142;break;}		  //Image
//	       if(TAG=="LI")   {by=144;subby=2;break;}    //Start of item
	       if(TAG=="META") {by=137;subby=0;break;}  //meta tag
	       if(TAG=="P")    {by=132;break;}		//new paragraph
	       if(TAG=="S")				//ATTR on strike out
			{
			if(StrStr(cq->TAG_Args.Member(0,0),"/S"))
			     by=130;
			else by=129;
			subby=13;break;
			}
	       if(TAG=="SMALL"){by=129;subby=3;break;}    //ATTR on small
	       if(TAG=="SCRIPT"){by=138;subby=0;break;}    //start SCRIPT
	       if(TAG=="SUB")  {by=129;subby=6;break;}    //ATTR on subscript
	       if(TAG=="SUP")  {by=129;subby=5;break;}    //ATTR on superscript
	       if(TAG=="T")    {by=143;subby=0;break;}    //Start of the table
	       if(TAG=="TD")   {by=143;subby=4;break;}    //Start of cell
	       if(TAG=="TH")   {by=143;subby=6;break;}    //Start of head cell
	       if(TAG=="TR")   {by=143;subby=2;break;}    //Start of row
	       if(TAG=="TT")   {by=129;subby=16;break;}	  //ATTR on typewriter
	       if(TAG=="U")				  //ATTR on underline
			{
			if(StrStr(cq->TAG_Args.Member(0,0),"/U"))
			     by=130;
			else by=129;
			subby=14;break;
			}
//	       if(TAG=="UL")   {by=144;subby=0;break;}    //Start of itemize
	       break;
	case XML_closetag:
               TAG = copy(cq->TAG,2,length(cq->TAG)-3);	//Closing tag </xxx>
	       if( (tag=TAG())==NULL) break;
	       if(TAG=="B")    {by=130;subby=12;break;}  //ATTR off bold
	       if(TAG=="BIG")  {by=130;subby=2;break;}   //ATTR off large
	       if(TAG=="DD")   {by=141;break;} 	   	  //End of indented definition
	       if(TAG=="DL")   {by=141;break;} 	   	  //End of definition list
	       if(TAG=="H1")   {by=136;subby=1;break;}    //section off level 1
	       if(TAG=="H2")   {by=136;subby=2;break;}    //section off level 2
	       if(TAG=="H3")   {by=136;subby=3;break;}    //section off level 3
	       if(TAG=="H4")   {by=136;subby=4;break;}    //section off level 4
	       if(TAG=="H5")   {by=136;subby=1;break;}    //section off level 5
	       if(TAG=="H6")   {by=136;subby=2;break;}    //section off level 6
//	       if(TAG=="LI")   {by=144;subby=3;break;}    //End of item
	       if(TAG=="I")    {by=130;subby=8;break;}    //ATTR off italic
	       if(TAG=="P")    {by=133;break;} 		  //end paragraph
	       if(TAG=="S")    {by=130;subby=13;break;}	  //ATTR off strike out
	       if(TAG=="SMALL"){by=130;subby=3;break;}    //ATTR off small
	       if(TAG=="SCRIPT"){by=138;subby=1;break;}   //end SCRIPT
	       if(TAG=="SUB")  {by=130;subby=6;break;}    //ATTR off subscript
	       if(TAG=="SUP")  {by=130;subby=5;break;}    //ATTR off superscript
	       if(TAG=="T")    {by=143;subby=1;break;}    //End of Table
	       if(TAG=="TD")   {by=143;subby=5;break;}	  //End of cell
	       if(TAG=="TH")   {by=143;subby=7;break;}    //End of head cell
	       if(TAG=="TR")   {by=143;subby=3;break;}    //End of row
	       if(TAG=="TT")   {by=130;subby=16;break;}	  //ATTR off typewriter
	       if(TAG=="U")    {by=130;subby=14;break;}   //ATTR off underline
//	       if(TAG=="UL")   {by=144;subby=1;break;}    //End of itemize
	       break;
        case XML_CDATA:
	case XML_comment:				//comment
	       break;
	case XML_badextchar:
               if(cq->TAG.isEmpty()) break;		//Extended chatacter &xxx
	       cq->TAG[length(cq->TAG)-1]=' ';
	       TAG = Ext_chr_str(cq->TAG[0],cq)+copy(cq->TAG,1,length(cq->TAG)-1);
	       tag = TAG();
	       by = 201;
	       break;
	}

  cq->by=by;
  cq->subby=subby;
  if(cq->flag<Nothing)
    switch(by)
	{
	case XML_char:                  //Normal character
                tag = Ext_chr_str(subby,cq,cq->ConvertCpg);
	        CharacterStr(cq,tag);
	        break;
        case XML_CDATA:
	case XML_comment:		//comment
                cq->CommentXML();
	        break;
	case XML_unicode:
                CharacterStr(cq,cq->TAG);
	        break;		//Already expanded unicode character

	case 32:putc(' ', cq->strip);   /*soft space*/
		break;

	case 128:if(cq->TablePos!=1 && cq->TablePos!=3)
		   if(cq->char_on_line)
			SoftReturn(cq);
		 break;
	case 129:AttrOn(cq->attr,subby);break;
	case 130:AttrOff(cq,subby);     break;
	case 131:HardReturn(cq);        break;
	case 132:if(cq->char_on_line) HardReturn(cq);  //Paragraph on
		 SoftReturn(cq);
		 break;
	case 133:if(cq->char_on_line) HardReturn(cq);  //Paragraph off
		 break;
	case 134:Justification(cq,subby);break;
	case 135:StartSection(cq,-subby);break;
	case 136:EndSection(cq,-subby);	 break;
//	case 137:MetaHTML(cq);		 break;
//	case 138:ScriptXML(cq);		 break;
	case 139:HLine(cq,-16);		 break;
//	case 140:IndentHTML(cq);	 break;
	case 141:End_of_indent(cq);	 break;
//	case 142:ImageHTML(cq);		 break;
	case 143:switch(subby)
		   {
//		   case 0:TableHTML(cq);cq->TablePos=1;break;
		   case 1:EndTable(cq);cq->TablePos=0; break;
		   case 2:RowTable(cq);cq->TablePos|=2;break;
		   case 3:cq->TablePos&=~2;break;
		   case 4:
		   case 6:CellTable(cq);cq->TablePos|=4;break;
		   case 5:
		   case 7:cq->TablePos&=~4;break;
		   }
		 break;
//	case 144:switch(subby)
//		   {
//		   case 0:ItemizeHTML(cq);break;
//		   }
//		 break;
	case 145:AccentImage(cq);break;

	case 200:fputc('~', cq->strip);strcpy(cq->ObjType, " ");
		 break;
	case 201:CharacterStr(cq,tag);break; //
	}


 cq->by=by;
 cq->subby=subby;
 if (cq->log != NULL)
    {   /**/
    if(by==128) fputc('\n',cq->log);
    else if(by==' ' || by==200) fputc(' ',cq->log);
    else if(by==0 || by==201)
	{
	fprintf(cq->log,"%s",tag);
	}
    else
	{
	fprintf(cq->log, _("\n%*s [%s %s]   "),
		  cq->recursion * 2, "", cq->TAG(), cq->ObjType);
//	if(*cq->ObjType==0) UnknownObjects++;
	}
    }

 cq->ActualPos = ftell(cq->wpd);
}


int TconvertedPass1_Accent::Convert_first_pass(void)
{
#ifdef DEBUG
  fprintf(log,"\n#TconvertedPass1_Accent::Convert_first_pass() ");fflush(log);
#endif
DWORD fsize;

  if(Verbosing >= 1)
     printf(_("\n>>>Accent2LaTeX<<< Conversion program: From Accent to LaTeX Version %s\n"
	      "      Made by J.Fojtik  (Hosted on WP2LaTeX :))))\n\n"),
			AccentVersion);
  ConvertHTML = GetTranslator("htmlTOinternal");
  CharReader = &ch_fgetc;

  TablePos=0;

  DocumentStart = ftell(wpd);
  fsize = FileSize(wpd);
  perc.Init(ftell(wpd), fsize,_("First pass Accent:") );

  ActualPos = ftell(wpd);
  while (ActualPos < fsize)
      {
      if(Verbosing >= 1)		//actualise a procentage counter
	      perc.Actualise(ActualPos);

      TAG.erase();
      ProcessKeyAccent(this);
      }

  Finalise_Conversion(this);
  return(1);
}

