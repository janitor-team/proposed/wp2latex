#ifndef __IMAGES_H__
#define __IMAGES_H__


// 1 point (Postscript) = 0.3527777778 mm = 1/72 inch.
// 1 m equals 2660 pt
// [WPGu] = [inch] / 1200

#define WPGu2PSu(x) ( (float)(x)*71/(25.4f*47.0f) )
#define PSu2WPGu(x) ( (float)(x)*(25.4f*47.0f)/(71.0f) )
#define WPGu2mm(x) ( (float)(x)*71*2.66f/(7.91f*25.4f*47.0f) )
#define mm2WPGu(x) ( (float)(x)*7.91f*25.4f*47.0f / (71.0f*2.66f) )


class TconvertedPass1;
class AbstractTransformXY;
float *LoadPoints(TconvertedPass1 *cq, int n, FloatBBox &bbx, AbstractTransformXY *TRx=NULL,bool Precision=false,float LineWidth=0);


void AddCharacterToContainer(TextContainer *pTextCont, WORD WchI, const CpTranslator *PsNativeCP, const CpTranslator *PsNativeSym, 
		TconvertedPass1 *cq, PS_State &PSS);

#endif // __IMAGES_H__