#ifndef __WMF_SUPPORT_H__
#define __WMF_SUPPORT_H__


/** This functor class is used for flipping image data. */
class vFlip: public AbstractTransformXY
{
public:
  vFlip(float Top, float Bottom) {TopPBottom=Top+Bottom;}

  virtual void ApplyTransform(float &x, float &y) const {y = -y+TopPBottom;}

  float TopPBottom;
};


////////////////////////////////////////////////


class ObjectTable
{
public:
  VectorAttribute **Attributes;
  unsigned Count;

  ObjectTable() {Count=0; Attributes=NULL;}
  ObjectTable(const ObjectTable &OT);
  ~ObjectTable();
  ObjectTable &operator=(const ObjectTable &OT);

  void Erase(void);
  void AddObject(VectorAttribute *NewAttr);
  void DeleteObject(unsigned ObjectIndex);
  const VectorAttribute *GetObjectAt(unsigned ObjectIndex) const;
};


////////////////////////////////////////////////


typedef  enum  
{ 
  MM_TEXT = 0x0001, 
  MM_LOMETRIC = 0x0002, 
  MM_HIMETRIC = 0x0003, 
  MM_LOENGLISH = 0x0004, 
  MM_HIENGLISH = 0x0005, 
  MM_TWIPS = 0x0006, 
  MM_ISOTROPIC = 0x0007, 
  MM_ANISOTROPIC = 0x0008 
} TMapMode;


/** Device context: A collection of properties and objects that defines a dynamic environment for 
processes on a device. For graphics output, properties include brush style, line style, text layout, 
foreground and background colors, and mapping mode; and objects include a brush, pen, font, 
palette, region, and transform matrix. Multiple device contexts can exist simultaneously, but a 
single device context specifies the environment for graphics output at a particular point in time. */
class DeviceContext_xMF
{
public:
     DeviceContext_xMF(const DeviceContext_xMF & BaseDC) {PSS=BaseDC.PSS; MapMode=BaseDC.MapMode; MixMode=BaseDC.MixMode;}
     DeviceContext_xMF() {MapMode=MM_ANISOTROPIC; MixMode=1;/*Transparent*/}
     DeviceContext_xMF &operator=(const DeviceContext_xMF & BaseDC);

     PS_State PSS;
     WORD MapMode;
     char MixMode;
};


class DeviceContextList: public DeviceContext_xMF
{
public:
    DeviceContextList(): DeviceContext_xMF() {Next=NULL;}
    DeviceContextList(const DeviceContext_xMF & BaseDC): DeviceContext_xMF(BaseDC) {Next=NULL;}

    DeviceContextList *Next;
};


class TconvertedPass1_xMF: public TconvertedPass1, public DeviceContext_xMF
{
protected:
     int NoConvertImage;

public:
     TconvertedPass1_xMF(void);
     ~TconvertedPass1_xMF();

     void PutImageIncluder(const char *NewFilename);
     void PleaseReport(const char *Feature) {TconvertedPass1::PleaseReport(Feature,"WMF");}
     void PushDC(void);
     void PopDC(void);

     SWORD YOffset, XOffset;
     SWORD YExtent, XExtent;
     
     Image Img;
     FloatBBox bbx;
     const CpTranslator *PsNativeCP;
     const CpTranslator *PsNativeSym;
     
     ObjectTable ObjTab;
     DeviceContextList *DcList;
};



#endif
