/******************************************************************************
 * program:     wp2latex                                                      *
 * function:    Access to several charset converters.                         *
 * modul:       cp-trn.cc                                                     *
 * description: This modul initializes global objects of codepage translators.*
 *              They are normally accessible through cp-lib interface.        *
 * licency:     GPL		                                              *
 ******************************************************************************/
#include <stdio.h>

#include "cp_lib/cptran.h"

#include "cp_lib/out_dir/cpg.trn"
