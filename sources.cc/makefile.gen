# generic makefile for wp2latex for GCC (EMX)
# runs under DOS, linux, solaris and perhaps OS/2
#
TARGET_NAME = wp2latex
TARGET_DIR = ../bin/
GCC=gcc
GXX=gpp
FLAGS=-O2 -I./atoms/include/ -I./cp_lib/ -DFINAL #-Wall #-static
CXXFLAGS=-fno-rtti -fno-exceptions
OBJ=.o
#OSTYPE=linux-gnu
INST_PREFIX=/usr/local


# Uncomment next line for compile a debugging version of WP2LaTeX
#DEBUG=-DDEBUG -g
DEBUG+=-DERROR_HANDLER

# Uncomment next line for allowing a gettext multilanguage translation features
# This feature is not required for succesfull run of WP2LaTeX.
GETTEXT=-D__gettext__

# to get an error list under OS/2
# ERROR=2>&1 | tee >> errlist



#### OS autodetection part of makefile ####

ifeq ($(GETTEXT),-D__gettext__)
ifeq ($(OSTYPE),)		#-lintl is no longer needed under linux
LIBS=-lintl
endif 
IGETTEXTOBJ=igettext$(OBJ)
endif

#Nothing is an indentifier for DOS? Too bad.
ifeq ($(OSTYPE),)
ifeq ($(HOSTTYPE),iris4d)
OSTYPE = iris4d
LIBS=-lm
TARGET_DIR = ../bin/iris/
else
TARGET_DIR = ../bin/dos/
TARGET_NAME = wp2latex.exe
OSTYPE = dos
FLAGS+=-DSupportPNG=0
endif
endif


# for Linux - debian
ifeq ($(OSTYPE),linux-gnu)
TARGET_DIR = ../bin/linux/
LIBS=-lm
GCC = g++
GXX = g++
endif

# for 'linux' - slackware; 'Linux' - Red Hat
ifeq ($(OSTYPE),$(filter $(OSTYPE),linux Linux))
TARGET_DIR = ../bin/linux/
LIBS+= -lstdc++
endif
#end of Linux

# for MAC
ifeq ($(OSTYPE),darwin)
TARGET_DIR = ../bin/mac/
LIBS=-lm
GCC = g++
GXX = g++
endif

# for sun
ifeq ($(OSTYPE),solaris)
TARGET_DIR = ../bin/sun/
LIBS=-lm
endif
ifeq ($(OSTYPE),$(filter $(OSTYPE),solaris2.9 solaris2.10))
TARGET_DIR = ../bin/sun/
endif

ifeq ($(OSTYPE),sunos4)
TARGET_DIR = ../bin/sun/
LIBS=-lm -lstdc++
endif
#end of sun

#for OS/2
ifneq "$(OS2_SHELL)" ""
TARGET_DIR = ../bin/os2/
TARGET_NAME = wp2latex.exe
OSTYPE = os2
LIBS=-s -lstdcpp
endif

# for aix
ifeq ($(OSTYPE),aix)
TARGET_DIR = ../bin/aix/
LIBS=-lm
endif
#end of aix

# for MSDOS under djgpp
ifeq ($(OSTYPE),msdosdjgpp)
TARGET_DIR = ../bin/dos/
TARGET_NAME = wp2latex.exe
FLAGS+=-DPOINTER_SIZE=32
endif
#end of MSDOS under DJGPP


#library components
OBJECTS=cp_lib/cptran$(OBJ) 
OBJECTS+=images/raster$(OBJ) images/rasterc$(OBJ) images/img_tool$(OBJ) images/rasterut$(OBJ) images/filehnd$(OBJ)
OBJECTS+=images/vecimage$(OBJ) images/vecraster$(OBJ) images/error$(OBJ) images/imgsupp$(OBJ) images/ras_gif$(OBJ) images/img_futi$(OBJ)

#mandatory components
OBJECTS+=wp2latex$(OBJ) wp2l_lib$(OBJ) pass1$(OBJ) pass2$(OBJ) wp2lfuti$(OBJ) formulas$(OBJ) images$(OBJ) charactr$(OBJ) $(IGETTEXTOBJ)
OBJECTS+=wmfsupp$(OBJ)


OBJECTS+=word/bintree$(OBJ)
OBJECTS+=word/wvparser$(OBJ)



#################################################################
#note for user: You could comment out any conversion module to create shorter
#               wp2latex conversion modules for WordPerfect

include modules.mak


#JPG support - feel free to comment it out
LIBS+=-ljpeg
OBJECTS+=images/ras_jpg$(OBJ)


COMMONDEP=wp2latex.h atoms/include/*.h $(MAKEFILE_LIST)

######### End of definitions starting compile rules ###########

.SUFFIXES: .c .cc $(OBJ) .exe
.PHONY: default all static distclean generic clean install uninstall unix2dos dos2unix TG_WP TG_ALL


default: $(TARGET_NAME)


all:     $(TARGET_NAME)
#	$(TARGET_NAME) -i test/charactr.wp


.c$(OBJ): $(MAKEFILE_LIST)
	$(GCC) $(FLAGS) $(DEBUG) -c $*.c $(ERROR) -o $*$(OBJ) $(GETTEXT)

.cc$(OBJ): $(MAKEFILE_LIST)
	$(GXX) $(FLAGS) $(CXXFLAGS) $(DEBUG) -c $*.cc $(ERROR) -o $*$(OBJ) $(GETTEXT) 

$(OBJ).exe: $(MAKEFILE_LIST)
	$(GCC) $*$(OBJ) $(LIBS) $(ERROR)

$(TARGET_NAME): $(OBJECTS) atoms/libatoms.a | $(TARGET_DIR)
	$(GCC) $(GETTEXT) -o $(TARGET_NAME) $(OBJECTS) atoms/libatoms.a $(LIBS) $(LDFLAGS) $(ERROR)
	cp ./$(TARGET_NAME) $(TARGET_DIR)
	strip $(TARGET_DIR)$(TARGET_NAME)
	@echo BINARY STORED IN $(TARGET_DIR)$(TARGET_NAME)

static: $(OBJECTS) atoms/libatoms.a | $(TARGET_DIR)
	$(GCC) $(GETTEXT) -o $(TARGET_NAME) $(OBJECTS) atoms/libatoms.a $(LIBS) $(ERROR) -static
	cp ./$(TARGET_NAME) $(TARGET_DIR)
	strip $(TARGET_DIR)$(TARGET_NAME)


$(TARGET_DIR):
	mkdir $@


wp2latex$(OBJ): wp2latex.cc $(COMMONDEP)
wp2l_lib$(OBJ): wp2l_lib.cc $(COMMONDEP)
pass1$(OBJ):    pass1.cc    $(COMMONDEP)
pass1_1$(OBJ):  pass1_1.cc  $(COMMONDEP)
pass1_3$(OBJ):  pass1_3.cc  $(COMMONDEP) cp_lib/out_dir/macroman.trn
pass1_4$(OBJ):  pass1_4.cc  $(COMMONDEP)
pass1_5$(OBJ):  pass1.cc    $(COMMONDEP)
pass1_6$(OBJ):  pass1_6.cc  $(COMMONDEP)
pass1c45$(OBJ): pass1c45.cc $(COMMONDEP)
pass1acc$(OBJ): pass1acc.cc $(COMMONDEP)
pass1htm$(OBJ): pass1htm.cc $(COMMONDEP) cp_lib/out_dir/html.trn
pass1mtf$(OBJ): pass1mtf.cc $(COMMONDEP) cp_lib/out_dir/mtef.trn
pass1ole$(OBJ): pass1ole.cc $(COMMONDEP) cole/cole.h
pass1rtf$(OBJ): pass1rtf.cc $(COMMONDEP) images/raster.h
pass1602$(OBJ): pass1602.cc $(COMMONDEP) cp_lib/cptran.h
pass2$(OBJ):    pass2.cc    $(COMMONDEP)
wp2lfuti$(OBJ): wp2lfuti.cc $(COMMONDEP)
formulas$(OBJ): formulas.cc $(COMMONDEP)
images$(OBJ):   images.cc   $(COMMONDEP) images/raster.h images/ras_img.cc
charactr$(OBJ): charactr.cc $(COMMONDEP) cp_lib/charactr.cc_
igettext$(OBJ): igettext.cc $(COMMONDEP)

cp_lib/cptran$(OBJ): cp_lib/cptran.cc cp_lib/cptran.h cp_lib/out_dir/trn.trn 
cp-trn$(OBJ):   cp-trn.cc wp2latex.h cp_lib/out_dir/cpg.trn

ras_jpg$(OBJ):  $(MAKEFILE_LIST) images/ras_jpg.cc images/raster.h
images/rasterut$(OBJ): $(MAKEFILE_LIST) images/rasterut.cc images/raster.cc images/raster.h
images/vecimage$(OBJ): $(MAKEFILE_LIST) images/vecimage.cc images/*.h
images/vecraster$(OBJ): $(MAKEFILE_LIST) images/vecraster.cc images/*.h
images/imgsupp$(OBJ): $(MAKEFILE_LIST) images/imgsupp.cc images/imgsupp.h images/raster.h

atoms/libatoms.a: atoms/*.cc atoms/include/*.h
	$(MAKE) -C atoms libatoms.a

#atoms/lists$(OBJ):   atoms/lists.cc atoms/include/lists.h
#atoms/dbllist$(OBJ): atoms/dbllist.cc
#atoms/std_str$(OBJ): atoms/std_str.cc atoms/include/std_str.h
#atoms/strings$(OBJ): atoms/strings.cc atoms/include/stringa.h
#atoms/stack$(OBJ):   atoms/stack.cc atoms/include/stacks.h
#atoms/struct$(OBJ):  atoms/struct.c
#atoms/utf8$(OBJ):    atoms/utf8.c
#atoms/sets$(OBJ):    atoms/sets.cc atoms/include/sets.h
#atoms/mtx_impl$(OBJ):atoms/mtx_impl.cc atoms/include/matrix.cc atoms/include/matrix.h

cole/new_cole$(OBJ): cole/new_cole.c cole/cole.h

word/bintree$(OBJ):   word/bintree.c
word/wvparser$(OBJ):  word/wvparser.c

cp_lib/charactr.cc_:  cp_lib/chars.c_ 
	$(MAKE) -C cp_lib charactr.cc_

cp_lib/out_dir/trn.trn:
	$(MAKE) -C cp_lib out_dir/trn.trn

cp_lib/out_dir/html.trn:
	$(MAKE) -C cp_lib out_dir/html.trn

cp_lib/out_dir/cpg.trn:
	$(MAKE) -C cp_lib out_dir/cpg.trn

cp_lib/out_dir/macroman.trn:
	$(MAKE) -C cp_lib out_dir/macroman.trn

cp_lib/out_dir/mtef.trn:
	$(MAKE) -C cp_lib out_dir/mtef.trn


TG_WP:
	rm -f $(TARGET_NAME) *.exe
	cp mod_wp.mak modules.mak
	make
TG_ALL:
	rm -f $(TARGET_NAME) *.exe
	cp mod_x.mak modules.mak
	make



distclean: clean
	cp mod_x.mak modules.mak
	make -C cp_lib distclean
	make -C atoms distclean
	rm makefile
	cp makefile.gen makefile

generic:
	make -C atoms generic
	make -C cp_lib generic
	rm makefile
	cp makefile.gen makefile

clean:
	rm -f wp2latex *.exe *$(OBJ) *.obj *.OBJ *.DSK *.SYM *.bak *.BAK core *.~?? *.~? *.\$$\$$\$$ *.log *.tds *.dsk *.sym out.txt
	rm -f test/*.tex
	rm -f images/*$(OBJ) images/*.bak images/*.o images/*.bkp images/core
	rm -f cole/*$(OBJ) cole/*.obj ole/*.bak
	rm -f word/*$(OBJ) word/*.obj word/*.bak
	rm -f jobs/*$(OBJ) jobs/*.obj jobs/*.bak
	make -C cp_lib clean
	make -C atoms clean


# This works correctly only in Linux
install: $(TARGET_NAME)
	@echo "Installing binary file"
	cp $(TARGET_DIR)$(TARGET_NAME) $(INST_PREFIX)/bin


uninstall: 
	rm -f /usr/local/bin/$(TARGET_NAME)	 
	rm -f /usr/local/man/man1/wp2latex.1
	rm -f /usr/local/man/cat1/wp2latex.1.gz
#	rm -f /usr/share/texmf/tex/latex/wp2latex/*.*

unix2dos:
	unix2dos images.cc
	unix2dos charactr.cc

dos2unix:
	dos2unix images.cc
	dos2unix charactr.cc
