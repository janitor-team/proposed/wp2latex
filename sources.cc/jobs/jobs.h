/** Suport for parallel task processing on multiple CPUs.
 * (c) 2013 - 2021 Jaroslav Fojtik.
 * This code could be distributed under LGPL licency. */
#ifndef __JOBS_H__
#define __JOBS_H__


/// This type should be supported by InterlockedIncrement() function.
#ifdef _WIN32
  #define JOB_COUNT_TYPE	long
#else
  #define JOB_COUNT_TYPE	int
#endif


/// Base class wrapper that must be inherited in every job.
/// Job must implement its Run() method and destructor.
class JobBase
{
public:
  static volatile JOB_COUNT_TYPE JobCount;

  virtual void Run(void) = 0;	///< Job worker, this one is pure virtual.
  JobBase();
  virtual ~JobBase();
};


int InitJobs(int ThrCount);
int RunJob(JobBase *js);
void FinishJobs(int WaitMs = 5000);
void EndJobs(void);


#endif	// __JOBS_H__
