/******************************************************************************
 * program:     wp2latex                                                      *
 * function:    module with XML support prototypes for HTML, AbiWord and Accent*
 * modul:       pass1xml.h                                                   *
 * description: 							      *
 ******************************************************************************/
#ifndef __PASS1_XML_H__
#define __PASS1_XML_H__

class TconvertedPass1_XML: public TconvertedPass1
     {
public:
     DWORD (*CharReader)(FILE *F);
     CpTranslator *ConvertHTML;
     string TAG;
     doublelist TAG_Args;
     BYTE TablePos;

     void SelectTranslator(const char *CharSet);

     //cq->by = 0 (normal char), 1 (extended char &xxx;), 2 (tag <>), 3 (end tag </>), 4 comment, 5 unfinished &xxx, 6 expanded unicode, 127 fail
     void ReadXMLTag(bool MakeUpper=true);

     /*This function converts comment inside HTML*/
     void CommentXML(void);
     };


typedef enum
{
  XML_char = 0,     ///< normal char
  XML_extchar,
  XML_tag,
  XML_closetag,
  XML_comment,
  
  XML_badextchar,
  //XML_badspecchar,
  XML_unicode,

  XML_CDATA,
  
  XML_fail = 127,
} XML_STATUS;


#endif	//#define __PASS1_XML_H__
