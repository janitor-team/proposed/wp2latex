/********************************************************************************
 * program:     wp2latex                                                        *
 * function:    convert WordPerfect 1.x,2.x,3.x,4.x,5.x and 6.x files into LaTeX*
 * modul:       charactr.cc                                                     *
 * description: This modul contains descriptions of all extended characters     *
 *              and perform a conversion to their LaTeX ekvivalents.            *
 *                Currently there is a WP5.x character set and other charsets   *
 *              are converted into it.					        *
 * licency:     GPL		                                                *
 ********************************************************************************/
#include<stdio.h>
#include<stdlib.h>

#include<string.h>
#include<lists.h>
#include<sets.h>

#include "wp2latex.h"
#include "cp_lib/cptran.h"


CpTranslator *Convert2Target = NULL;

int UseGrStyle(const char ReqFlag)
{
  if((InputPS|1) == ReqFlag)
  {
    InputPS |= 1;
    return 1;
  }
  return 0;
}


#define UseStyle(Flag, LaTeX_str)  \
  if(Flag>=STYLE_NOTUSED) {Flag=STYLE_USED;return(LaTeX_str);}
  
#define Use2Styles(Flag1, LaTeX_str1, Flag2, LaTeX_str2)\
  if(Flag1>=STYLE_USED || (Flag1>=STYLE_NOTUSED && Flag2<=STYLE_NOTUSED))  \
		   {Flag1=STYLE_USED;return(LaTeX_str1);}  \
  if(Flag2>=STYLE_NOTUSED) {Flag2=STYLE_USED;return(LaTeX_str2);}

#define SWITCH_CYR(LaTeX_str) if(Cyrillic>=0) \
  { \
  cq->RequiredFont = FONT_CYRILLIC; /*Cyrillic font*/ \
  return LaTeX_str; \
  }

#define SWITCH_HEB if(cjHebrew>=0) \
  { \
  cq->RequiredFont=FONT_HEBREW;	  /*Hebrew font*/ \
  }


/** Get character froom User Character Set */
WORD UserCharSet(unsigned char char_code, TconvertedPass1 *cq)
{
  if(UserWPCharSet==NULL)
	{
	if(cq->err)
	  {         
	  cq->perc.Hide();
	  fprintf(cq->err, _("\nWarning: Please define /CurrentFontSet for user symbol %u!"), (unsigned)char_code);
	  }
	UnknownCharacters++;
	return 0xFFFF;
	}
  return (*UserWPCharSet)[char_code];
}


static char buffer[4];

const char *ExpandPlainChar(WORD wchar)
{
  buffer[0] = wchar;
  buffer[1] = 0;
  return buffer;
}


/** Return unicode character packed in ASCII. */
const char *ExpandUTF8(WORD wchar)
{
  if(wchar<=0x7F)
  {
    buffer[0] = wchar;
    buffer[1] = 0;
    return buffer;
  }

  if(wchar<=0x7FF)
  {
    buffer[0] = 0xC0 | (wchar>>6);
    buffer[1] = 0x80 | (wchar & 0x3F);
    buffer[2] = 0;
    return buffer;
  }

  buffer[0] = 0xC8 | (wchar>>12);
  buffer[1] = 0x80 | ((wchar>>6) & 0x3F);
  buffer[2] = 0x80 | (wchar & 0x3F);
  buffer[3] = 0;
  return buffer;

//static char buffer[17];
// sprintf(buffer,"\\unichar{\"%4.4X\"}", wchar);
// sprintf(buffer,"\\symbol{\"%4.4X}", wchar);
// return buffer;
}


/*This function expands an extended WP character into LaTEX sequence*/
#include "cp_lib/charactr.cc_"


#ifdef _1
unsigned short OutCodePage, WPcharset;
bool Cyrillic;
signed char Verbosing;
bool TexChars;


int main(void)
{
int i;
TconvertedPass1 cq;

Table_Init(lat);
for(i=0; i<255; i++)
	{
        printf("%d %d: %s\n\r",i,1,Ext_chr_str(1,i,&cq));
        }
printf("\n\r");

return(0);
}
#endif  /**/


/* End of charactr.cc. */
