#ifndef __WRD_TYPE__
#define __WRD_TYPE__


#ifndef _FILETIME_
#define _FILETIME_
/* 64 bit number of 100 nanoseconds intervals since January 1, 1601 */
    typedef struct {
	__u32 dwLowDateTime;
	__u32 dwHighDateTime;
    } FILETIME;
#endif				/* _FILETIME_ */

typedef struct {
	__u16 wIdent;			/* 0x0000 */
	__u16 nFib;			/* 0x0002 */
	__u16 nProduct;			/* 0x0004 */
	__u16 lid;			/* 0x0006 */
	__s16 pnNext;			/* 0x0008 */

	__u8  fDot:1;			/* Bitfield 0x0001 0x000A */
	__u8  fGlsy:1;			/* Bitfield 0x0002 */
	__u8  fComplex:1;		/* Bitfield 0x0004 */
	__u8  fHasPic:1;		/* Bitfield 0x0008 */
	__u8  cQuickSaves:4;		/* Bitfield 0x00F0 */
	__u8  fEncrypted:1;		/* Bitfield 0x0100 */
	__u8  fWhichTblStm:1;		/* Bitfield 0x0200 */
	__u8  fReadOnlyRecommended:1;	/* Bitfield 0x0400 */
	__u8  fWriteReservation:1;	/* Bitfield 0x0800 */
	__u8  fExtChar:1;		/* Bitfield 0x1000 */
	__u8  fLoadOverride:1;		/* Bitfield 0x2000 */
	__u8  fFarEast:1;		/* Bitfield 0x4000 */
	__u8  fCrypto:1;		/* Bitfield 0x8000 */
	__u16 nFibBack;//:16;		/* 0x000C */
	__u32 lKey;			/* 0x000E */
	__u8  envr;//:8;		/* 0x0012 */
	__u8  fMac:1;			/* Bitfield 0x01 0x0013 */
	__u8  fEmptySpecial:1;		/* Bitfield 0x02 */
	__u8  fLoadOverridePage:1;	/* Bitfield 0x04 */
	__u8  fFutureSavedUndo:1;	/* Bitfield 0x08 */
	__u8  fWord97Saved:1;		/* Bitfield 0x10 */
	__u8  fSpare0:3;		/* Bitfield 0xFE */
	__u16 chse;//:16;		/* 0x0014 *//*was chs */
	__u16 chsTables;		/* 0x0016 */
	__u32 fcMin;			/* 0x0018 */
	__u32 fcMac;			/* 0x001C */
	__u16 csw;			/* 0x0020 */
	__u16 wMagicCreated;		/* 0x0022 */
	__u16 wMagicRevised;		/* 0x0024 */
	__u16 wMagicCreatedPrivate;	/* 0x0026 */
	__u16 wMagicRevisedPrivate;	/* 0x0028 */
	__s16 pnFbpChpFirst_W6;		/* 0x002A */
	__s16 pnChpFirst_W6;		/* 0x002C */
	__s16 cpnBteChp_W6;		/* 0x002E */
	__s16 pnFbpPapFirst_W6;		/* 0x0030 */
	__s16 pnPapFirst_W6;		/* 0x0032 */
	__s16 cpnBtePap_W6;		/* 0x0034 */
	__s16 pnFbpLvcFirst_W6;		/* 0x0036 */
	__s16 pnLvcFirst_W6;		/* 0x0038 */
	__s16 cpnBteLvc_W6;		/* 0x003A */
	__s16 lidFE;			/* 0x003C */
	__u16 clw;			/* 0x003E */
	__s32 cbMac;			/* 0x0040 */
	__u32 lProductCreated;		/* 0x0044 */
	__u32 lProductRevised;		/* 0x0048 */
	__u32 ccpText;			/* 0x004C */
	__s32 ccpFtn;			/* 0x0050 */
	__s32 ccpHdr;			/* 0x0054 */
	__s32 ccpMcr;			/* 0x0058 */
	__s32 ccpAtn;			/* 0x005C */
	__s32 ccpEdn;			/* 0x0060 */
	__s32 ccpTxbx;			/* 0x0064 */
	__s32 ccpHdrTxbx;		/* 0x0068 */
	__s32 pnFbpChpFirst;		/* 0x006C */
	__s32 pnChpFirst;		/* 0x0070 */
	__s32 cpnBteChp;		/* 0x0074 */
	__s32 pnFbpPapFirst;		/* 0x0078 */
	__s32 pnPapFirst;		/* 0x007C */
	__s32 cpnBtePap;		/* 0x0080 */
	__s32 pnFbpLvcFirst;		/* 0x0084 */
	__s32 pnLvcFirst;		/* 0x0088 */
	__s32 cpnBteLvc;		/* 0x008C */
	__s32 fcIslandFirst;		/* 0x0090 */
	__s32 fcIslandLim;		/* 0x0094 */
	__u16 cfclcb;			/* 0x0098 */
	__s32 fcStshfOrig;		/* 0x009A */
	__u32 lcbStshfOrig;		/* 0x009E */
	__s32 fcStshf;			/* 0x00A2 */
	__u32 lcbStshf;			/* 0x00A6 */
	__s32 fcPlcffndRef;		/* 0x00AA */
	__u32 lcbPlcffndRef;		/* 0x00AE */
	__s32 fcPlcffndTxt;		/* 0x00B2 */
	__u32 lcbPlcffndTxt;		/* 0x00B6 */
	__s32 fcPlcfandRef;		/* 0x00BA */
	__u32 lcbPlcfandRef;		/* 0x00BE */
	__s32 fcPlcfandTxt;		/* 0x00C2 */
	__u32 lcbPlcfandTxt;		/* 0x00C6 */
	__s32 fcPlcfsed;		/* 0x00CA */
	__u32 lcbPlcfsed;		/* 0x00CE */
	__s32 fcPlcpad;			/* 0x00D2 */
	__u32 lcbPlcpad;		/* 0x00D6 */
	__s32 fcPlcfphe;		/* 0x00DA */
	__u32 lcbPlcfphe;		/* 0x00DE */
	__s32 fcSttbfglsy;		/* 0x00E2 */
	__u32 lcbSttbfglsy;		/* 0x00E6 */
	__s32 fcPlcfglsy;		/* 0x00EA */
	__u32 lcbPlcfglsy;		/* 0x00EE */
	__s32 fcPlcfhdd;		/* 0x00F2 */
	__u32 lcbPlcfhdd;		/* 0x00F6 */
	__s32 fcPlcfbteChpx;		/* 0x00FA */
	__u32 lcbPlcfbteChpx;		/* 0x00FE */
	__s32 fcPlcfbtePapx;		/* 0x0102 */
	__u32 lcbPlcfbtePapx;		/* 0x0106 */
	__s32 fcPlcfsea;		/* 0x010A */
	__u32 lcbPlcfsea;		/* 0x010E */
	__s32 fcSttbfffn;		/* 0x0112 */
	__u32 lcbSttbfffn;		/* 0x0116 */
	__s32 fcPlcffldMom;		/* 0x011A */
	__u32 lcbPlcffldMom;		/* 0x011E */
	__s32 fcPlcffldHdr;		/* 0x0122 */
	__u32 lcbPlcffldHdr;		/* 0x0126 */
	__s32 fcPlcffldFtn;		/* 0x012A */
	__u32 lcbPlcffldFtn;		/* 0x012E */
	__s32 fcPlcffldAtn;		/* 0x0132 */
	__u32 lcbPlcffldAtn;		/* 0x0136 */
	__s32 fcPlcffldMcr;		/* 0x013A */
	__u32 lcbPlcffldMcr;		/* 0x013E */
	__s32 fcSttbfbkmk;		/* 0x0142 */
	__u32 lcbSttbfbkmk;		/* 0x0146 */
	__s32 fcPlcfbkf;		/* 0x014A */
	__u32 lcbPlcfbkf;		/* 0x014E */
	__s32 fcPlcfbkl;		/* 0x0152 */
	__u32 lcbPlcfbkl;		/* 0x0156 */
	__s32 fcCmds;			/* 0x015A */
	__u32 lcbCmds;			/* 0x015E */
	__s32 fcPlcmcr;			/* 0x0162 */
	__u32 lcbPlcmcr;		/* 0x0166 */
	__s32 fcSttbfmcr;		/* 0x016A */
	__u32 lcbSttbfmcr;		/* 0x016E */
	__s32 fcPrDrvr;			/* 0x0172 */
	__u32 lcbPrDrvr;		/* 0x0176 */
	__s32 fcPrEnvPort;		/* 0x017A */
	__u32 lcbPrEnvPort;		/* 0x017E */
	__s32 fcPrEnvLand;		/* 0x0182 */
	__u32 lcbPrEnvLand;		/* 0x0186 */
	__s32 fcWss;			/* 0x018A */
	__u32 lcbWss;			/* 0x018E */
	__s32 fcDop;			/* 0x0192 */
	__u32 lcbDop;			/* 0x0196 */
	__s32 fcSttbfAssoc;		/* 0x019A */
	__u32 lcbSttbfAssoc;		/* 0x019E */
	__s32 fcClx;			/* 0x01A2 */
	__u32 lcbClx;			/* 0x01A6 */
	__s32 fcPlcfpgdFtn;		/* 0x01AA */
	__u32 lcbPlcfpgdFtn;		/* 0x01AE */
	__s32 fcAutosaveSource;		/* 0x01B2 */
	__u32 lcbAutosaveSource;	/* 0x01B6 */
	__s32 fcGrpXstAtnOwners;	/* 0x01BA */
	__u32 lcbGrpXstAtnOwners;	/* 0x01BE */
	__s32 fcSttbfAtnbkmk;		/* 0x01C2 */
	__u32 lcbSttbfAtnbkmk;		/* 0x01C6 */
	__s32 fcPlcdoaMom;		/* 0x01CA */
	__u32 lcbPlcdoaMom;		/* 0x01CE */
	__s32 fcPlcdoaHdr;		/* 0x01D2 */
	__u32 lcbPlcdoaHdr;		/* 0x01D6 */
	__s32 fcPlcspaMom;		/* 0x01DA */
	__u32 lcbPlcspaMom;		/* 0x01DE */
	__s32 fcPlcspaHdr;		/* 0x01E2 */
	__u32 lcbPlcspaHdr;		/* 0x01E6 */
	__s32 fcPlcfAtnbkf;		/* 0x01EA */
	__u32 lcbPlcfAtnbkf;		/* 0x01EE */
	__s32 fcPlcfAtnbkl;		/* 0x01F2 */
	__u32 lcbPlcfAtnbkl;		/* 0x01F6 */
	__s32 fcPms;			/* 0x01FA */
	__u32 lcbPms;			/* 0x01FE */
	__s32 fcFormFldSttbs;		/* 0x0202 */
	__u32 lcbFormFldSttbs;		/* 0x0206 */
	__s32 fcPlcfendRef;		/* 0x020A */
	__u32 lcbPlcfendRef;		/* 0x020E */
	__s32 fcPlcfendTxt;		/* 0x0212 */
	__u32 lcbPlcfendTxt;		/* 0x0216 */
	__s32 fcPlcffldEdn;		/* 0x021A */
	__u32 lcbPlcffldEdn;		/* 0x021E */
	__s32 fcPlcfpgdEdn;		/* 0x0222 */
	__u32 lcbPlcfpgdEdn;		/* 0x0226 */
	__s32 fcDggInfo;		/* 0x022A */
	__u32 lcbDggInfo;		/* 0x022E */
	__s32 fcSttbfRMark;		/* 0x0232 */
	__u32 lcbSttbfRMark;		/* 0x0236 */
	__s32 fcSttbCaption;		/* 0x023A */
	__u32 lcbSttbCaption;		/* 0x023E */
	__s32 fcSttbAutoCaption;	/* 0x0242 */
	__u32 lcbSttbAutoCaption;	/* 0x0246 */
	__s32 fcPlcfwkb;		/* 0x024A */
	__u32 lcbPlcfwkb;		/* 0x024E */
	__s32 fcPlcfspl;		/* 0x0252 */
	__u32 lcbPlcfspl;		/* 0x0256 */
	__s32 fcPlcftxbxTxt;		/* 0x025A */
	__u32 lcbPlcftxbxTxt;		/* 0x025E */
	__s32 fcPlcffldTxbx;		/* 0x0262 */
	__u32 lcbPlcffldTxbx;		/* 0x0266 */
	__s32 fcPlcfhdrtxbxTxt;		/* 0x026A */
	__u32 lcbPlcfhdrtxbxTxt;	/* 0x026E */
	__s32 fcPlcffldHdrTxbx;		/* 0x0272 */
	__u32 lcbPlcffldHdrTxbx;	/* 0x0276 */
	__s32 fcStwUser;		/* 0x027A */
	__u32 lcbStwUser;		/* 0x027E */
	__s32 fcSttbttmbd;		/* 0x0282 */
	__u32 cbSttbttmbd;		/* 0x0286 */
	__s32 fcUnused;			/* 0x028A */
	__u32 lcbUnused;		/* 0x028E */
	__s32 fcPgdMother;		/* 0x0292 */
	__u32 lcbPgdMother;		/* 0x0296 */
	__s32 fcBkdMother;		/* 0x029A */
	__u32 lcbBkdMother;		/* 0x029E */
	__s32 fcPgdFtn;			/* 0x02A2 */
	__u32 lcbPgdFtn;		/* 0x02A6 */
	__s32 fcBkdFtn;			/* 0x02AA */
	__u32 lcbBkdFtn;		/* 0x02AE */
	__s32 fcPgdEdn;			/* 0x02B2 */
	__u32 lcbPgdEdn;		/* 0x02B6 */
	__s32 fcBkdEdn;			/* 0x02BA */
	__u32 lcbBkdEdn;		/* 0x02BE */
	__s32 fcSttbfIntlFld;		/* 0x02C2 */
	__u32 lcbSttbfIntlFld;		/* 0x02C6 */
	__s32 fcRouteSlip;		/* 0x02CA */
	__u32 lcbRouteSlip;		/* 0x02CE */
	__s32 fcSttbSavedBy;		/* 0x02D2 */
	__u32 lcbSttbSavedBy;		/* 0x02D6 */
	__s32 fcSttbFnm;		/* 0x02DA */
	__u32 lcbSttbFnm;		/* 0x02DE */
	__s32 fcPlcfLst;		/* 0x02E2 */
	__u32 lcbPlcfLst;		/* 0x02E6 */
	__s32 fcPlfLfo;			/* 0x02EA */
	__u32 lcbPlfLfo;		/* 0x02EE */
	__s32 fcPlcftxbxBkd;		/* 0x02F2 */
	__u32 lcbPlcftxbxBkd;		/* 0x02F6 */
	__s32 fcPlcftxbxHdrBkd;		/* 0x02FA */
	__u32 lcbPlcftxbxHdrBkd;	/* 0x02FE */
	__s32 fcDocUndo;		/* 0x0302 */
	__u32 lcbDocUndo;		/* 0x0306 */
	__s32 fcRgbuse;			/* 0x030A */
	__u32 lcbRgbuse;		/* 0x030E */
	__s32 fcUsp;			/* 0x0312 */
	__u32 lcbUsp;			/* 0x0316 */
	__s32 fcUskf;			/* 0x031A */
	__u32 lcbUskf;			/* 0x031E */
	__s32 fcPlcupcRgbuse;		/* 0x0322 */
	__u32 lcbPlcupcRgbuse;		/* 0x0326 */
	__s32 fcPlcupcUsp;		/* 0x032A */
	__u32 lcbPlcupcUsp;		/* 0x032E */
	__s32 fcSttbGlsyStyle;		/* 0x0332 */
	__u32 lcbSttbGlsyStyle;		/* 0x0336 */
	__s32 fcPlgosl;			/* 0x033A */
	__u32 lcbPlgosl;		/* 0x033E */
	__s32 fcPlcocx;			/* 0x0342 */
	__u32 lcbPlcocx;		/* 0x0346 */
	__s32 fcPlcfbteLvc;		/* 0x034A */
	__u32 lcbPlcfbteLvc;		/* 0x034E */
	FILETIME ftModified;		/* 0x0352 */
	__s32 fcPlcflvc;		/* 0x035A */
	__u32 lcbPlcflvc;		/* 0x035E */
	__s32 fcPlcasumy;		/* 0x0362 */
	__u32 lcbPlcasumy;		/* 0x0366 */
	__s32 fcPlcfgram;		/* 0x036A */
	__u32 lcbPlcfgram;		/* 0x036E */
	__s32 fcSttbListNames;		/* 0x0372 */
	__u32 lcbSttbListNames;		/* 0x0376 */
	__s32 fcSttbfUssr;		/* 0x037A */
	__u32 lcbSttbfUssr;		/* 0x037E */

	/* Added for Word 2 */
	__u32 Spare;			/* 0x000E */
	__u16 rgwSpare0[3];		/* 0x0012 */
	__u32 fcSpare0;			/* 0x0024 */
	__u32 fcSpare1;			/* 0x0028 */
	__u32 fcSpare2;			/* 0x002C */
	__u32 fcSpare3;			/* 0x0030 */
	__u32 ccpSpare0;		/* 0x0048 */
	__u32 ccpSpare1;		/* 0x004C */
	__u32 ccpSpare2;		/* 0x0050 */

	__u32 ccpSpare3;		/* 0x0054 */
	__u32 fcPlcfpgd;		/* 0x0082 */
	__u16 cbPlcfpgd;		/* 0x0086 */

	__u32 fcSpare5;			/* 0x0130 */
	__u16 cbSpare5;			/* 0x0136 */
	__u32 fcSpare6;			/* 0x0130 */
	__u16 cbSpare6;			/* 0x0136 */
	__u16 wSpare4;			/* 0x013C */
	} FIB;

typedef enum {
	cbATRD = 30,
	cbANLD = 84,
	cbANLV = 16,
	cbASUMY = 4,
	cbASUMYI = 12,
	cbBTE = 4,
	cbBKD = 6,
	cbBKF = 4,
	cbBKL = 2,
	cbBRC = 4,
	cbBRC10 = 2,
	cbCHP = 136,
	cbDTTM = 4,
	cbDCS = 2,
	cbDOGRID = 10,
	cbDOPTYPOGRAPHY = 310,
	cbFSPA = 26,
	cbFIB = 898,
	cbLSPD = 4,
	cbOLST = 212,
	cbNUMRM = 128,
	cbPGD = 10,
	cbPHE = 12,
	cbPAP = 610,
	cbPCD = 8,
	/*
	   cbPLC
	 */
	cbPRM = 2,
	cbRS = 16,
	cbRR = 4,
	cbSED = 12,
	cbSEP = 704,
	cbSHD = 2,
	cbTBD = 1,
	cbTC = 20,
	cbTLP = 4,
	cbTAP = 1728,
	cbWKB = 12,
	cbLSTF = 28,
	cbFDOA = 6,
	cbFTXBXS = 22,

	cb7DOP = 88,

	cb6BTE = 2,
	cb6FIB = 682,
	cb6PHE = 6,
	cb6ANLD = 52,
	cb6BRC = 2,
	cb6DOP = 84,
	cb6PGD = 6,
	cb6TC = 10,
	cb6CHP = 42
    } cbStruct;

    typedef enum _SprmName {
	/*
	   these ones are ones I made up entirely to match
	   unnamed patterns in word 95 files, whose
	   purpose is currently unknown
	 */
	sprmTUNKNOWN1 = 0xD400,
	sprmPUNKNOWN2 = 0x2400,	/* word 7 0x39 */
	sprmPUNKNOWN3 = 0x2401,	/* word 7 0x3a */
	sprmPUNKNOWN4 = 0x4400,	/* word 7 0x3b */
	sprmCUNKNOWN5 = 0x4800,	/* word 7 0x6f */
	sprmCUNKNOWN6 = 0x4801,	/* word 7 0x70 */
	sprmCUNKNOWN7 = 0x4802,	/* word 7 0x71 */


	/*
	   these ones showed up in rgsprmPrm and are mostly
	   out of date i reckon
	 */
	sprmNoop = 0x0000,	/* this makes sense */
	sprmPPnbrRMarkNot = 0x0000,	/* never seen this one */

	/*
	   this subset were not listed in word 8, but i recreated them
	   from the word 8 guidelines and the original word 6, so
	   basically they will blow things up when ms decides to reuse them
	   in word 2000 or later versions, but what the hell...
	 */
	sprmCFStrikeRM = 0x0841,
	sprmPNLvlAnm = 0x240D,
	sprmCFtc = 0x483D,
	/*end subset */

	/*
	   one of the sprm's that shows up in word 6 docs is "0", which
	   appears to be either the pap.istd or just an index, seeing
	   as the word 6 people didn't list it, lets just ignore it.
	   as it only happens in word 6 docs, our code happens to
	   function fine in the current setup, but at some stage
	   im sure it will bite me hard
	 */

	sprmPIstd = 0x4600,
	sprmPIstdPermute = 0xC601,
	sprmPIncLvl = 0x2602,
	sprmPJc = 0x2403,
	sprmPFSideBySide = 0x2404,
	sprmPFKeep = 0x2405,
	sprmPFKeepFollow = 0x2406,
	sprmPFPageBreakBefore = 0x2407,
	sprmPBrcl = 0x2408,
	sprmPBrcp = 0x2409,
	sprmPIlvl = 0x260A,
	sprmPIlfo = 0x460B,
	sprmPFNoLineNumb = 0x240C,
	sprmPChgTabsPapx = 0xC60D,
	sprmPDxaRight = 0x840E,
	sprmPDxaLeft = 0x840F,
	sprmPNest = 0x4610,
	sprmPDxaLeft1 = 0x8411,
	sprmPDyaLine = 0x6412,
	sprmPDyaBefore = 0xA413,
	sprmPDyaAfter = 0xA414,
	sprmPChgTabs = 0xC615,
	sprmPFInTable = 0x2416,
	sprmPFTtp = 0x2417,
	sprmPDxaAbs = 0x8418,
	sprmPDyaAbs = 0x8419,
	sprmPDxaWidth = 0x841A,
	sprmPPc = 0x261B,
	sprmPBrcTop10 = 0x461C,
	sprmPBrcLeft10 = 0x461D,
	sprmPBrcBottom10 = 0x461E,
	sprmPBrcRight10 = 0x461F,
	sprmPBrcBetween10 = 0x4620,
	sprmPBrcBar10 = 0x4621,
	sprmPDxaFromText10 = 0x4622,
	sprmPWr = 0x2423,
	sprmPBrcTop = 0x6424,
	sprmPBrcLeft = 0x6425,
	sprmPBrcBottom = 0x6426,
	sprmPBrcRight = 0x6427,
	sprmPBrcBetween = 0x6428,
	sprmPBrcBar = 0x6629,
	sprmPFNoAutoHyph = 0x242A,
	sprmPWHeightAbs = 0x442B,
	sprmPDcs = 0x442C,
	sprmPShd = 0x442D,
	sprmPDyaFromText = 0x842E,
	sprmPDxaFromText = 0x842F,
	sprmPFLocked = 0x2430,
	sprmPFWidowControl = 0x2431,
	sprmPRuler = 0xC632,
	sprmPFKinsoku = 0x2433,
	sprmPFWordWrap = 0x2434,
	sprmPFOverflowPunct = 0x2435,
	sprmPFTopLinePunct = 0x2436,
	sprmPFAutoSpaceDE = 0x2437,
	sprmPFAutoSpaceDN = 0x2438,
	sprmPWAlignFont = 0x4439,
	sprmPFrameTextFlow = 0x443A,
	sprmPISnapBaseLine = 0x243B,
	sprmPAnld = 0xC63E,
	sprmPPropRMark = 0xC63F,
	sprmPOutLvl = 0x2640,
	sprmPFBiDi = 0x2441,
	sprmPFNumRMIns = 0x2443,
	sprmPCrLf = 0x2444,
	sprmPNumRM = 0xC645,
	sprmPHugePapx = 0x6645,
	sprmPHugePapx2 = 0x6646,	/* this is the one I have found in
					   the wild, maybe the doc is incorrect
					   in numbering it 6645 C. */
	sprmPFUsePgsuSettings = 0x2447,
	sprmPFAdjustRight = 0x2448,

	sprmCFRMarkDel = 0x0800,
	sprmCFRMark = 0x0801,
	sprmCFFldVanish = 0x0802,
	sprmCPicLocation = 0x6A03,
	sprmCIbstRMark = 0x4804,
	sprmCDttmRMark = 0x6805,
	sprmCFData = 0x0806,
	sprmCIdslRMark = 0x4807,
	sprmCChs = 0xEA08,
	sprmCSymbol = 0x6A09,
	sprmCFOle2 = 0x080A,
	sprmCIdCharType = 0x480B,
	sprmCHighlight = 0x2A0C,
	sprmCObjLocation = 0x680E,
	sprmCFFtcAsciSymb = 0x2A10,
	sprmCIstd = 0x4A30,
	sprmCIstdPermute = 0xCA31,
	sprmCDefault = 0x2A32,
	sprmCPlain = 0x2A33,
	sprmCKcd = 0x2A34,
	sprmCFBold = 0x0835,
	sprmCFItalic = 0x0836,
	sprmCFStrike = 0x0837,
	sprmCFOutline = 0x0838,
	sprmCFShadow = 0x0839,
	sprmCFSmallCaps = 0x083A,
	sprmCFCaps = 0x083B,
	sprmCFVanish = 0x083C,
	sprmCFtcDefault = 0x4A3D,
	sprmCKul = 0x2A3E,
	sprmCSizePos = 0xEA3F,
	sprmCDxaSpace = 0x8840,
	sprmCLid = 0x4A41,
	sprmCIco = 0x2A42,
	sprmCHps = 0x4A43,
	sprmCHpsInc = 0x2A44,
	sprmCHpsPos = 0x4845,
	sprmCHpsPosAdj = 0x2A46,
	sprmCMajority = 0xCA47,
	sprmCIss = 0x2A48,
	sprmCHpsNew50 = 0xCA49,
	sprmCHpsInc1 = 0xCA4A,
	sprmCHpsKern = 0x484B,
	sprmCMajority50 = 0xCA4C,
	sprmCHpsMul = 0x4A4D,
	sprmCYsri = 0x484E,
	sprmCRgFtc0 = 0x4A4F,
	sprmCRgFtc1 = 0x4A50,
	sprmCRgFtc2 = 0x4A51,
	sprmCCharScale = 0x4852,
	sprmCFDStrike = 0x2A53,
	sprmCFImprint = 0x0854,
	sprmCFSpec = 0x0855,
	sprmCFObj = 0x0856,
	sprmCPropRMark = 0xCA57,
	sprmCFEmboss = 0x0858,
	sprmCSfxText = 0x2859,
	sprmCFBiDi = 0x085A,
	sprmCFDiacColor = 0x085B,
	sprmCFBoldBi = 0x085C,
	sprmCFItalicBi = 0x085D,
	sprmCFtcBi = 0x4A5E,
	sprmCLidBi = 0x485F,
	sprmCIcoBi = 0x4A60,
	sprmCHpsBi = 0x4A61,
	sprmCDispFldRMark = 0xCA62,
	sprmCIbstRMarkDel = 0x4863,
	sprmCDttmRMarkDel = 0x6864,
	sprmCBrc = 0x6865,
	sprmCShd = 0x4866,
	sprmCIdslRMarkDel = 0x4867,
	sprmCFUsePgsuSettings = 0x0868,
	sprmCCpg = 0x486B,
	sprmCRgLid0 = 0x486D,
	sprmCRgLid1 = 0x486E,
	sprmCIdctHint = 0x286F,

	sprmPicBrcl = 0x2E00,
	sprmPicScale = 0xCE01,
	sprmPicBrcTop = 0x6C02,
	sprmPicBrcLeft = 0x6C03,
	sprmPicBrcBottom = 0x6C04,
	sprmPicBrcRight = 0x6C05,

	sprmScnsPgn = 0x3000,
	sprmSiHeadingPgn = 0x3001,
	sprmSOlstAnm = 0xD202,
	sprmSDxaColWidth = 0xF203,
	sprmSDxaColSpacing = 0xF204,
	sprmSFEvenlySpaced = 0x3005,
	sprmSFProtected = 0x3006,
	sprmSDmBinFirst = 0x5007,
	sprmSDmBinOther = 0x5008,
	sprmSBkc = 0x3009,
	sprmSFTitlePage = 0x300A,
	sprmSCcolumns = 0x500B,
	sprmSDxaColumns = 0x900C,
	sprmSFAutoPgn = 0x300D,
	sprmSNfcPgn = 0x300E,
	sprmSDyaPgn = 0xB00F,
	sprmSDxaPgn = 0xB010,
	sprmSFPgnRestart = 0x3011,
	sprmSFEndnote = 0x3012,
	sprmSLnc = 0x3013,
	sprmSGprfIhdt = 0x3014,
	sprmSNLnnMod = 0x5015,
	sprmSDxaLnn = 0x9016,
	sprmSDyaHdrTop = 0xB017,
	sprmSDyaHdrBottom = 0xB018,
	sprmSLBetween = 0x3019,
	sprmSVjc = 0x301A,
	sprmSLnnMin = 0x501B,
	sprmSPgnStart = 0x501C,
	sprmSBOrientation = 0x301D,
	sprmSBCustomize = 0x301E,
	sprmSXaPage = 0xB01F,
	sprmSYaPage = 0xB020,
	sprmSDxaLeft = 0xB021,
	sprmSDxaRight = 0xB022,
	sprmSDyaTop = 0x9023,
	sprmSDyaBottom = 0x9024,
	sprmSDzaGutter = 0xB025,
	sprmSDmPaperReq = 0x5026,
	sprmSPropRMark = 0xD227,
	sprmSFBiDi = 0x3228,
	sprmSFFacingCol = 0x3229,
	sprmSFRTLGutter = 0x322A,
	sprmSBrcTop = 0x702B,
	sprmSBrcLeft = 0x702C,
	sprmSBrcBottom = 0x702D,
	sprmSBrcRight = 0x702E,
	sprmSPgbProp = 0x522F,
	sprmSDxtCharSpace = 0x7030,
	sprmSDyaLinePitch = 0x9031,
	sprmSClm = 0x5032,
	sprmSTextFlow = 0x5033,

	sprmTJc = 0x5400,
	sprmTDxaLeft = 0x9601,
	sprmTDxaGapHalf = 0x9602,
	sprmTFCantSplit = 0x3403,
	sprmTTableHeader = 0x3404,
	sprmTTableBorders = 0xD605,
	sprmTDefTable10 = 0xD606,
	sprmTDyaRowHeight = 0x9407,
	sprmTDefTable = 0xD608,
	sprmTDefTableShd = 0xD609,
	sprmTTlp = 0x740A,
	sprmTFBiDi = 0x560B,
	sprmTHTMLProps = 0x740C,
	sprmTSetBrc = 0xD620,
	sprmTInsert = 0x7621,
	sprmTDelete = 0x5622,
	sprmTDxaCol = 0x7623,
	sprmTMerge = 0x5624,
	sprmTSplit = 0x5625,
	sprmTSetBrc10 = 0xD626,
	sprmTSetShd = 0x7627,
	sprmTSetShdOdd = 0x7628,
	sprmTTextFlow = 0x7629,
	sprmTDiagLine = 0xD62A,
	sprmTVertMerge = 0xD62B,
	sprmTVertAlign = 0xD62C
    } SprmName;


typedef struct _Sprm {	/*16 bits in total */
	unsigned ispmd:9;		/*ispmd unique identifier within sgc group */
	unsigned fSpec:1;		/*fSpec sprm requires special handling */
	unsigned sgc:3;		/*sgc   sprm group; type of sprm (PAP, CHP, etc) */
	unsigned spra:3;		/*spra  size of sprm argument */
    } Sprm;


typedef struct _PRM {	/*full total of bits should be 16 */
	__u16 fComplex:1;
	union {
	    struct {
		__u16 isprm:7;
		__u16 val:8;
	    } var1;
	    struct {
		__u16 igrpprl:15;
	    } var2;
	} para;
    } PRM;

typedef struct _PCD {	/*this should be 16 bits for bitfields */
	__u16 fNoParaLast:1;
	__u16 fPaphNil:1;
	__u16 fCopied:1;
	__u16 reserved:5;
	__u16 fn:8;
	__u32 fc;
	PRM prm;
    } PCD;

typedef struct _CLX {
	PCD *pcd;
	__u32 *pos;
	__u32 nopcd;

	__u16 grpprl_count;
	__u16 *cbGrpprl;
	__u8 **grpprl;
    } CLX;

typedef struct _BRC {
	BYTE dptLineWidth;
	BYTE brcType;
	BYTE ico;
	BYTE dptSpace:5;
	BYTE fShadow:1;
	BYTE fFrame:1;
	BYTE reserved:1;
    } BRC;

typedef struct _DTTM {
	unsigned mint:6;
	unsigned hr:5;
	unsigned dom:5;
	unsigned mon:4;
	unsigned yr:9;
	unsigned wdy:3;
    } DTTM;


typedef struct _ANLV {
	__u8 nfc;
	__u8 cxchTextBefore;

	unsigned cxchTextAfter:8;
	unsigned jc:2;
	unsigned fPrev:1;
	unsigned fHang:1;
	unsigned fSetBold:1;
	unsigned fSetItalic:1;
	unsigned fSetSmallCaps:1;
	unsigned fSetCaps:1;
	unsigned fSetStrike:1;
	unsigned fSetKul:1;
	unsigned fPrevSpace:1;
	unsigned fBold:1;
	unsigned fItalic:1;
	unsigned fSmallCaps:1;
	unsigned fCaps:1;
	unsigned fStrike:1;
	unsigned kul:3;
	unsigned ico:5;

	__s16 ftc;
	__u16 hps;
	__u16 iStartAt;
	__u16 dxaIndent;
	__u16 dxaSpace;
    } ANLV;

typedef __u16 XCHAR;

typedef struct _OLST {
	ANLV rganlv[9];
	__u8 fRestartHdr;
	__u8 fSpareOlst2;
	__u8 fSpareOlst3;
	__u8 fSpareOlst4;
	XCHAR rgxch[64];
    } OLST;


typedef struct _SEP {
	__u8 bkc;
	__u8 fTitlePage;
	__s8 fAutoPgn;
	__u8 nfcPgn;
	__u8 fUnlocked;
	__u8 cnsPgn;
	__u8 fPgnRestart;
	__u8 fEndNote;
	__u8 lnc;
	__s8 grpfIhdt;
	__u16 nLnnMod;
	__s32 dxaLnn;
	__s16 dxaPgn;
	__s16 dyaPgn;
	__s8 fLBetween;
	__s8 vjc;
	__u16 dmBinFirst;
	__u16 dmBinOther;
	__u16 dmPaperReq;
	BRC brcTop;
	BRC brcLeft;
	BRC brcBottom;
	BRC brcRight;
	__s16 fPropRMark;
	__s16 ibstPropRMark;
	DTTM dttmPropRMark;
	__s32 dxtCharSpace;
	__s32 dyaLinePitch;
	__u16 clm;
	__s16 reserved1;
	__u8 dmOrientPage;
	__u8 iHeadingPgn;
	__u16 pgnStart;
	__s16 lnnMin;
	__s16 wTextFlow;
	__s16 reserved2;

	WORD pgbProp;		//:16;
	BYTE pgbApplyTo:3;
	BYTE pgbPageDepth:2;
	BYTE pgbOffsetFrom:3;
	BYTE reserved:8;

	__u32 xaPage;
	__u32 yaPage;
	__u32 xaPageNUp;
	__u32 yaPageNUp;
	__u32 dxaLeft;
	__u32 dxaRight;
	__s32 dyaTop;
	__s32 dyaBottom;
	__u32 dzaGutter;
	__u32 dyaHdrTop;
	__u32 dyaHdrBottom;
	__s16 ccolM1;
	__s8 fEvenlySpaced;
	__s8 reserved3;
	__s32 dxaColumns;
	__s32 rgdxaColumnWidthSpacing[89];
	__s32 dxaColumnWidth;
	__u8 dmOrientFirst;
	__u8 fLayout;
	__s16 reserved4;
	OLST olstAnm;
    } SEP;

typedef struct _SED {
	__s16 fn;
	__u32 fcSepx;
	__s16 fnMpr;
	__u32 fcMpr;
    } SED;


typedef __u16 FTC;

/* STSHI: STyleSHeet Information, as stored in a file
  Note that new fields can be added to the STSHI without invalidating
  the file format, because it is stored preceded by it's length.
  When reading a STSHI from an older version, new fields will be zero. */
typedef struct _STSHI {
	__u16 cstd;		/* Count of styles in stylesheet */
	__u16 cbSTDBaseInFile;	/* Length of STD Base as stored in a file */
	unsigned fStdStylenamesWritten:1;	/* Are built-in stylenames stored? */
	unsigned reserved:15;	/* Spare flags */
	WORD stiMaxWhenSaved;	/* Max sti known when this file was written */
	__u16 istdMaxFixedWhenSaved;	/* How many fixed-index istds are there? */
	__u16 nVerBuiltInNamesWhenSaved;	/* Current version of built-in stylenames */
	FTC rgftcStandardChpStsh[3];	/* ftc used by StandardChpStsh for this document */
    } STSHI;


typedef union _UPX {
	struct {
	    __u8 *grpprl;
	} chpx;
	struct {
	    __u16 istd;
	    __u8 *grpprl;
	} papx;
	__u8 *rgb;
    } UPX;


typedef struct _LSPD {
       __s16 dyaLine;
       __s16 fMultLinespace;
    } LSPD;

typedef union _PHE {
	struct {
	    unsigned fSpare:1;
	    unsigned fUnk:1;
	    unsigned fDiffLines:1;
	    unsigned reserved1:5;
	    unsigned clMac:8;
	    unsigned reserved2:16;
	    __s32 dxaCol;
	    __s32 dymHeight;	/*also known as dymLine and dymTableHeight in docs */
	} var1;
	struct {
	    unsigned fSpare:1;
	    unsigned fUnk:1;
	    //unsigned dcpTtpNext:30;
	    DWORD dcpTtpNext;
	    __s32 dxaCol;
	    __s32 dymHeight;	/*also known as dymLine and dymTableHeight in docs */
	} var2;
    } PHE;


typedef struct _TLP {
	int itl:16;
	unsigned fBorders:1;
	unsigned fShading:1;
	unsigned fFont:1;
	unsigned fColor:1;
	unsigned fBestFit:1;
	unsigned fHdrRows:1;
	unsigned fLastRow:1;
	unsigned fHdrCols:1;
	unsigned fLastCol:1;
	unsigned unused:7;
    } TLP;


#define itcMax 64

typedef struct _TC {
	unsigned fFirstMerged:1;
	unsigned fMerged:1;
	unsigned fVertical:1;
	unsigned fBackward:1;
	unsigned fRotateFont:1;
	unsigned fVertMerge:1;
	unsigned fVertRestart:1;
	unsigned vertAlign:2;
	unsigned fUnused:7;
	unsigned wUnused:16;
	BRC brcTop;
	BRC brcLeft;
	BRC brcBottom;
	BRC brcRight;
    } TC;


typedef struct _SHD {
	/*16 bits in total */
	unsigned icoFore:5;
	unsigned icoBack:5;
	unsigned ipat:6;
    } SHD;


typedef struct _TAP {
	__s16 jc;
	__s32 dxaGapHalf;
	__s32 dyaRowHeight;
	__u8 fCantSplit;
	__u8 fTableHeader;
	TLP tlp;
	__s32 lwHTMLProps;
	unsigned fCaFull:1;
	unsigned fFirstRow:1;
	unsigned fLastRow:1;
	unsigned fOutline:1;
	unsigned reserved:12;
	int itcMac:16;
	__s32 dxaAdjust;
	__s32 dxaScale;
	__s32 dxsInch;
	__s16 rgdxaCenter[itcMax + 1];
	__s16 rgdxaCenterPrint[itcMax + 1];
	TC rgtc[itcMax];
	SHD rgshd[itcMax];
	BRC rgbrcTable[6];
    } TAP;


typedef struct _DCS {
	/* 16 bits for bitfields */
	unsigned fdct:3;
	unsigned count:5;
	unsigned reserved:8;
    } DCS;


typedef struct _ANLD {
	__u8 nfc;
	__u8 cxchTextBefore;

	unsigned cxchTextAfter:8;
	unsigned jc:2;
	unsigned fPrev:1;
	unsigned fHang:1;
	unsigned fSetBold:1;
	unsigned fSetItalic:1;
	unsigned fSetSmallCaps:1;
	unsigned fSetCaps:1;
	unsigned fSetStrike:1;
	unsigned fSetKul:1;
	unsigned fPrevSpace:1;
	unsigned fBold:1;
	unsigned fItalic:1;
	unsigned fSmallCaps:1;
	unsigned fCaps:1;
	unsigned fStrike:1;
	unsigned kul:3;
	unsigned ico:5;

	__s16 ftc;
	__u16 hps;
	__u16 iStartAt;
	__s16 dxaIndent;
	__u16 dxaSpace;
	__u8 fNumber1;
	__u8 fNumberAcross;
	__u8 fRestartHdn;
	__u8 fSpareX;
	XCHAR rgxch[32];
    } ANLD;


typedef struct _NUMRM {
	__u8 fNumRM;
	__u8 Spare1;
	__s16 ibstNumRM;
	DTTM dttmNumRM;
	__u8 rgbxchNums[9];
	__u8 rgnfc[9];
	__s16 Spare2;
	__s32 PNBR[9];
	XCHAR xst[32];
    } NUMRM;


#define itbdMax 64
typedef struct _TBD	/* 8 bits */
    {
    unsigned	jc:3;
    unsigned	tlc:3;
    unsigned	reserved:2;
    } TBD;


typedef struct _PAP {
	__u16 istd;
	__u8 jc;
	__u8 fKeep;
	__u8 fKeepFollow;

	unsigned fPageBreakBefore:8;
	unsigned fBrLnAbove:1;
	unsigned fBrLnBelow:1;
	unsigned fUnused:2;
	unsigned pcVert:2;
	unsigned pcHorz:2;
	unsigned brcp:8;
	unsigned brcl:8;

	__u8 reserved1;
	__u8 ilvl;
	__u8 fNoLnn;
	__s16 ilfo;
	__u8 nLvlAnm;
	__u8 reserved2;
	__u8 fSideBySide;
	__u8 reserved3;
	__u8 fNoAutoHyph;
	__u8 fWidowControl;
	__s32 dxaRight;
	__s32 dxaLeft;
	__s32 dxaLeft1;
	LSPD lspd;
	__u32 dyaBefore;
	__u32 dyaAfter;
	PHE phe;
	__u8 fCrLf;
	__u8 fUsePgsuSettings;
	__u8 fAdjustRight;
	__u8 reserved4;
	__u8 fKinsoku;
	__u8 fWordWrap;
	__u8 fOverflowPunct;
	__u8 fTopLinePunct;
	__u8 fAutoSpaceDE;
	__u8 fAtuoSpaceDN;
	__u16 wAlignFont;

	unsigned fVertical:1;
	unsigned fBackward:1;
	unsigned fRotateFont:1;
	unsigned reserved5:13;
	unsigned reserved6:16;

	__s8 fInTable;
	__s8 fTtp;
	__u8 wr;
	__u8 fLocked;
	TAP ptap;
	__s32 dxaAbs;
	__s32 dyaAbs;
	__s32 dxaWidth;
	BRC brcTop;
	BRC brcLeft;
	BRC brcBottom;
	BRC brcRight;
	BRC brcBetween;
	BRC brcBar;
	__s32 dxaFromText;
	__s32 dyaFromText;
	/*16 bits for the next two entries */
	int dyaHeight:15;
	int fMinHeight:1;
	SHD shd;
	DCS dcs;
	__s8 lvl;
	__s8 fNumRMIns;
	ANLD anld;
	__s16 fPropRMark;
	__s16 ibstPropRMark;
	DTTM dttmPropRMark;
	NUMRM numrm;
	__s16 itbdMac;
	__s16 rgdxaTab[itbdMax];
	TBD rgtbd[itbdMax];
/* >>------------------PATCH */
	char stylename[100];
/* -----------------------<< */
      /* BiDi */
      unsigned fBidi:1;
    } PAP;

#define istdNil 4095

typedef __u16 LID;

#define istdNormalChar 10

typedef struct _CHP {
    unsigned fBold:1;
    unsigned fItalic:1;
    unsigned fRMarkDel:1;
    unsigned fOutline:1;
    unsigned fFldVanish:1;
    unsigned fSmallCaps:1;
    unsigned fCaps:1;
    unsigned fVanish:1;
    unsigned fRMark:1;
    unsigned fSpec:1;
    unsigned fStrike:1;
    unsigned fObj:1;
    unsigned fShadow:1;
    unsigned fLowerCase:1;
    unsigned fData:1;
    unsigned fOle2:1;
    unsigned fEmboss:1;
    unsigned fImprint:1;
    unsigned fDStrike:1;
    int		fUsePgsuSettings:1;/*? */
    unsigned reserved1:12;
    __u32 reserved2;
    __u16 reserved11;
    __u16 ftc;
    __u16 ftcAscii;
    __u16 ftcFE;
    __u16 ftcOther;
    __u16 hps;
    __s32 dxaSpace;
    unsigned iss:3;
    unsigned kul:4;
    unsigned fSpecSymbol:1;
    unsigned ico:5;
    unsigned reserved3:1;
    unsigned fSysVanish:1;
    unsigned hpsPos:1;
    int		super_sub:16;
    LID lid;
    LID lidDefault;
    LID lidFE;
    __u8 idct;
    __u8 idctHint;
    __u8 wCharScale;
    __s32 fcPic_fcObj_lTagObj;
    __s16 ibstRMark;
    __s16 ibstRMarkDel;
    DTTM dttmRMark;
    DTTM dttmRMarkDel;
    __s16 reserved4;
    __u16 istd;
    __s16 ftcSym;
    XCHAR xchSym;
    __s16 idslRMReason;
    __s16 idslReasonDel;
    __u8 ysr;
    __u8 chYsr;
    __u16 cpg;
    __u16 hpsKern;
    unsigned icoHighlight:5;
    unsigned fHighlight:1;
    unsigned kcd:3;
    unsigned fNavHighlight:1;
    unsigned fChsDiff:1;
    unsigned fMacChs:1;
    unsigned fFtcAsciSym:1;
    unsigned reserved5:3;
    unsigned fPropRMark:16;	/*was typo of fPropMark in documentation */
    __s16 ibstPropRMark;
    DTTM dttmPropRMark;
    __u8 sfxtText;
    __u8 reserved6;
    __u8 reserved7;
    __u16 reserved8;
    __u16 reserved9;
    DTTM reserved10;
    __u8 fDispFldRMark;
    __s16 ibstDispFldRMark;
    DTTM dttmDispFldRMark;
    XCHAR xstDispFldRMark[16];
    SHD shd;
    BRC brc;

      /* BiDi properties */
      unsigned  fBidi:1;
      unsigned  fBoldBidi:1;
      unsigned  fItalicBidi:1;
      __u16 ftcBidi;
      __u16 hpsBidi;
      __u8  icoBidi;
      LID lidBidi;

    } CHP;

typedef struct _CHPX
    {
    __u16	istd;
    __u8	cbGrpprl;
    __u8	*grpprl;
    } CHPX;


typedef union _UPD {
	PAP apap;
	CHP achp;
	CHPX chpx;
    } UPD;

/* The UPE structure is the non-zero prefix of a UPD structure

For my purposes we'll call them the same, and when we get around
to writing word files, when we'll make a distinction. */
    typedef UPD UPE;


typedef struct _UPXF {
	WORD cbUPX;
	UPX upx;
    } UPXF;


/* STD: STyle Definition
   The STD contains the entire definition of a style.
   It has two parts, a fixed-length base (cbSTDBase bytes long)
   and a variable length remainder holding the name, and the upx and upe
   arrays (a upx and upe for each type stored in the style, std.cupx)
   Note that new fields can be added to the BASE of the STD without
   invalidating the file format, because the STSHI contains the length
   that is stored in the file.  When reading STDs from an older version,
   new fields will be zero.
*/
    typedef struct _wvSTD {
	/* Base part of STD: */
	unsigned sti:12;		/* invariant style identifier */
	unsigned fScratch:1;		/* spare field for any temporary use,
				   always reset back to zero! */
	unsigned fInvalHeight:1;	/* PHEs of all text with this style are wrong */
	unsigned fHasUpe:1;		/* UPEs have been generated */
	unsigned fMassCopy:1;	/* std has been mass-copied; if unused at
				   save time, style should be deleted */
	unsigned sgc:4;		/* style type code */
	unsigned istdBase:12;	/* base style */

	unsigned cupx:4;		/* # of UPXs (and UPEs) */
	unsigned istdNext:12;	/* next style */
	WORD bchUpe;		/* offset to end of upx's, start of upe's */

	/* 16 bits in the following bitfields */
	unsigned fAutoRedef:1;	/* auto redefine style when appropriate */
	unsigned fHidden:1;		/* hidden from UI? */
	unsigned reserved:14;	/* unused bits */

		/* Variable length part of STD: */
		/*	XCHAR *xstzName;*/	/* sub-names are separated by chDelimStyle */
	char *xstzName;

	UPXF *grupxf;		/*was UPX *grupx in the spec, but for my
							  purposes its different */

	/* the UPEs are not stored on the file; they are a cache of the based-on
	   chain */
	UPE *grupe;
    } STD;

typedef enum {
	sgcPara = 1,
	sgcChp,
	sgcPic,
	sgcSep,
	sgcTap
	} sgcval;


/*The style sheet (STSH) is stored in the file in two parts, a STSHI and then
an array of STDs. The STSHI contains general information about the following
stylesheet, including how many styles are in it. After the STSHI, each style
is written as an STD. Both the STSHI and each STD are preceded by a U16
that indicates their length.

 Field     Size        Comment
 cbStshi   2 bytes     size of the following STSHI structure
 STSHI     (cbStshi)   Stylesheet Information
 Then for each style in the stylesheet (stshi.cstd), the following is
 stored:
 cbStd     2 bytes     size of the following STD structure
 STD       (cbStd)     the style description*/
typedef struct _STSH {
	STSHI Stshi;
	STD *std;
    } STSH;

typedef struct _SEPX {
	__u16 cb;
	__u8 *grpprl;
    } SEPX;

    typedef struct _BRC10 {	/* 16 bits in total */
	unsigned dxpLine2Width:3;
	unsigned dxpSpaceBetween:3;
	unsigned dxpLine1Width:3;
	unsigned dxpSpace:5;
	unsigned fShadow:1;
	unsigned fSpare:1;
    } BRC10;

#define WV_PAGESIZE 512

 typedef struct _BX {
	__u8 offset;
	PHE phe;
    } BX;

 typedef struct _PAPX {
	__u16 cb;
	__u16 istd;
	__u8 *grpprl;
    } PAPX;

 typedef struct _PAPX_FKP {
	__u32 *rgfc;
	BX *rgbx;
	PAPX *grppapx;
	__u8 crun;
    } PAPX_FKP;

 typedef struct _BTE {
	DWORD pn; //:22;
	unsigned unused:10;
    } BTE;

 typedef struct _CHPX_FKP {
	__u32 *rgfc;
	__u8 *rgb;
	CHPX *grpchpx;
	__u8 crun;
    } CHPX_FKP;

typedef enum {
	DOCBEGIN,
	DOCEND,
	SECTIONBEGIN,
	SECTIONEND,
	PARABEGIN,
	PARAEND,
	CHARPROPBEGIN,
	CHARPROPEND,
	COMMENTBEGIN,
	COMMENTEND
    } wvTag;


  typedef struct _DOGRID {
	WORD xaGrid;
	WORD yaGrid;
	WORD dxaGrid;
	WORD dyaGrid; //:16;
	unsigned dyGridDisplay:7;
	unsigned fTurnItOff:1;
	unsigned dxGridDisplay:7;
	unsigned fFollowMargins:1;
    } DOGRID;


    typedef struct _COPTS {
	/* 16 bits for bitfields */
	unsigned fNoTabForInd:1;
	unsigned fNoSpaceRaiseLower:1;
	unsigned fSuppressSpbfAfterPageBreak:1;
	unsigned fWrapTrailSpaces:1;
	unsigned fMapPrintTextColor:1;
	unsigned fNoColumnBalance:1;
	unsigned fConvMailMergeEsc:1;
	unsigned fSuppressTopSpacing:1;
	unsigned fOrigWordTableRules:1;
	unsigned fTransparentMetafiles:1;
	unsigned fShowBreaksInFrames:1;
	unsigned fSwapBordersFacingPgs:1;
	unsigned reserved:4;
    } COPTS;


    typedef struct _DOPTYPOGRAPHY {
	unsigned fKerningPunct:1;
	unsigned iJustification:2;
	unsigned iLevelOfKinsoku:2;
	unsigned f2on1:1;
	unsigned reserved:10;
	unsigned cchFollowingPunct:16;

	WORD cchLeadingPunct;
	WORD rgxchFPunct[101];
	WORD rgxchLPunct[51];
    } DOPTYPOGRAPHY;


    typedef struct _ASUMYI {
	unsigned fValid:1;
	unsigned fView:1;
	unsigned iViewBy:2;
	unsigned fUpdateProps:1;
	unsigned reserved:11;
	unsigned wDlgLevel:16;

	DWORD lHighestLevel;
	DWORD lCurrentLevel;
    } ASUMYI;


    typedef struct _DOP {
	unsigned fFacingPages:1;
	unsigned fWidowControl:1;
	unsigned fPMHMainDoc:1;
	unsigned grfSuppression:2;
	unsigned fpc:2;		/*where footnotes are put */
	unsigned reserved1:1;
	unsigned grpfIhdt:8;
	unsigned rncFtn:2;		/*how to restart footnotes */
	unsigned fFtnRestart:1;	/* Word 2 */

	unsigned nFtn:15;		/*first footnote no. WORD 2: int :15 */

	__u8 irmBar;		/* W2 */
	unsigned irmProps:7;		/* W2 */

	unsigned fOutlineDirtySave:1;
	unsigned reserved2:7;
	unsigned fOnlyMacPics:1;
	unsigned fOnlyWinPics:1;
	unsigned fLabelDoc:1;
	unsigned fHyphCapitals:1;
	unsigned fAutoHyphen:1;
	unsigned fFormNoFields:1;
	unsigned fLinkStyles:1;

	unsigned fRevMarking:1;
	unsigned fBackup:1;
	unsigned fExactCWords:1;
	unsigned fPagHidden:1;
	unsigned fPagResults:1;
	unsigned fLockAtn:1;
	unsigned fMirrorMargins:1;

	unsigned fKeepFileFormat:1;	/* W2 */

	unsigned reserved3:1;

	unsigned fDfltTrueType:1;
	unsigned fPagSuppressTopSpacing:1;

	unsigned fRTLAlignment:1;	/* W2 */
	unsigned reserved3a:6;	/* " */
	unsigned reserved3b:7;	/* " */

	unsigned fSpares:16;		/* W2 */

	unsigned fProtEnabled:1;
	unsigned fDispFormFldSel:1;
	unsigned fRMView:1;
	unsigned fRMPrint:1;
	unsigned reserved4:1;
	unsigned fLockRev:1;
	unsigned fEmbedFonts:1;

	COPTS copts;

	WORD dxaTab;

	DWORD ftcDefaultBi;	/* W2 */

	WORD wSpare;
	WORD dxaHotZ;
	WORD cConsecHypLim;
	WORD wSpare2;

	DWORD wSpare3;		/* W2 */

	DTTM dttmCreated;
	DTTM dttmRevised;
	DTTM dttmLastPrint;

	WORD nRevision;
	DWORD tmEdited;
	DWORD cWords;
	DWORD cCh;
	WORD cPg;
	DWORD cParas;

	WORD rgwSpareDocSum[3];	/* W2 */

	unsigned rncEdn:2;		/*how endnotes are restarted */
	unsigned nEdn:14;		/*beginning endnote no */
	unsigned epc:2;		/*where endnotes go */
	unsigned nfcFtnRef:4;	/*number format code for auto footnotes, i think use the new_* instead */
	unsigned nfcEdnRef:4;	/*number format code for auto endnotes, i thing use the new_* instead */
	unsigned fPrintFormData:1;
	unsigned fSaveFormData:1;
	unsigned fShadeFormData:1;
	unsigned reserved6:2;
	unsigned fWCFtnEdn:1;

	DWORD cLines;
	DWORD cWordsFtnEnd;
	DWORD cChFtnEdn;
	WORD cPgFtnEdn;
	DWORD cParasFtnEdn;
	DWORD cLinesFtnEdn;
	DWORD lKeyProtDoc;	/*password protection key (! ?) */

	unsigned wvkSaved:3;
	unsigned wScaleSaved:9;
	unsigned zkSaved:2;
	unsigned fRotateFontW6:1;
	unsigned iGutterPos:1;
	unsigned fNoTabForInd:1;
	unsigned fNoSpaceRaiseLower:1;
	unsigned fSuppressSpbfAfterPageBreak:1;
	unsigned fWrapTrailSpaces:1;
	unsigned fMapPrintTextColor:1;
	unsigned fNoColumnBalance:1;
	unsigned fConvMailMergeEsc:1;
	unsigned fSuppressTopSpacing:1;
	unsigned fOrigWordTableRules:1;
	unsigned fTransparentMetafiles:1;
	unsigned fShowBreaksInFrames:1;
	unsigned fSwapBordersFacingPgs:1;
	unsigned reserved7:4;

	unsigned fSuppressTopSpacingMac5:1;
	unsigned fTruncDxaExpand:1;
	unsigned fPrintBodyBeforeHdr:1;
	unsigned fNoLeading:1;
	unsigned reserved8:1;
	unsigned fMWSmallCaps:1;
	unsigned reserved9:10;
	unsigned adt:16;

	DOPTYPOGRAPHY doptypography;
	DOGRID dogrid;

	unsigned reserver11:1;
	unsigned lvl:4;
	unsigned fGramAllDone:1;
	unsigned fGramAllClean:1;
	unsigned fSubsetFonts:1;
	unsigned fHideLastVersion:1;
	unsigned fHtmlDoc:1;
	unsigned reserved10:1;
	unsigned fSnapBorder:1;
	unsigned fIncludeHeader:1;
	unsigned fIncludeFooter:1;
	unsigned fForcePageSizePag:1;
	unsigned fMinFontSizePag:1;
	unsigned fHaveVersions:1;
	unsigned fAutoVersion:1;
	unsigned reserved11:14;

	ASUMYI asumyi;

	DWORD cChWS;
	DWORD cChWSFtnEdn;
	DWORD grfDocEvents;
	unsigned fVirusPrompted:1;
	unsigned fVirusLoadSafe:1;
	DWORD KeyVirusSession30; //:30;

	__u8 Spare[30];
	DWORD reserved12;
	DWORD reserved13;

	DWORD cDBC;
	DWORD cDBCFtnEdn;
	DWORD reserved14;
	WORD new_nfcFtnRef;	/*number format code for auto footnote references */
	WORD new_nfcEdnRef;	/*number format code for auto endnote references */
	WORD hpsZoonFontPag;
	WORD dywDispPag;
    } DOP;


#ifdef __cplusplus
extern "C" {
#endif

__u32 wvNormFC(__u32 fc, int *flag);

Sprm wvApplySprmFromBucket (BYTE ver, __u16 sprm, PAP * apap, CHP * achp,
		SEP * asep, STSH * stsh, __u8 * pointer, __u16 * pos,
		FILE * data);
int wvAssembleSimplePAP (int ver, PAP * apap, __u32 fc, PAPX_FKP * fkp,
		STSH * stsh, FILE * data);
void wvInitPAP (PAP * item);
void wvInitPAPX_FKP (PAPX_FKP * fkp);
#define wvReleasePAPX_FKP(X) {}

int wvGetIndexFCInFKP_PAPX (PAPX_FKP * fkp, __u32 currentfc);

void wvCopyPAP (PAP * dest, PAP * src);
void wvCopyTAP (TAP * dest, TAP * src);

static __u8 wvEatSprm (__u16 sprm, __u8 * pointer, __u16 * pos);
SprmName wvGetrgsprmWord6 (__u8 in);

int wvGetPieceBoundsFC(__u32 * begin, __u32 * end, CLX * clx, __u32 piececount);
__u32 wvGetPieceFromCP (__u32 currentcp, CLX * clx);
__u32 wvConvertCPToFC (__u32 currentcp, CLX * clx);
void wvInitSEP (SEP * item);
void wvGetSEPX(SEPX * item, FILE * fd);
int wvAddSEPXFromBucket (SEP * asep, SEPX * item, STSH * stsh);
int wvAddSEPXFromBucket6 (SEP * asep, SEPX * item, STSH * stsh);
void wvReleaseSEPX (SEPX * item);
int wvGetBTE_FromFC (BTE * bte, __u32 currentfc, BTE * list, __u32 * fcs, int nobte);
void wvGetPAPX_FKP (int ver, PAPX_FKP * fkp, __u32 pn, FILE * fd);
void wvGetCHPX_FKP(CHPX_FKP * fkp, __u32 pn, FILE * fd);
#define wvReleaseCHPX_FKP(XX) {}
int wvGetPieceBoundsCP (__u32 * begin, __u32 * end, CLX * clx, __u32 piececount);
int wvAssembleSimpleCHP(int ver, CHP * achp, __u32 fc, CHPX_FKP * fkp, STSH * stsh);
void wvInitCLX(CLX *item);
void wvInitSTSH(STSH * item);
void wvInitCHPX_FKP (CHPX_FKP * fkp);

void wvGetFIB2(FIB *item, FILE* fd);
void wvGetFIB6(FIB *item, FILE* fd);
void wvGetFIB8(FIB *item, FILE* fd);
void wvGetCLX(BYTE ver, CLX *clx, __u32 offset, __u32 len, __u8 fExtChar, FILE *fd);
void wvBuildCLXForSimple6(CLX *clx, FIB * fib);
void wvGetSTSH(STSH *item, __u32 offset, __u32 len, FILE *fd);
int wvGetBTE_PLCF6(BTE ** bte, __u32 ** pos, __u32 * nobte, __u32 offset, __u32 len, FILE * fd);
int wvGetBTE_PLCF(BTE ** bte, __u32 ** pos, __u32 * nobte, __u32 offset, __u32 len, FILE * fd);
int wvGetSED_PLCF (SED ** item, __u32 ** pos, __u32 * noitem, __u32 offset, __u32 len, FILE * fd);
void wvReleaseCLX(CLX *clx);
void wvReleaseSTSH (STSH * item);
void internal_wvReleasePAPX_FKP (PAPX_FKP * fkp);
void internal_wvReleaseCHPX_FKP (CHPX_FKP * fkp);
__u32 wvSearchNextLargestFCPAPX_FKP (PAPX_FKP * fkp, __u32 currentfc);
int wvQuerySamePiece (__u32 fcTest, CLX * clx, __u32 piece);
__u32 wvGetEndFCPiece (__u32 piece, CLX * clx);
__u32 wvSearchNextSmallestFCPAPX_FKP (PAPX_FKP * fkp, __u32 currentfc);

__u8 bread_8ubit (__u8 * in, __u16 * pos);
__u16 bread_16ubit (__u8 * in, __u16 * pos);

SprmName wvGetrgsprmWord6 (__u8 in);
SprmName wvGetrgsprmPrm (__u16 in);

extern TAP * cache_TAP;

#ifdef __cplusplus
}
#endif

#endif