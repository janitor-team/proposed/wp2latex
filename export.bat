@rem Placeholder for DOS - do not remove
@rem It also calls chained command sepateted by '&&' if exists.

echo EXPORT %1 %2 %3 %4 %5

if not %2 == && goto NoMake
if not %3 == make goto NoMake

make %4 %5 %6 %7
  
:NoMake
