echo off
cls
echo            Distribution package for the program WP2LATEX.
echo  WP2LATEX converts WordPerfect 5.1 documents to LaTeX documents.
echo. 
echo. 
echo  This diskette or archive contains the following files :
echo. 
echo  readme.1st    ( the 1'st readme file, label ) 
echo  bin\DOS\WP2LATEX.EXE ( the conversion program, DOS executable )
echo  bin\WIN\WP2LATEX.EXE ( the conversion program, Windows executable )
echo  bin\LINUX\WP2LATEX   ( the conversion program, Linux executable (ELF) )
echo  doc\WP2LATEX.TEX ( user's guide for the program, in English; LaTeX format )
echo  doc\FILELIST.BAT   ( this file )
echo  doc\BUG.TXT      (list of known bugs)	
echo  SOURCES.cc\wp2latex.cc  ( the C++ sources, main file )
echo  SOURCES.cc\wp2latex.h  ( the C++ sources, main header file )
echo  styles.tex\WP2LATEX.STY ( style file needed by LaTeX for processing )
echo  TEST\TEST.WP     (WP test file with mix of features)
echo  TEST\CHARACTR.WP (WP test file with all WP charsets)
echo.
echo  The following two files may also be included :
echo  DOC\WP2LTX.TEX   ( Dutch (original) version of the user's guide-now obsolette)
echo. 
