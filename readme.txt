Introduction to version 3.81 of WP2LaTeX.

INTRODUCTION
------------
This program converts WordPerfect 1.x, MAC 2,3,4.x, PC 4.x, 5.x, 6(7,8,9).x, 
RTF, HTML, T602 and AbiWord document files to LaTeX input files. Please read
the manual for the basic conditions under which a satisfying conversion is 
made. Currently,there are MS-DOS, Linux and Windows versions of the program available.
  The C++ sources are included in this package. Obsolette Pascal sources 
are placed inside separate package.

Please read doc/wp2latex.tex for more information.

New to this version are:
 + Conversion from MAC WP 1.x, 2.x, 3.x
 + Improved file format autodetection
 + Support for new formula format MTEF
 
 
The things have to be done:
 - Decrease amount of TeX Errors in converted documents
 - Make tests of quality of the conversion
 - Add new foreigner characters
 - Add new features
 - Improve support for WPG l.2   and hybrid images (Raster+Line draw+Postscript)


THE PACKAGE
-----------

The home base for this package is
http://www.penguin.cz/~fojtik/wp2latex/


HOW TO BUILD & INSTALL
----------------------

**********DOS/WINDOWS and GCC********
There is makefile prepared for GCC. It will work under both Linux and DJGPP.

    A, if you have bash run it and type
./configure
    B, or if you do not have bash type (it will return generic makefiles)
make distclean

    type for building whole project:
make all		
	
    Automatic installation is not fully finished, but you might try:
make install    
    

**********DOS/WINDOWS and Borland C********
Because there is freely available Borland C++ compiler, I also provide script
for building wp2latex by BCC. (You could undo .configure script resulte by
typing 'make distclean')
	
	change to directory win
cd win
	execute bcc55.bat
bcc55.bat


**********DOS/WINDOWS and Microsoft Visual C++ 6********

(You could undo .configure script resulte by
typing 'make distclean') Open MSVC/wp2latex.dsw inside your Visual
C++ environment. Rebuild the target. The result of your build you
will find in the  MSVC/release/wp2latex.exe


**********WINDOWS and MikTex********

mkdir "C:\Program Files\MiKTeX-2.8\tex\latex\wp2latex\"

copy all files from wp2latex.377\styles.tex\ to
      "C:\Program Files\MiKTeX-2.8\tex\latex\wp2latex\"

   initexmf -u 
or 
   click to "MikRex Options/General/Refresh FNDB"


********Linux build and instalation*********
	Under Linux you may type:
./configure	
make all
make install


For finishing instalation you should also store files wp2latex.sty,
endnotes.sty, wncyr.sty, cyracc.def etc. to proper directory.
These files are stored in the directory 'styles.tex'. The TeX translator
should know about these files. It is solved by using a TEXINPUTS system
variable.

Suggestion from Roger for kpathsea LaTeX distribution:
After 'make install' it is necessary to do this (as root):
(1) in the tex tree, within the latex subdirectory, create a subdirectory
    called wp2latex. For example in my (web2c) installation I have
    /usr/share/texmf/tex/latex/wp2latex
(2) Into this subdirectory copy the contents of the styles.tex
    subdirectory.
(3) Invoke texhash.



******Generic Unix manual instalation*******

Please ensure that your wp2latex is correctly build.
Try: 
  ./configure
  make

When you succeed, you have half of a job done. If this fail, you could try:

  make generic
  make

In some cases you will need to manually fiddle with compile options.


I will assume that you have compiled wp2latex at this point.
The "wp2latex" executable is produced. A name of executable for cygwin
is "wp2latex.exe".
	
copy  YOUR_EXECUTABLE   to     /usr/local/bin/
mkdir /usr/share/texmf/tex/latex/wp2latex
copy  styles.tex/*.*    to     /usr/share/texmf/tex/latex/wp2latex/
copy  doc/wp2latex.1    to     /usr/local/man/man1/
copy  doc/locale/*.*    to     /usr/share/locale/
run   texthash  (for kpathsea TeX distribution)



***********DOS installation***********
       Currently you might also try to run
make install
     Cross your fingers - it might perhaps work.

Place a file wp2latex.exe (from directory dos\ or dos\real) to the directory
which is referred by the PATH variable. 

Place all stuff from the directory 'styles.tex\' to the
directory   emtex\texinput\ .

Optionally you can copy a file doc\locale\lc_messages\cs\wp2latex.mo
to the directory $(DJDIR)\SHARE\LOCALE\LC_MESSA\CS\WP2LATEX.MO .


For Borland C++ you should make another makefile. All code was tested
and you cannot expect any problems. Do not omit to add files
from directory .\atoms\ to your makefile. 


How to build WP2LaTeX for multiple Mac OS X soft- and hardware
==============================================================

First a statement from Apple: http://developer.apple.com/library/mac/#qa/qa1118/_index.html. In order to provide a WP2LaTeX binary with JPEG and PNG (and, coming, libintl) support we have to build a statically linked binary. This requires the mentioned crt0.o object file. Since Mac OS X 10.6, Snow Leopard, intel only, Apple does not provide this object file, one has to build it one-self. This page describes it a bit: http://osdir.com/ml/xcode-users/2009-09/msg00213.html. (You can use my later explanations to build the perfect crt0.o file. And, actually, the Apple developers link above leads via Xcode 3.2.6 to Csu-76.)

The absence of crt0.o and JPEG, PNG, and other libraries makes the "static" build option of WP2LaTeX unusable on Mac OS X.

Next is to build the static JPEG (jpeg-8c; http://www.ijg.org/, http://www.openjpeg.org/) and PNG (v. 1.2.44; http://www.libpng.org/pub/png/libpng.html) libraries. Building them is necessary because one cannot guarantee that on some remote Mac the JPEG and PNG shared libraries exist. So we have to link them statically into the binary. The libs' configure scripts allow quite the same parameters:

    ./configure --prefix=/some/path/to/a/build/directory --enable-shared=no --with-pic \
      	CFLAGS="-arch i386 -isysroot /Developer/SDKs/MacOSX10.4u.sdk -mmacosx-version-min=10.4 -force_cpusubtype_ALL -m32 -H -pipe -fPIC -Os" \
      	LDFLAGS="-L/some/path/to/a/build/directory/lib -L/some/path/to/a/build/directory/Csu-71 -arch i386 -Wl,-t" \
      	CC=gcc-4.0 CCP=cpp-4.0 CCX=g++-4.0 LD=gcc-4.0

The backslash characters ("\") at the end of the lines above mean that the lines are broken here and continue on the next line. They, the leading spaces, and the line breaks can be omitted when configuring. A few compiler switches and environment settings in these lines point towards compilation for a minimal Mac OS X version 10.4 ("Tiger"), so such a binary will (presumingly) run on updated Tiger (with intel support integrated), Leopard, Snow Leopard, Lion, ... for which GCC 4.0 is used because its shared libraries already exist on Tiger. And it looks as if it's just the first of a series of configurations, builds, and installations with a final step to stitch the few static libraries to one "fat" one with lipo which can then be linked to the WP2LaTeX binary.

The problem is that starting with the updated intel only Snow Leopard the Developer package on the Mac OS X 10.6.6 and above install DVD comes with Xcode 4 which does not support elder Mac OS X versions than 10.6 � itself only (see for example http://stackoverflow.com/questions/5333490/how-can-we-restore-ppc-ppc64-as-well-as-full-10-4-10-5-sdk-support-to-xcode-4/5348547, http://hints.macworld.com/article.php?story=20110318050811544). So, with modern Apple hardware you need to install an elder Xcode version (3.2.6), available from the Apple Developer Connection (http://developer.apple.com/), for free, after registration. It would overwrite the whole installation in /Developer, it overwrites in any case those parts of installed Xcode which are installed in /usr (so you might need to reinstall Xcode 4). You might decide to install in /Developer45 to remind you that it contains the SDKs for Mac OS X 10.4 and 10.5 (+ 10.6 + a lot of iPhone simulators). The -isysroot compiler option then takes as argument the path to the root of the SDK used for compilation, either as '-isysroot /Developer45/SDKs/MacOSX10.4u.sdk' or as '-isysroot /Developer/SDKs/MacOSX10.4u.sdk'.

Before we start to build the *optimised* JPEG and PNG libraries we need to take care that we don't overwrite previously built libraries. We need n passes, where n is the number of supported Mac OS X architectures. So let's install the built libraries in directories named build/lib-<arch name>. In the final step the lipo utility would combine them in build/lib or build/lib32 or build/lib64 build/lib{old|modern}. The optimisation switches for the different architectures can be:

    G3:     -arch ppc    -mcpu=G3 	    -m32 -isysroot /Developer45/SDKs/MacOSX10.4u.sdk -mmacosx-version-min=10.4.8 -H -pipe -fPIC -pthread -Os   -mpowerpc-gfxopt
    G4:     -arch ppc    -mcpu=G4 -mtune=G4 -m32 -isysroot /Developer45/SDKs/MacOSX10.4u.sdk -mmacosx-version-min=10.4.8 -H -pipe -fPIC -pthread -fast -mpowerpc-gfxopt -faltivec -mabi=altivec
    G5:     -arch ppc64  -mcpu=G5 -mtune=G5 -m64 -isysroot /Developer45/SDKs/MacOSX10.4u.sdk -mmacosx-version-min=10.4.8 -H -pipe -fPIC -pthread -fast -mpowerpc-gfxopt -faltivec -mabi=altivec
    i386:   -arch i386   -march=prescott    -m32 -isysroot /Developer45/SDKs/MacOSX10.4u.sdk -mmacosx-version-min=10.4.8 -H -pipe -fPIC -pthread -Os -msse3
    x86_64: -arch x86_64 -march=nocona      -m64 -isysroot /Developer45/SDKs/MacOSX10.4u.sdk -mmacosx-version-min=10.4.8 -H -pipe -fPIC -pthread -Os -msse3 -mssse3

Doing so we also perform "cross-compilation": our local host machine produces output for a foreign architecture. With Rosetta installed (http://en.wikipedia.org/wiki/Rosetta_(software)) compilation for G3 and G4 targets is like native. So, here are the configure invocations on an intel based Mac with Snow Leopard:

    G3:     ./configure --target powerpc-apple-darwin10.8.0   --host x86_64-apple-darwin10.8.0 --prefix=/path/to/directory/build --libdir=/path/to/directory/build/lib-G3     --enable-shared=no --with-pic LDFLAGS="-arch ppc    -Wl,-dead_strip_dylibs -Wl,-bind_at_load -Wl,-t" CC=gcc-4.0 CPP=cpp-4.0 CCX=g++-4.0 LD=gcc-4.0 CFLAGS="-arch ppc    -isysroot /Developer45/SDKs/MacOSX10.4u.sdk -mmacosx-version-min=10.4.8 -mcpu=G3        -m32 -H -pipe -fPIC -Os   -pthread -mpowerpc-gfxopt"
    G4:     ./configure --target powerpc-apple-darwin10.8.0   --host x86_64-apple-darwin10.8.0 --prefix=/path/to/directory/build --libdir=/path/to/directory/build/lib-G4     --enable-shared=no --with-pic LDFLAGS="-arch ppc    -Wl,-dead_strip_dylibs -Wl,-bind_at_load -Wl,-t" CC=gcc-4.0 CPP=cpp-4.0 CCX=g++-4.0 LD=gcc-4.0 CFLAGS="-arch ppc    -isysroot /Developer45/SDKs/MacOSX10.4u.sdk -mmacosx-version-min=10.4.8 -mcpu=G4        -m32 -H -pipe -fPIC -fast -pthread -mpowerpc-gfxopt -faltivec -maltivec -mabi=altivec -mtune=G4"
    G5:     ./configure --target powerpc64-apple-darwin10.8.0 --host x86_64-apple-darwin10.8.0 --prefix=/path/to/directory/build --libdir=/path/to/directory/build/lib-G5     --enable-shared=no --with-pic LDFLAGS="-arch ppc64  -Wl,-dead_strip_dylibs -Wl,-bind_at_load -Wl,-t" CC=gcc-4.0 CPP=cpp-4.0 CCX=g++-4.0 LD=gcc-4.0 CFLAGS="-arch ppc64  -isysroot /Developer45/SDKs/MacOSX10.4u.sdk -mmacosx-version-min=10.4.8 -mcpu=G5        -m64 -H -pipe -fPIC -fast -pthread -mpowerpc-gfxopt -faltivec -maltivec -mabi=altivec"
    i386:   ./configure --target i386-apple-darwin10.8.0      --host x86_64-apple-darwin10.8.0 --prefix=/path/to/directory/build --libdir=/path/to/directory/build/lib-i386   --enable-shared=no --with-pic LDFLAGS="-arch i386   -Wl,-dead_strip_dylibs -Wl,-bind_at_load -Wl,-t" CC=gcc-4.0 CPP=cpp-4.0 CCX=g++-4.0 LD=gcc-4.0 CFLAGS="-arch i386   -isysroot /Developer45/SDKs/MacOSX10.4u.sdk -mmacosx-version-min=10.4.8 -march=prescott -m32 -H -pipe -fPIC -Os   -pthread -msse3 -mfpmath=sse"
    x86_64: ./configure                                                                        --prefix=/path/to/directory/build --libdir=/path/to/directory/build/lib-x86_64 --enable-shared=no --with-pic LDFLAGS="-arch x86_64 -Wl,-dead_strip_dylibs -Wl,-bind_at_load -Wl,-t" CC=gcc-4.0 CPP=cpp-4.0 CCX=g++-4.0 LD=gcc-4.0 CFLAGS="-arch x86_64 -isysroot /Developer45/SDKs/MacOSX10.4u.sdk -mmacosx-version-min=10.4.8 -march=nocona   -m64 -H -pipe -fPIC -Os -pthread -msse3 -mssse3"

For each architecture a different target for the built library is set with "--libdir=�". The binaries play no role, won't be used, the C header files will be used to compile WP2LaTeX later on, but these files are independent of any hardware or OS issues, so for them it plays no role which configure/compile/install cycle was last. So choose some hardware target to start with: first configure accordingly (you might save the output of the configure run as well as the created config.log file), second run make, third run 'make install', and finally 'make clean'. This ends the cycle and you can proceed with the next hardware target. When you're through with this cycle, start the cycle for the next library!

When all libraries are built you need to "combine" them. This is done with the lipo utility � which does not understand the marketing names G3, G4, G5! So it's not possible to combine all three 32-bit architectures. First we need to create the directories to save the "fat" libraries in, so let's enter a shell and change directory ("cd") to that "build" directory (/path/to/directory/build) and invoke:

	mkdir lib-old lib-new

Now the lipo invocations to build libpng and libjpeg:

	lipo -create lib-G3/libpng.a lib-i386/libpng.a -o lib-old/libpng.a
	lipo -create lib-G4/libpng.a lib-G5/libpng.a lib-x86_64/libpng.a -o lib-new/libpng.a
	lipo -create lib-G3/libjpeg.a lib-i386/libjpeg.a -o lib-old/libjpeg.a
	lipo -create lib-G4/libjpeg.a lib-G5/libjpeg.a lib-x86_64/libjpeg.a -o lib-new/libjpeg.a

So let's build WP2LaTeX finally! We'll build two binaries, one for old hardware and one for modern hardware, similar to what is delivered as up-to-date X11. So we have to configure (and then compile) twice:

     old: ./configure --without-libintl \
        CPPFLAGS=-I/path/to/directory/build/include \
	  CFLAGS="-H -pipe -fPIC -pthread -fast -fno-exceptions" \
	CXXFLAGS="-H -pipe -fPIC -pthread -fast -fno-exceptions -fastcp -fconserve-space -Wno-non-template-friend" \
	LDFLAGS="-L/path/to/directory/build/Csu-71 -L/path/to/directory/build/lib-old -Wl,-dead_strip_dylibs -Wl,-bind_at_load -Wl,-t" \
	CXX="g++-4.0 -arch ppc -arch i386 -mmacosx-version-min=10.4.8 -isysroot /Developer45/SDKs/MacOSX10.4u.sdk -mmacosx-version-min=10.4.8 -Xarch_ppc -Xarch_ppc -m32 -Xarch_ppc -mcpu=G3 -Xarch_ppc -mtune=G3 -Xarch_i386 -march=prescott -Xarch_i386 -m32 -Xarch_i386 -msse3" \
	 CC="gcc-4.0 -arch ppc -arch i386 -mmacosx-version-min=10.4.8 -isysroot /Developer45/SDKs/MacOSX10.4u.sdk -mmacosx-version-min=10.4.8 -Xarch_ppc -Xarch_ppc -m32 -Xarch_ppc -mcpu=G3 -Xarch_ppc -mtune=G3 -Xarch_i386 -march=prescott -Xarch_i386 -m32 -Xarch_i386 -msse3" \
	CPP="cpp-4.0"

  modern: ./configure --without-libintl \
  	CPPFLAGS=-I/path/to/directory/build/include \
	  CFLAGS="-H -pipe -fPIC -pthread -fast -fno-exceptions -arch ppc -arch ppc64 -arch x86_64 -isysroot /Developer45/SDKs/MacOSX10.4u.sdk -iwithsysroot /Developer45/SDKs/MacOSX10.4u.sdk -mmacosx-version-min=10.4.8" \
	CXXFLAGS="-H -pipe -fPIC -pthread -fast -fno-exceptions -arch ppc -arch ppc64 -arch x86_64 -isysroot /Developer45/SDKs/MacOSX10.4u.sdk -iwithsysroot /Developer45/SDKs/MacOSX10.4u.sdk -mmacosx-version-min=10.4.8 -fastcp -fconserve-space -Wno-non-template-friend" \
	LDFLAGS="-L/path/to/directory/build/Csu-71 -L/path/to/directory/build/lib-new -Wl,-dead_strip_dylibs -Wl,-bind_at_load -Wl,-t -arch ppc -arch ppc64 -arch x86_64" \
	CXX="g++-4.0 -Xarch_ppc -faltivec -Xarch_ppc -mabi=altivec -Xarch_ppc -mcpu=G4 -Xarch_ppc -mtune=G4 -Xarch_ppc64 -mpowerpc-gfxopt -Xarch_ppc64 -faltivec -Xarch_ppc64 -mabi=altivec -Xarch_x86_64 -march=nocona -Xarch_x86_64 -m64 -Xarch_x86_64 -msse3 -Xarch_x86_64 -mssse3" \
	 CC="gcc-4.0 -Xarch_ppc -faltivec -Xarch_ppc -mabi=altivec -Xarch_ppc -mcpu=G4 -Xarch_ppc -mtune=G4 -Xarch_ppc64 -mpowerpc-gfxopt -Xarch_ppc64 -faltivec -Xarch_ppc64 -mabi=altivec -Xarch_x86_64 -march=nocona -Xarch_x86_64 -m64 -Xarch_x86_64 -msse3 -Xarch_x86_64 -mssse3" \
	CPP="cpp-4.0"

Again the lines are broken near the backslashes and the many space characters are inserted because of the look. This time I am not using "gcc-4.0 -E" as the C preprocessor (CPP), because -E and some other options are not allowed when using multiple -arch settings. For educational reasons. And because of these I also chose to show the two different methods to build real fat binaries in one compiler run (although it makes a difference: when putting the -arch flags into CFLAGS/CXXFLAGS instead of CC/CXX configure will set dependency style to gcc3 rather than gcc[4]; this can play a role when -E, -S, -save-temps, or -M options are used, which is not allowed with multiple -arch flags; -MD would need "quoting" with -Wp, or -Xarch_<arch>). Of no educational use is the demonstration of the Apple additions -Xarch_<arch> (for the curious, the wrapper for GCC that Apple uses is here: http://www.opensource.apple.com/source/gcc/gcc-5666.3/driverdriver.c, http://www.opensource.apple.com/source/llvmgcc42/llvmgcc42-2336.1/driverdriver.c). These switches separate optimising options that they apply only for the specific hardware (for example, all intel CPUs and the G3, PPC750, do not have the Altivec extension, and no G3, G4, or G5 has intel's SSE variations). They are similar to CFLAGS and CXXFLAGS, meant for the C language or for the C++ language.

So start building WP2LaTeX and configure now for the one target. After this invoke make. The first target is built in wp2latex.3.56/mac (the directories 32 and 64 contain elder versions of WP2LaTeX, either for old or for new hardware). Before starting over for the next target rename the just built wp2latex binary to wp2latex-new or wp2latex-old or save it in the according subdirectory. Otherwise it would be overwritten. Before configuring and compiling once more, we need to perform 'make clean'. Now configure, make, and rename again, followed by a final 'make clean' to save disk space. You'll have now software that will run on Tiger (Mac OS X 10.4.8 and later), Leopard, Snow Leopard, Lion, ..., and on (approximately) five sorts of Apple hardware: G3/PowerPC 750, G4/PowerPC 74xx, G5/PowerPC 970, i386/intel Core Solo & Duo T1200/T2300/T2400/T2500/T2600 ("Yonah"), x86_64/intel Core 2 Duo T5600/T7200/T7300/T7400/T7500/T7600/T7700/T7800 ("Merom")/E8135/E8235/E8335/E8435/P7350/P7450/P7550/P8400/P8600/P8700/P8800/T8100/T8300/T9300/T9400/T9500/T9550/T9600/T9800/T9900/L9300/L9400/L9600/U9400/U9600 ("Penryn")/E7600/E8600 ("Wolfdale")/Core i3/Core i5/Core i7 ("Nehalem", "Clarkdale", "Arrandale", "Lynnfield", "Sandy Bridge")/intel Xeon 3500/3530/3565/3680 ("Bloomfield")/intel Xeon 5100 ("Woodcrest")/intel Xeon 5300 ("Clovertown")/intel Xeon 5400 ("Harpertown")/intel Xeon 5500 ("Gainestown")/intel Xeon E5620/X5650/X5670 ("Westmere").

When you change the setting of -mmacosx-version-min=10.4.8 to some larger value, 10.5 for example, then the software will not run on Tiger � and you won't need to compile for the G3. It's support ended with Mac OS X 10.4.11. So you could build a real 32-bit WP2LaTeX, supporting G4 and i386, a real 64-bit WP2LaTeX, supporting G5 and x86_64. And starting with Mac OS X 10.6, Snow Leopard, you can remove the G4 and G5 support, it's intel only. Then it's probably useful to do it like Apple and create one combined i386/x86_64 binary. Similarly when you reduce the features of the binaries you have to reduce the features of JPEG, PNG and other libraries.


How to build (native) WP2LaTeX for one Mac OS X soft- and hardware
==================================================================

The multiple case is clearly meant to make a software that can be easily distributed. In the simple case you can use the shared or dynamic libraries ("dylibs") from your system, as installed by any means: manually, by some package manager, otherwise (in some DMG or such archive format). All you need to do is to tell the configure script where the C header files and the (proper) shared libraries can be found. For example:

	./configure LDFLAGS=-L/sw/lib CPPFLAGS=-I/sw/include

There is one problem, that your compiler can be set to produce a 64-bit binary while the libraries are only 32-bit or it's set to produce a 32-bit binary by default and the libraries are 64-bit. Apple installs with Tiger, Leopard, and Snow Leopard "fat" libraries, libraries that have built-in support for a handful of architectures, as I described in the multiple case, so compilation will always work with the system binaries, i.e., everything but JPEG, PNG, and libintl support. If you do not have their libraries and C header files already installed you need to perform this yourself. Similarly as described for the multiple systems case you have the choice of 32-bit or 64-bit support and their appropriate compiler options, degree of optimisation. Configure, make, and install these auxiliary libraries. You don't need to care whether they are 32-bit or 64-bit, they will be the same as the WP2LaTeX binary, so you can install directly in /some/path/to/a/build/directory/lib. If necessary correct the settings of LDFLAGS and CPPFLAGS as given before. Now decide for WP2LaTeX which CFLAGS and CXXFLAGS you want to use. If you wish to set a particular level of optimisation you need to include the --enable-extra-optimization=no and --enable-optimization=no configure options, otherwise configure will add -O2 to the compiler switches. The configure invocation could be:

    ./configure \
	CPPFLAGS=-I/path/to/directory/build/include \
	  CFLAGS="-H -pipe -fPIC -pthread -fast -fno-exceptions <more options>" \
	CXXFLAGS="-H -pipe -fPIC -pthread -fast -fno-exceptions <more options> -fastcp -fconserve-space -Wno-non-template-friend" \
	 LDFLAGS="-L/some/path/to/a/build/directory/lib -Wl,-dead_strip_dylibs -Wl,-bind_at_load -Wl,-t"

Since you're building native libraries and a native WP2LaTeX binary you don't need to specify compilers and linkage editor (LD), but you can, to make a specific choice of a compiler (beware: clang/clang++ might fail). The native build bring one problem to daylight: the compiler will spit out hundreds of warnings! They are only warnings. They are not errors. They will allow that the binary gets built (hopefully). And you are warned now that you might see what you don't expect to see.

I was recommending compiler switches that produce a lot of output. -H makes the compiler report all C header files it reads which it tries to process the C or C++ source file. Using this option allows you find whether some C header is used that is supposed not to be used because it can produce errors. You can safely omit this switch when every things works fine. The -t switch, meant only for ld and therefore targeted with -Wl, so that it reads completely -Wl,-t, makes ld (gcc in case of GCC used in all stages) report all object files (*.o) and all libraries (*.a or *.dylib) it links to create the final binary. It helps to debug the build process, it can also safely be omitted when everything compiles fine.




USAGE
-----
The command line looks like :

    WP2LaTeX [arguments] <wpfilename> [<latexfilename>]
 [arguments]

When not supplied [<latexfilename>]
, the LaTeX filename is composed
by changing or
 adding `.tex' to the base name of WP-file. There are
also a lot of
 other arguments, which are described in the included
documentation or are available by executing 'wp2latex -?'.


THANKS
------
I want to thank all previous authors of this package to bring some
ideas. I try to gather them into my package. I thank to everybody
who found a bug or sended me some notes about incorrect function of 
WP2LaTeX.


COPYRIGHT
---------
This program can be redistributed and/or modified under the terms
of the GNU General Public License version 2 available from
http://www.gnu.org/copyleft/gpl.html. If anybody makes any change to this
package, I am asking him to explicitelly write that his package
differs from original package.
	Current copyright could be obtained by typing wp2latex -copyright.
	WP2LaTeX contains several libraries, that have different 
licencing models.
	It is also possible to buy commercional version that 
is GPL free.


DISCLAIMER
----------
Neither the author nor the Eindhoven University of Technology takes any
responsibility for any damage or loss of data regarding to the use of
the program. Usage of the program is for your own responsibility.


BUG REPORTING
-------------

Note:Before reporting anything 1,consult a file doc\bug.txt
			       2,Test latest version from:
			          http://www.penguin.cz/~fojtik/wp2latex/

a,If you find a bug, which cause a data loss or program crash please send
me a description, how to repeat this bug. Cut your WP file to shortest
possible, which still cause this bug. Then send me this file.

b,If you find a bug which causes TeX errors, send me a detailed description
what was happen and idea how the convertor should work in this case.
I know about many situation, when TeX reports errors and I don't know
how to cope with them.

c,If you cannot compile wp2latex please send me output of your make file.
make >out.txt
    Be prepared that I will ask you for some analysis of the problem because
on my computer it works fine. Please do not suspect me, that I missed some
typo in the source code. Before every distribution I recompile wp2latex
under several different compilers. I also welcome any suggestions obtained from
porting wp2latex to another platforms.


HOW TO CONTACT THE AUTHOR
-------------------------
Because nobody of previous authors want to hear about this program,
contact me: JaFojtik@seznam.cz

Yours Sincerely,
       Jaroslav Fojtik

