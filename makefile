##########################################################################
#	     The root generic makefile for wp2latex package		 #	
#									 #	
# Available targets are: make 		make selected modules		 #	
#                        make all	Make all + all selected modules	 #
#			 make clean	Remove all temporary files 	 #
#			 make distclean Thourough cleanup          	 #
#			 make install	Perform automatic installation   #
#                        make package   Statical build for package       #
#                        make wp2latex	Make only WordPerfect modules    #
#                        make x2latex	Make all conversion modules 	 #
#                        make testconv	Execute conversion test.         #
#                        make testltx	Execute conv followed by LaTeX   #
##########################################################################

all: cplib
	export OSTYPE && make -C ./sources.cc/ 	
	make -C ./doc/ all

default: cplib
	export OSTYPE && make -C ./sources.cc/ 

wp2latex: cplib
	export OSTYPE && make -C ./sources.cc/ TG_WP

x2latex: cplib
	export OSTYPE && make -C ./sources.cc/ TG_ALL

cplib:
	export OSTYPE && make -C ./sources.cc/cp_lib/

package:
	export OSTYPE && make -C ./sources.cc/ static

static:
	export OSTYPE && $(MAKE) -C ./sources.cc/ static
	$(MAKE) -C ./doc/ all

distclean: 
	make -C ./sources.cc/ distclean
	make -C ./instaler/ clean
#	make -C ./sources.pas/ clean
	make -C ./doc/ distclean
	make -k -C ./msvc/ clean
	make -C ./test/ clean
	rm -f *.exe *.OBJ *.obj *.o *.bak core *.gpr *.gdt *.dvi *.aux *.log *.ent *.toc *.idx
	rm -f config.status config.h
	rm makefile
	cp makefile.gen makefile

generic:
	make -C ./sources.cc/ generic
	make -C ./instaler/ generic
	rm makefile
	cp makefile.gen makefile

clean:
	make -C ./sources.cc/ clean
	make -C ./instaler/ clean
#	make -C ./sources.pas/ clean
	make -C ./doc/ clean
	make -C ./msvc/ clean
	make -C ./test/ clean
	rm -f *.exe *.OBJ *.obj *.o *.bak core *.gpr *.gdt *.dvi *.aux *.log *.ent *.toc *.idx
	rm -f config.status config.h

install:
	instaler/install

doc:
	make -C ./doc/ all

test:	default
	make -C ./test test

testltx: default
	make -C ./test testltx

testconv: default
	$(MAKE) -C ./test testconv



menuconfig:	instaler/menucfg.exe
	instaler/menucfg.exe sources.cc/modules.mak


#internal stuff
instaler/menucfg.exe:	instaler/menucfg.cc
instaler/menucfg.exe:	instaler/menucfg.cc
	export OSTYPE && make -C ./instaler/ menucfg.exe

configure: configure.in
	autoconf
