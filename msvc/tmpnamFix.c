#include "stdio.h"
#include "string.h"


char *tmpnamFix(char *s)
{
 char *ret = tmpnam(s);
 if(ret==NULL) return NULL;
 if(*ret=='\\' && strchr(ret+1,'\\')==NULL)  // Root of drive access?
 {
   s = ret;
   while(*s!=0)		// Destroy first \ character.
   {
     *s = s[1];
     s++;
   }
 }
 return ret;
}