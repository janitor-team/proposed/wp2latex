# Microsoft Developer Studio Project File - Name="wp2latex" - Package Owner=<4>
# Microsoft Developer Studio Generated Build File, Format Version 6.00
# ** DO NOT EDIT **

# TARGTYPE "Win32 (x86) Console Application" 0x0103

CFG=wp2latex - Win32 Debug PNG Gtxt
!MESSAGE This is not a valid makefile. To build this project using NMAKE,
!MESSAGE use the Export Makefile command and run
!MESSAGE 
!MESSAGE NMAKE /f "wp2latex.mak".
!MESSAGE 
!MESSAGE You can specify a configuration when running NMAKE
!MESSAGE by defining the macro CFG on the command line. For example:
!MESSAGE 
!MESSAGE NMAKE /f "wp2latex.mak" CFG="wp2latex - Win32 Debug PNG Gtxt"
!MESSAGE 
!MESSAGE Possible choices for configuration are:
!MESSAGE 
!MESSAGE "wp2latex - Win32 Release" (based on "Win32 (x86) Console Application")
!MESSAGE "wp2latex - Win32 Debug" (based on "Win32 (x86) Console Application")
!MESSAGE "wp2latex - Win32 Debug_Gettext" (based on "Win32 (x86) Console Application")
!MESSAGE "wp2latex - Win32 Release_Gettext" (based on "Win32 (x86) Console Application")
!MESSAGE "wp2latex - Win32 Debug PNG Gtxt" (based on "Win32 (x86) Console Application")
!MESSAGE "wp2latex - Win32 Release PNG Gtxt" (based on "Win32 (x86) Console Application")
!MESSAGE 

# Begin Project
# PROP AllowPerConfigDependencies 0
# PROP Scc_ProjName ""
# PROP Scc_LocalPath ""
CPP=cl.exe
RSC=rc.exe

!IF  "$(CFG)" == "wp2latex - Win32 Release"

# PROP BASE Use_MFC 0
# PROP BASE Use_Debug_Libraries 0
# PROP BASE Output_Dir "Release"
# PROP BASE Intermediate_Dir "Release"
# PROP BASE Target_Dir ""
# PROP Use_MFC 0
# PROP Use_Debug_Libraries 0
# PROP Output_Dir "Release"
# PROP Intermediate_Dir "Release"
# PROP Ignore_Export_Lib 0
# PROP Target_Dir ""
# ADD BASE CPP /nologo /W3 /GX /O2 /D "WIN32" /D "NDEBUG" /D "_CONSOLE" /D "_MBCS" /YX /FD /c
# ADD CPP /nologo /Zp1 /W3 /O1 /I "..\sources.cc\atoms\include" /I "..\sources.cc" /I "..\sources.cc\cp_lib" /D "WIN32" /D "NDEBUG" /D "_CONSOLE" /D "_MBCS" /D "FINAL" /YX /FD /c
# ADD BASE RSC /l 0x405 /d "NDEBUG"
# ADD RSC /l 0x409 /d "NDEBUG"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LINK32=link.exe
# ADD BASE LINK32 kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib odbccp32.lib kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib odbccp32.lib /nologo /subsystem:console /machine:I386
# ADD LINK32 kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib odbccp32.lib kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib odbccp32.lib /nologo /subsystem:console /machine:I386

!ELSEIF  "$(CFG)" == "wp2latex - Win32 Debug"

# PROP BASE Use_MFC 0
# PROP BASE Use_Debug_Libraries 1
# PROP BASE Output_Dir "Debug"
# PROP BASE Intermediate_Dir "Debug"
# PROP BASE Target_Dir ""
# PROP Use_MFC 0
# PROP Use_Debug_Libraries 1
# PROP Output_Dir "Debug"
# PROP Intermediate_Dir "Debug"
# PROP Target_Dir ""
# ADD BASE CPP /nologo /W3 /Gm /GX /ZI /Od /D "WIN32" /D "_DEBUG" /D "_CONSOLE" /D "_MBCS" /YX /FD /GZ /c
# ADD CPP /nologo /W3 /Gm /GX /ZI /Od /I "..\sources.cc\atoms\include" /I "..\sources.cc" /I "..\sources.cc\cp_lib" /D "WIN32" /D "_DEBUG" /D "_CONSOLE" /D "_MBCS" /FR /YX /FD /GZ /c
# ADD BASE RSC /l 0x405 /d "_DEBUG"
# ADD RSC /l 0x405 /d "_DEBUG"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LINK32=link.exe
# ADD BASE LINK32 kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib odbccp32.lib kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib odbccp32.lib /nologo /subsystem:console /debug /machine:I386 /pdbtype:sept
# ADD LINK32 kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib odbccp32.lib kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib odbccp32.lib /nologo /subsystem:console /debug /machine:I386 /pdbtype:sept

!ELSEIF  "$(CFG)" == "wp2latex - Win32 Debug_Gettext"

# PROP BASE Use_MFC 0
# PROP BASE Use_Debug_Libraries 1
# PROP BASE Output_Dir "wp2latex___Win32_Debug_Gettext"
# PROP BASE Intermediate_Dir "wp2latex___Win32_Debug_Gettext"
# PROP BASE Target_Dir ""
# PROP Use_MFC 0
# PROP Use_Debug_Libraries 1
# PROP Output_Dir "DbgGtxt"
# PROP Intermediate_Dir "DbgGtxt"
# PROP Ignore_Export_Lib 0
# PROP Target_Dir ""
# ADD BASE CPP /nologo /W3 /Gm /GX /ZI /Od /I "..\sources.cc\atoms\include" /I "..\sources.cc" /I "..\sources.cc\cp_lib" /D "WIN32" /D "_DEBUG" /D "_CONSOLE" /D "_MBCS" /YX /FD /GZ /c
# ADD CPP /nologo /W3 /Gm /GX /ZI /Od /I "..\sources.cc\atoms\include" /I "..\sources.cc" /I "..\sources.cc\cp_lib" /I "..\..\..\localiser" /D "WIN32" /D "_DEBUG" /D "_CONSOLE" /D "_MBCS" /D "__gettext__" /YX /FD /GZ /c
# ADD BASE RSC /l 0x405 /d "_DEBUG"
# ADD RSC /l 0x405 /d "_DEBUG"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LINK32=link.exe
# ADD BASE LINK32 kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib odbccp32.lib kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib odbccp32.lib /nologo /subsystem:console /debug /machine:I386 /pdbtype:sept
# ADD LINK32 gtxt.lib kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib odbccp32.lib kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib odbccp32.lib libjpeg.lib /nologo /subsystem:console /debug /machine:I386 /pdbtype:sept /libpath:"..\..\..\localiser\Debug" /libpath:"d:\prog\jpeg\debug"

!ELSEIF  "$(CFG)" == "wp2latex - Win32 Release_Gettext"

# PROP BASE Use_MFC 0
# PROP BASE Use_Debug_Libraries 0
# PROP BASE Output_Dir "wp2latex___Win32_Release_Gettext"
# PROP BASE Intermediate_Dir "wp2latex___Win32_Release_Gettext"
# PROP BASE Ignore_Export_Lib 0
# PROP BASE Target_Dir ""
# PROP Use_MFC 0
# PROP Use_Debug_Libraries 0
# PROP Output_Dir "RelGtxt"
# PROP Intermediate_Dir "RelGtxt"
# PROP Ignore_Export_Lib 0
# PROP Target_Dir ""
# ADD BASE CPP /nologo /Zp1 /W3 /O1 /I "..\sources.cc\atoms\include" /I "..\sources.cc" /I "..\sources.cc\cp_lib" /D "WIN32" /D "NDEBUG" /D "_CONSOLE" /D "_MBCS" /D "FINAL" /YX /FD /c
# ADD CPP /nologo /Zp1 /W3 /O1 /I "..\sources.cc\atoms\include" /I "..\sources.cc" /I "..\sources.cc\cp_lib" /I "..\..\..\localiser" /I "d:\prog\jpeg" /D "WIN32" /D "NDEBUG" /D "_CONSOLE" /D "_MBCS" /D "FINAL" /D "__gettext__" /D "_USE_JPEG" /YX /FD /c
# ADD BASE RSC /l 0x409 /d "NDEBUG"
# ADD RSC /l 0x409 /d "NDEBUG"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LINK32=link.exe
# ADD BASE LINK32 kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib odbccp32.lib kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib odbccp32.lib /nologo /subsystem:console /machine:I386
# ADD LINK32 gtxt.lib kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib odbccp32.lib kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib odbccp32.lib libjpeg.lib /nologo /subsystem:console /machine:I386 /libpath:"..\..\..\localiser\Release" /libpath:"d:\prog\jpeg\release"

!ELSEIF  "$(CFG)" == "wp2latex - Win32 Debug PNG Gtxt"

# PROP BASE Use_MFC 0
# PROP BASE Use_Debug_Libraries 1
# PROP BASE Output_Dir "wp2latex___Win32_Debug_PNG_Gtxt"
# PROP BASE Intermediate_Dir "wp2latex___Win32_Debug_PNG_Gtxt"
# PROP BASE Ignore_Export_Lib 0
# PROP BASE Target_Dir ""
# PROP Use_MFC 0
# PROP Use_Debug_Libraries 1
# PROP Output_Dir "wp2latex___Win32_Debug_PNG_Gtxt"
# PROP Intermediate_Dir "wp2latex___Win32_Debug_PNG_Gtxt"
# PROP Ignore_Export_Lib 0
# PROP Target_Dir ""
# ADD BASE CPP /nologo /W3 /Gm /GX /ZI /Od /I "..\sources.cc\atoms\include" /I "..\sources.cc" /I "..\sources.cc\cp_lib" /I "..\..\..\localiser" /D "WIN32" /D "_DEBUG" /D "_CONSOLE" /D "_MBCS" /D "__gettext__" /YX /FD /GZ /c
# ADD CPP /nologo /W3 /Gm /GX /ZI /Od /I "..\sources.cc\atoms\include" /I "..\sources.cc" /I "..\sources.cc\cp_lib" /I "..\..\..\localiser" /D "WIN32" /D "_DEBUG" /D "_CONSOLE" /D "_MBCS" /D "__gettext__" /D "_USE_PNG" /YX /FD /GZ /c
# ADD BASE RSC /l 0x405 /d "_DEBUG"
# ADD RSC /l 0x405 /d "_DEBUG"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LINK32=link.exe
# ADD BASE LINK32 gtxt.lib kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib odbccp32.lib kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib odbccp32.lib libjpeg.lib /nologo /subsystem:console /debug /machine:I386 /pdbtype:sept /libpath:"..\..\..\localiser\Debug" /libpath:"d:\prog\jpeg\debug"
# ADD LINK32 gtxt.lib kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib odbccp32.lib kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib odbccp32.lib libjpeg.lib libpng.lib /nologo /subsystem:console /debug /machine:I386 /pdbtype:sept /libpath:"..\..\..\localiser\Debug" /libpath:"d:\prog\jpeg\debug" /libpath:"E:\libs\src\libpng\1.2.37\libpng-1.2.37-src\projects\visualc6\Win32_LIB_Release"

!ELSEIF  "$(CFG)" == "wp2latex - Win32 Release PNG Gtxt"

# PROP BASE Use_MFC 0
# PROP BASE Use_Debug_Libraries 0
# PROP BASE Output_Dir "wp2latex___Win32_Release_PNG_Gtxt"
# PROP BASE Intermediate_Dir "wp2latex___Win32_Release_PNG_Gtxt"
# PROP BASE Ignore_Export_Lib 0
# PROP BASE Target_Dir ""
# PROP Use_MFC 0
# PROP Use_Debug_Libraries 0
# PROP Output_Dir "Win32_Release_PNG_Gtxt"
# PROP Intermediate_Dir "Win32_Release_PNG_Gtxt"
# PROP Ignore_Export_Lib 0
# PROP Target_Dir ""
# ADD BASE CPP /nologo /Zp1 /W3 /O1 /I "..\sources.cc\atoms\include" /I "..\sources.cc" /I "..\sources.cc\cp_lib" /I "..\..\..\localiser" /I "d:\prog\jpeg" /D "WIN32" /D "NDEBUG" /D "_CONSOLE" /D "_MBCS" /D "FINAL" /D "__gettext__" /D "_USE_JPEG" /YX /FD /c
# ADD CPP /nologo /Zp1 /W3 /O1 /I "..\sources.cc\atoms\include" /I "..\sources.cc" /I "..\sources.cc\cp_lib" /I "..\..\..\localiser" /I "E:\libs\src\jpeg" /D "WIN32" /D "NDEBUG" /D "_CONSOLE" /D "_MBCS" /D "FINAL" /D "__gettext__" /D "_USE_JPEG" /D "_USE_PNG" /YX /FD /c
# ADD BASE RSC /l 0x409 /d "NDEBUG"
# ADD RSC /l 0x409 /d "NDEBUG"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LINK32=link.exe
# ADD BASE LINK32 gtxt.lib kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib odbccp32.lib kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib odbccp32.lib libjpeg.lib /nologo /subsystem:console /machine:I386 /libpath:"..\..\..\localiser\Release" /libpath:"d:\prog\jpeg\release"
# ADD LINK32 gtxt.lib kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib odbccp32.lib kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib odbccp32.lib libjpeg.lib libpng.lib /nologo /subsystem:console /machine:I386 /libpath:"..\..\..\localiser\Release" /libpath:"E:\libs\src\jpeg\Release" /libpath:"E:\libs\src\libpng\1.2.37\libpng-1.2.37-src\projects\visualc6\Win32_LIB_Release"

!ENDIF 

# Begin Target

# Name "wp2latex - Win32 Release"
# Name "wp2latex - Win32 Debug"
# Name "wp2latex - Win32 Debug_Gettext"
# Name "wp2latex - Win32 Release_Gettext"
# Name "wp2latex - Win32 Debug PNG Gtxt"
# Name "wp2latex - Win32 Release PNG Gtxt"
# Begin Group "Source Files"

# PROP Default_Filter "cpp;c;cxx;rc;def;r;odl;idl;hpj;bat"
# Begin Group "Atoms"

# PROP Default_Filter ""
# Begin Source File

SOURCE=.\dbllist.cpp
# End Source File
# Begin Source File

SOURCE=.\lists.cpp
# End Source File
# Begin Source File

SOURCE=.\mtx_impl.cpp
# End Source File
# Begin Source File

SOURCE=.\sets.cpp
# End Source File
# Begin Source File

SOURCE=.\stack.cpp
# End Source File
# Begin Source File

SOURCE=.\strings.cpp
# End Source File
# End Group
# Begin Group "Convertors"

# PROP Default_Filter ""
# Begin Source File

SOURCE=.\pass1.cpp
# End Source File
# Begin Source File

SOURCE=.\pass1602.cpp
# End Source File
# Begin Source File

SOURCE=.\pass1_1.cpp
# End Source File
# Begin Source File

SOURCE=.\pass1_3.cpp
# End Source File
# Begin Source File

SOURCE=.\pass1_4.cpp
# End Source File
# Begin Source File

SOURCE=.\pass1_5.cpp
# End Source File
# Begin Source File

SOURCE=.\pass1_6.cpp
# End Source File
# Begin Source File

SOURCE=.\pass1abi.cpp
# End Source File
# Begin Source File

SOURCE=.\pass1acc.cpp
# End Source File
# Begin Source File

SOURCE=.\PASS1C45.cpp
# End Source File
# Begin Source File

SOURCE=.\pass1dcb.cpp
# End Source File
# Begin Source File

SOURCE=.\pass1htm.cpp
# End Source File
# Begin Source File

SOURCE=.\pass1mtf.cpp
# End Source File
# Begin Source File

SOURCE=.\pass1ole.cpp
# End Source File
# Begin Source File

SOURCE=.\pass1rtf.cpp
# End Source File
# Begin Source File

SOURCE=.\pass1wmf.cpp
# End Source File
# Begin Source File

SOURCE=.\Pass1wrd.cpp
# End Source File
# Begin Source File

SOURCE=.\pass1xml.cpp
# End Source File
# End Group
# Begin Group "images"

# PROP Default_Filter ""
# Begin Source File

SOURCE=.\ras_jpg.cpp
# End Source File
# Begin Source File

SOURCE=.\ras_png.cpp
# End Source File
# Begin Source File

SOURCE=.\raster.cpp
# End Source File
# Begin Source File

SOURCE=..\sources.cc\images\rasterc.c
# End Source File
# Begin Source File

SOURCE=.\rasterut.cpp
# End Source File
# End Group
# Begin Source File

SOURCE=..\Sources.cc\word\Bintree.c
# End Source File
# Begin Source File

SOURCE=.\charactr.cpp
# End Source File
# Begin Source File

SOURCE=".\CP-TRN.cpp"
# End Source File
# Begin Source File

SOURCE=.\cptran.cpp
# End Source File
# Begin Source File

SOURCE=.\filehnd.cpp
# End Source File
# Begin Source File

SOURCE=.\formulas.cpp
# End Source File
# Begin Source File

SOURCE=.\igettext.cpp
# End Source File
# Begin Source File

SOURCE=.\images.cpp
# End Source File
# Begin Source File

SOURCE=..\sources.cc\cole\new_cole.c
# End Source File
# Begin Source File

SOURCE=.\pass2.cpp
# End Source File
# Begin Source File

SOURCE=.\std_str.cpp
# End Source File
# Begin Source File

SOURCE=..\sources.cc\atoms\Struct.c
# End Source File
# Begin Source File

SOURCE=..\sources.cc\atoms\Utf8.c
# End Source File
# Begin Source File

SOURCE=.\WP2L_lib.cpp
# End Source File
# Begin Source File

SOURCE=.\wp2latex.cpp
# End Source File
# Begin Source File

SOURCE=.\wp2lfuti.cpp
# End Source File
# Begin Source File

SOURCE=..\Sources.cc\word\Wvparser.c
# End Source File
# End Group
# Begin Group "Header Files"

# PROP Default_Filter "h;hpp;hxx;hm;inl"
# Begin Source File

SOURCE=..\Sources.cc\pass1xml.h
# End Source File
# Begin Source File

SOURCE=..\Sources.cc\wp2latex.h
# End Source File
# End Group
# Begin Group "Resource Files"

# PROP Default_Filter "ico;cur;bmp;dlg;rc2;rct;bin;rgs;gif;jpg;jpeg;jpe"
# End Group
# End Target
# End Project
